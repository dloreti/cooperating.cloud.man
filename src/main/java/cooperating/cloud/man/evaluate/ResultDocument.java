/**
 * 
 */
package cooperating.cloud.man.evaluate;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.HostStatus;

/**
 * @author Daniela Loreti
 *
 */
public class ResultDocument {

	private static ResultDocument resultDocument;
	private Document doc ;
	private Element rootElement ;
	private Element migCountElement;
	private Element ditailsElement;
	private Element migElement;
	private Element msgCountElement;

	private ResultDocument() {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			doc = docBuilder.newDocument();
			rootElement = doc.createElement("scenario");
			doc.appendChild(rootElement);

			migElement = doc.createElement("migrations");
			rootElement.appendChild(migElement);

			migCountElement = doc.createElement("count");
			migCountElement.setTextContent("0");
			migElement.appendChild(migCountElement);

			ditailsElement = doc.createElement("details");
			migElement.appendChild(ditailsElement);

			Element msgElement = doc.createElement("messages");
			rootElement.appendChild(msgElement);
			msgCountElement = doc.createElement("count");
			msgCountElement.setTextContent("0");
			msgElement.appendChild(msgCountElement);


		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		}
	}

	public static ResultDocument getInstance()	  {
		if (resultDocument == null)	    {
			resultDocument = new ResultDocument();
		}
		return resultDocument; 
	}

	public synchronized void resetResultDocument(){
		migCountElement.setTextContent("0");
		msgCountElement.setTextContent("0");
		
		migElement.removeChild(ditailsElement);
		ditailsElement = doc.createElement("details");
		migElement.appendChild(ditailsElement);
//		System.out.println(Thread.currentThread().getId()+": reset result document");
	}

	public synchronized void addMessage(){
		int currentMsgNum=Integer.parseInt(msgCountElement.getTextContent());
		currentMsgNum++;
		msgCountElement.setTextContent(currentMsgNum+"");
//		System.out.println(Thread.currentThread().getId()+": ***************************************current message value: "+currentMsgNum);
	}
	public synchronized int getMessages(){
		return Integer.parseInt(msgCountElement.getTextContent());
	}
	
	public synchronized void addMigrations(int num){
		int currentMigrationNum=Integer.parseInt(migCountElement.getTextContent());
		currentMigrationNum+=num;
		migCountElement.setTextContent(currentMigrationNum+"");
		//System.out.println(Thread.currentThread().getId()+": current migration value: "+currentMigrationNum);
	}
	public synchronized void addMigrations(int num,int hostID, String details){
		int currentMigrationNum=Integer.parseInt(migCountElement.getTextContent());
		currentMigrationNum+=num;
		migCountElement.setTextContent(currentMigrationNum+"");

		Element detail =  doc.createElement("detail");
		detail.setAttribute("intohost", hostID+"");
		detail.setAttribute("countvms", num+"");
		detail.setTextContent(details);
		ditailsElement.appendChild(detail);
		//System.out.println(Thread.currentThread().getId()+": current migration value: "+currentMigrationNum);
	}
	public synchronized int getMigrations(){
		return Integer.parseInt(migCountElement.getTextContent());
	}

	public synchronized void addHostNode(HostDescriptor host) {
		host.toXml(this.rootElement);
	}

	public synchronized void writeDoc(String nomeFile){
		try {
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File(nomeFile));
			transformer.transform(source, result);

		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

	public synchronized int getWorkingHostsNum(){
		int solutionNumServer=0;
		doc.getDocumentElement().normalize();
		NodeList hostNodes=doc.getDocumentElement().getChildNodes();
		for (int i = 0; i < hostNodes.getLength(); i++) {
			if (hostNodes.item(i).getNodeName()=="host"){
				HostStatus hs = HostStatus.valueOf(((Element) hostNodes.item(i)).getAttribute("status"));
				if (hs!=HostStatus.OFF){
					solutionNumServer++;
				}
			}
		}
		return solutionNumServer;
	}
}
