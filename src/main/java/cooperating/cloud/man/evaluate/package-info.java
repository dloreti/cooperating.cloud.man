/**
 * This package contains the classes to build the cloud scenario and to start the simulation.
 * @since 1.0
 */
package cooperating.cloud.man.evaluate;
