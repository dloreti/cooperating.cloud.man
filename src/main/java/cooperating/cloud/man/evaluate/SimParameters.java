/**
 * 
 */
package cooperating.cloud.man.evaluate;

/**
 * The simulation parameters
 * @author Daniela Loreti
 *
 */
public class SimParameters {
	public static int MAX_roundWithUnchangedStatus = 3;
	public static final int SERVERSOCKET_TO=30000;
	public static final int WAITERROR_TO=40000;
	public static final boolean WAVE_BACK=false;

	public static final double HOST_TOTALCPU = 3000.0;
	public static double VM_TOTALCPU = 100.0;

	public final static boolean PRINTDEBUG = false;
	public final static boolean LOGDEBUG = true;
	public final static boolean FULLDEBUG = false;
	public final static boolean PROTOCOLDEBUG = true;
	public final static boolean MIGRATIONDEBUG = false;
	
	public final static String RESOURCESDIR = "../../resources";
	public final static String LOGONDIR = RESOURCESDIR+"/log";
	public final static String LOGONFILE = LOGONDIR+"/log.txt";
	public final static String CONFIGDIR = RESOURCESDIR+"/config";
	//public final static String CONFIGFILE_PATTERN = CONFIGDIR+"/config_ID.txt";
	public final static String CONF_FILE_PATTERN = CONFIGDIR+"/config_nServer_nVM_Load.xml";
	public final static String RESULTDIR = RESOURCESDIR+"/result";
	//public final static String RESULTFILE_PATTERN = RESULTDIR+"/result_ID.txt";
	public final static String RES_FILE_PATTERN = RESOURCESDIR+"/result"+"/result_nServer_nVM_nNeigh_Load_"
			+MAX_roundWithUnchangedStatus+".xml";

	//public final static String MIGRATIONFILE = RESOURCESDIR+"/result"+"/migrations"; //non txt!
	public final static String SUMMARYFILE = LOGONDIR+"/summary.csv"; //non txt!


	public static final double HOST_THRESH_UP = 100; //percent
	public static final double HOST_THRESH_LOW = 49; //percent

	public static final double MOBILE_THRESH = 8; //percent over and under the neighborhood average (MobileBestFit) 
	
	public static final int P_MAX = 250;

	public static final int SIMPLEX_TO = 3600;
	public static int VM_MINSIZE = 1;

	public enum ENUM_WORKING_HOST_MODE { ALL, STATIC, ACTIVE, };
	public static ENUM_WORKING_HOST_MODE WORKING_HOST_MODE = ENUM_WORKING_HOST_MODE.ACTIVE;
}
