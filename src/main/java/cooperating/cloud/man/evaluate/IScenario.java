/**
 * 
 */
package cooperating.cloud.man.evaluate;

import java.util.List;

import cooperating.cloud.man.com.SlaveServer;

/**
 * @author Daniela Loreti
 *
 */
public interface IScenario {
	public void generateScenarioWithLoad();
	public void writeConfigXML();
	public void loadXML();
	public void start(List<SlaveServer> slaveServers);

}
