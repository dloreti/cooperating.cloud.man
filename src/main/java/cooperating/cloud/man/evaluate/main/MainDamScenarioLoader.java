/**
 * 
 */
package cooperating.cloud.man.evaluate.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import cooperating.cloud.man.com.SlaveServer;
import cooperating.cloud.man.com.SubsequentNeighborhoodBuilder;
import cooperating.cloud.man.com.pc.ChannelManager;
import cooperating.cloud.man.com.pc.DamHostMonitor;
import cooperating.cloud.man.com.pc.DamSlaveServer;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.FairDamScenario;
import cooperating.cloud.man.evaluate.FairScenario;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;
import cooperating.cloud.man.policy.Allocator;
import cooperating.cloud.man.policy.BestFitAllocator;
import cooperating.cloud.man.policy.MobileBestFitAllocator;


/**
 * @author Daniela Loreti
 *
 */
public class MainDamScenarioLoader {

	public static final int MIN_nServer=100;
	public static final int PASSO_nServer=50;
	public static final int MAX_nServer=100;

	public static final int MIN_nNeigh=10;
	public static final int PASSO_nNeigh=5;
	public static final int MAX_nNeigh=MIN_nNeigh;//MIN_nServer/4;

	public static final int VMperServer =30;

	public static final int MIN_load=50;
	public static final int PASSO_load=5;
	public static final int MAX_load=85;

	public static String algo = "BF";
	public static int MAX_round = 3;

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		Utility.resetFile(SimParameters.LOGONFILE);
		SimParameters.MAX_roundWithUnchangedStatus = MAX_round;

		for(int loadPercentInt=MIN_load;loadPercentInt<=MAX_load;loadPercentInt+=PASSO_load){
			double loadPercent = loadPercentInt;
			for(int nServer=MIN_nServer;nServer<=MAX_nServer;nServer+=PASSO_nServer){
				int nVM= VMperServer*nServer; // 15505;//
				int nNeigh=MIN_nNeigh;
				while (nNeigh<=MAX_nNeigh){
					try {
						Utility.logPrintln("****------------------------ nServer="+nServer+" nVM="+nVM+" nNeigh="+nNeigh+" load="+loadPercentInt+"------------------------------------------****");
						System.out.println("****------------------------ nServer="+nServer+" nVM="+nVM+" nNeigh="+nNeigh+" load="+loadPercentInt+"------------------------------------------****");
						FairDamScenario scenario = new FairDamScenario(nServer, nVM, nNeigh, loadPercent);

						scenario.loadXML();
						Utility.logPrintln("Avvio lo scenario...");
						System.out.println("Avvio lo scenario...");;
						List<SlaveServer> slaveServers = buildSlaveServerList(scenario.getHostCollection(), algo, nNeigh, nServer);
						scenario.start(slaveServers);

						Utility.logPrintln(algo+";"+nServer+";"+nVM+";"+nNeigh+";"+loadPercent+";"+scenario.getSolutionNumServer()+";"+scenario.getSolutionMigration()+";"+scenario.getSolutionMessages()+";\n");
						System.out.println(algo+";"+nServer+";"+nVM+";"+nNeigh+";"+loadPercent+";"+scenario.getSolutionNumServer()+";"+scenario.getSolutionMigration()+";"+scenario.getSolutionMessages()+";\n");

						if (!(scenario.getSolutionNumServer()==nServer && scenario.getSolutionMigration()==0)){
							Utility.summaryFileAppend(algo, nServer, nVM, scenario.getObtainedNumVM(), nNeigh, loadPercent, scenario.getSolutionNumServer(), scenario.getSolutionMigration(), scenario.getSolutionMessages(), 0);
							nNeigh+=PASSO_nNeigh;
						}

						Set<Thread> threadSet = Thread.getAllStackTraces().keySet();

						for (Thread thread : threadSet) {
							System.out.print(thread.getId()+ " ");
						}
						int openchannels=ChannelManager.getInstance().getChannelSize();
						if (openchannels!=0){
							System.out.println("\n open channels: "+openchannels);
							System.out.println(ChannelManager.getInstance());
						}
					} catch (Exception e) {
						System.out.println(e.getMessage());
						continue;
					}
				}
			}
		}
	}

	/**
	 * @param map 
	 * @return
	 */
	static List<SlaveServer> buildSlaveServerList(Map<Integer, IHostDescriptor> map, String algorithm, int nNeigh, int nServer) {
		List<SlaveServer> slaveServers = new ArrayList<SlaveServer>();
		for (Entry<Integer, IHostDescriptor> hE : map.entrySet()) {
			Allocator allocator =null;
			switch (algorithm) {
			case "BF":
				allocator = new BestFitAllocator();
				break;
			case "MBF":
				allocator= new MobileBestFitAllocator(hE.getValue());
				break;

			default:
				break;
			}

			DamSlaveServer dss = 
					new DamSlaveServer( 
							new DamHostMonitor( hE.getValue(), 
									allocator,
									(new SubsequentNeighborhoodBuilder(hE.getValue().getId(),nNeigh, nServer)).buildEndPointDescriptors()
									)
							);
			slaveServers.add(dss);
		}
		return slaveServers;
	}
}
