/**
 * 
 */
package cooperating.cloud.man.evaluate.main;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import cooperating.cloud.man.com.SlaveServer;
import cooperating.cloud.man.com.SubsequentNeighborhoodBuilder;
import cooperating.cloud.man.com.pc.DamHostMonitor;
import cooperating.cloud.man.com.pc.DamSlaveServer;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;
import cooperating.cloud.man.evaluate.FairDamScenario;
import cooperating.cloud.man.evaluate.FairScenario;
import cooperating.cloud.man.evaluate.ResultDocument;
import cooperating.cloud.man.evaluate.Scenario;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.SimParameters.ENUM_WORKING_HOST_MODE;
import cooperating.cloud.man.evaluate.Utility;
import cooperating.cloud.man.policy.*;

/**
 * @author Daniela Loreti
 * @author Roberto Foschini
 *
 */
public class MainTest {
	private static int nVM = 1000;
	private static int nServer = 33;
	private final static NumberFormat numberFormat = NumberFormat.getInstance();
	private final static int[] neighbours = { 0, 5, 10, 15, }; //, 20
	private static int[] loads = { 50, 60, 70, 80, 90, };
	private final static Allocator[] allocators = {
		new NullFitAllocator(),
		new NextFitAllocator(),
		new FirstFitAllocator(),
		new BestFitAllocator(),
		new WorstFitAllocator(),
		new BppHeurAllocator(),
//		new CompositeFitAllocator(),
		new BppExactAllocator(),
	};

	public static void main(String[] args) throws IOException {
		// la CPU richiesta da ogni VM è compresa tra 1% e 10%
//		SimParameters.VM_TOTALCPU = SimParameters.HOST_TOTALCPU*0.1d;
//		SimParameters.VM_MINSIZE = (int)(SimParameters.HOST_TOTALCPU*0.01d);
		// la CPU richiesta da ogni VM è compresa tra 10% e 50%
//		SimParameters.VM_TOTALCPU = SimParameters.HOST_TOTALCPU*0.5d;
//		SimParameters.VM_MINSIZE = (int)(SimParameters.HOST_TOTALCPU*0.1d);

		int nObtainedVM;

		if (args.length>0) {
			nVM = Integer.parseInt(args[0]);
			nServer = Integer.parseInt(args[1]);
//			SimParameters.HOST_DIVISOR = Integer.parseInt(args[2]);
		}
		if (args.length>2)
			switch (args[2]) {
			case "ALL":
				SimParameters.WORKING_HOST_MODE = ENUM_WORKING_HOST_MODE.ALL;
				break;
			case "STATIC":
				SimParameters.WORKING_HOST_MODE = ENUM_WORKING_HOST_MODE.STATIC;
				break;
			case "ACTIVE":
				SimParameters.WORKING_HOST_MODE = ENUM_WORKING_HOST_MODE.ACTIVE;
				break;
			default:
				System.out.println("Invalid WORKING_HOST_MODE");
				System.exit(1);
				break;
			}
		if (args.length>3)
			loads = new int[]{ Integer.parseInt(args[3]) };
		if (args.length>4)
			SimParameters.MAX_roundWithUnchangedStatus = Integer.parseInt(args[4]);

		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);

		StringBuilder sbHeading= new StringBuilder();
		StringBuilder sbHosts = new StringBuilder("hosts\n");
		StringBuilder sbMessages = new StringBuilder("messages\n");
		StringBuilder sbMigrations = new StringBuilder("migrations\n");
		StringBuilder sbTime = new StringBuilder("time\n");

		sbHosts.append("load");
		sbMessages.append("load");
		sbMigrations.append("load");
		sbTime.append("load");
		for (int neighbour : neighbours) {
			for (Allocator allocator : allocators) {
				if (neighbour == 0) {
					sbHosts.append("\tGlobal-"+allocator.getClass().getSimpleName());
					sbMessages.append("\tGlobal-"+allocator.getClass().getSimpleName());
					sbMigrations.append("\tGlobal-"+allocator.getClass().getSimpleName());
					sbTime.append("\tGlobal-bestfit"+allocator.getClass().getSimpleName());
				} else {
//					if (allocator instanceof BppTemplateAllocator)
					if (allocator instanceof BppExactAllocator)
						continue;
					sbHosts.append("\tDam-"+allocator.getClass().getSimpleName()+"("+neighbour+")");
					sbMessages.append("\tDam-"+allocator.getClass().getSimpleName()+"("+neighbour+")");
					sbMigrations.append("\tDam-"+allocator.getClass().getSimpleName()+"("+neighbour+")");
					sbTime.append("\tDam-"+allocator.getClass().getSimpleName()+"("+neighbour+")");
				}
			}
		}
		sbHosts.append("\n");
		sbMessages.append("\n");
		sbMigrations.append("\n");
		sbTime.append("\n");

		Utility.resetFile(SimParameters.LOGONFILE);
//		try {
			for (int load : loads) {
				for (int nScenario = 0; nScenario < 1; nScenario++) {
					try {
						FairScenario s = new FairDamScenario(nServer, nVM, 0, load); // numNeighbour è irrilevante (nel costruttore)

						System.out.println("***************STARTING CONFIGURATION*********\n");
						System.out.printf("nServer=%d, nVM=%d, load=%d\n", nServer, nVM, load);
						s.generateScenarioWithLoad();
						s.writeConfigXML();
//						System.out.println(s.toString());
//						s.calculateOptimalSolution();
						
						nObtainedVM = s.getObtainedNumVM();
	
						sbHeading.append(nObtainedVM);

						sbHosts.append(load);
						sbMessages.append(load);
						sbMigrations.append(load);
						sbTime.append(load);
	
						// eventuale stampa dati dello scenario
						boolean print = false;
						if (print) {
							for (IHostDescriptor host : s.getHostCollection().values()) {
								System.out.println(host.getId() + ", [" + host.getTotalCPU() + "]");
								int total = 0;
								for (IVmDescriptor vm : host.getCurrentVmMap().values()) {
									System.out.println(" " + vm.getId() + ", " + vm.getUsedCPU());
									total += vm.getUsedCPU();
								}
								System.out.println("total=[" + total + "]");
							}
						}
	
						for (int neighbour : neighbours) {
							for (Allocator allocator : allocators) {
								String sType;
								int numHosts;
								int numMessages;
								int numMigrations;
								float dt;
								if (neighbour == 0) {
									// esecuzione algoritmi globali
									System.out.println("***************GLOBAL "+allocator.getClass().getSimpleName()+" CONFIGURATION*********");
									s.loadXML();
									s.setNumNeighbour(neighbour);
									ResultDocument.getInstance().resetResultDocument();
	
									SortedMap<Integer, IHostDescriptor> tempHostCollection = new TreeMap<>(s.getHostCollection());
					
									allocator.setHostCollection(tempHostCollection);
									long t1 = System.currentTimeMillis();
									allocator.optimize();
									long t2 = System.currentTimeMillis();

									for (Entry<Integer, IHostDescriptor> tempEntry : tempHostCollection.entrySet()) {
										tempEntry.getValue().commitChanges();
										ResultDocument.getInstance().addMessage();
										ResultDocument.getInstance().addMessage();
										ResultDocument.getInstance().addMessage();
									}
					
									String fileName = SimParameters.RES_FILE_PATTERN
											.replace("nServer", nServer + "")
											.replace("nVM", nVM + "")
											.replace("nNeigh", neighbour + "")
											.replace("Load", ((int) load) + "");
									ResultDocument.getInstance().writeDoc(fileName);
					
									sType = "Globale";
									numHosts = ResultDocument.getInstance().getWorkingHostsNum();
									numMessages = ResultDocument.getInstance().getMessages();
									numMigrations = ResultDocument.getInstance().getMigrations();
									dt = (t2-t1)/1000f;
								} else {
									// se non azzero, nulla viene ricalcolato
									// devo creare un nuovo resultdocument ==> impossibile con un singleton

	//								if (allocator instanceof BppTemplateAllocator)
									if (allocator instanceof BppExactAllocator)
										continue;
									// esecuzione algoritmi distribuiti (variando i vicinati)
									System.out.println("********************DISTRIBUTED "+allocator.getClass().getSimpleName()+" CONFIGURATION*********");
									s.loadXML();
									s.setNumNeighbour(neighbour);

									System.out.printf("nNeighbour=%d\n", neighbour); 
									SimParameters.MAX_roundWithUnchangedStatus = 3;
									System.out.println("MAX_roundWithUnchangedStatus="+SimParameters.MAX_roundWithUnchangedStatus);
									System.out.println("Avvio lo scenario...");
	
									List<SlaveServer> slaveServers = buildSlaveServerList(s.getHostCollection(), allocator, neighbour);
									long t1 = System.currentTimeMillis();
									s.start(slaveServers);
									long t2 = System.currentTimeMillis();

									sType = "Distrib";
									numHosts = s.getSolutionNumServer();
									numMessages = s.getSolutionMessages();
									numMigrations = s.getSolutionMigration();
									dt = (t2-t1)/1000f;
								}
								System.out.println("La soluzione "+sType+" con "+allocator.getClass().getSimpleName()
										+" usa "+numHosts+" server" +
										", Migrazioni richieste: "+numMigrations+
										", Messaggi richiesti: "+numMessages+
										", Tempo elaborazione: " + dt
								);
								sbHosts.append("\t" + s.getSolutionNumServer());
								sbMessages.append("\t" + s.getSolutionMessages());
								sbMigrations.append("\t" + s.getSolutionMigration());
								sbTime.append("\t" + numberFormat.format(dt));

								Utility.summaryFileAppend(sType+" "+allocator.getClass().getSimpleName(), nServer, nVM, nObtainedVM, neighbour, load, numHosts, numMigrations, numMessages, dt);
							}
						}
//					} catch (Exception ex) {
//						sbHosts.append(ex.getMessage());
//						sbMessages.append(ex.getMessage());
//						sbMigrations.append(ex.getMessage());
//						sbTime.append(ex.getMessage());
//						ex.printStackTrace();
//						throw ex;
					} finally {
						sbHosts.append("\n");
						sbMessages.append("\n");
						sbMigrations.append("\n");
						sbTime.append("\n");
					}

					BufferedWriter writer = new BufferedWriter(new FileWriter("out"+nVM+"_"+nServer+"_"+load+"_"+args[2]+".csv"));
					try {
//						writer.append("Heading: ");
						writer.append("numServer" + "\t" + "numVM" + "\t" + "obtainedVM" + "\n");
						writer.append(nServer + "\t" + nVM + "\t" + sbHeading.toString() + "\n");

						writer.append(sbHosts.toString() + "\n");
						writer.append(sbMessages.toString() + "\n");
						writer.append(sbMigrations.toString() + "\n");
						writer.append(sbTime.toString() + "\n");
					} finally {
						writer.close();
					}
				}
			}
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}

		System.out.println("fine");
		System.exit(0);
	}

	/**
	 * @param tempHostCollection
	 * @return
	 */
	@SuppressWarnings("unused")
	private static String hostMapToString(SortedMap<Integer, IHostDescriptor> tempHostCollection) {
		String toRet="";
		for (Entry<Integer, IHostDescriptor> hostEntry : tempHostCollection.entrySet()) {
			toRet+=hostEntry.getValue()+"\n";
		}
		return toRet;
	}

	/**
	 * @param map 
	 * @return
	 */
	static List<SlaveServer> buildSlaveServerList(Map<Integer, IHostDescriptor> map, final Allocator allocator, int nNeighbour) {
		List<SlaveServer> slaveServers = new ArrayList<SlaveServer>();
		for (Entry<Integer, IHostDescriptor> hE : map.entrySet()) {
			Allocator myAllocator;
			if (allocator instanceof MobileBestFitAllocator)
				myAllocator = new MobileBestFitAllocator(hE.getValue());
			else
				try {
					myAllocator = allocator.getClass().newInstance();
				} catch (InstantiationException | IllegalAccessException ex) {
					throw new UnsupportedOperationException();
				}

			DamSlaveServer dss = 
					new DamSlaveServer( 
							new DamHostMonitor(
									hE.getValue(), 
									myAllocator,
									(new SubsequentNeighborhoodBuilder(hE.getValue().getId(), nNeighbour, nServer)).buildEndPointDescriptors()
							)
					);
			slaveServers.add(dss);
		}
		return slaveServers;
	}
}
