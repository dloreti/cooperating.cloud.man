/**
 * 
 */
package cooperating.cloud.man.evaluate.main;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;
import cooperating.cloud.man.evaluate.FairDamScenario;
import cooperating.cloud.man.evaluate.ResultDocument;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;
import cooperating.cloud.man.policy.BestFitAllocator;


/**
 * @author Daniela Loreti
 *
 */
public class MainScenarioGenerator {

	public static final int MIN_nServer=100;
	public static final int PASSO_nServer=50;
	public static final int MAX_nServer=100;

	public static final int VMperServer =30;

	public static final int MIN_load=50;
	public static final int PASSO_load=5;
	public static final int MAX_load=95;

	public static final int nServer_under=1;
	public static final int nServer_over=0;
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		Utility.resetFile(SimParameters.LOGONFILE);

		for(int loadPercentInt=MIN_load;loadPercentInt<=MAX_load;loadPercentInt+=PASSO_load){
			double loadPercent = loadPercentInt;
			for(int nServer=MIN_nServer;nServer<=MAX_nServer;nServer+=PASSO_nServer){
				int nVM=VMperServer*nServer;
				try {
					Utility.logPrintln("****------------------------ nServer="+nServer+" nVM="+nVM+" load="+loadPercentInt+"------------------------------------------****");
					System.out.println("****------------------------ nServer="+nServer+" nVM="+nVM+" load="+loadPercentInt+"------------------------------------------****");
					FairDamScenario scenario = new FairDamScenario(nServer, nVM, 2, loadPercent, nServer_under, nServer_over);

					scenario.generateScenarioWithLoad();
					nVM = scenario.getNumVM();

					scenario.writeConfigXML();
					ResultDocument.getInstance().resetResultDocument();
					SortedMap<Integer, IHostDescriptor> tempHostCollection = new TreeMap<>() ;
					tempHostCollection.putAll(scenario.getHostCollectionClone());


					BestFitAllocator bfa = new BestFitAllocator();
					bfa.setHostCollection(tempHostCollection);
					bfa.optimize();
					tempHostCollection= bfa.getHostCollection();
					for (Entry<Integer, IHostDescriptor> tempEntry : tempHostCollection.entrySet()) {
						tempEntry.getValue().commitChanges();
						ResultDocument.getInstance().addMessage();
						ResultDocument.getInstance().addMessage();
						ResultDocument.getInstance().addMessage();
					}

					String fileName = SimParameters.RES_FILE_PATTERN
							.replace("nServer", nServer + "").replace("nVM", nVM + "").replace("nNeigh", nServer + "")
							.replace("Load", ((int) loadPercent) + "");
					ResultDocument.getInstance().writeDoc(fileName);
					int solnServer = ResultDocument.getInstance().getWorkingHostsNum();
					int solnMig = ResultDocument.getInstance().getMigrations();
					int solnMess = ResultDocument.getInstance().getMessages();
					String toPrint = "GLO;"+nServer+";"+nVM+";"+nServer+";"+loadPercent+";"+solnServer+";"+solnMig+";"+solnMess+";\n";
					Utility.logPrintln(toPrint);
					System.out.println(toPrint);
					
					Utility.summaryFileAppend("GLO", nServer, nVM, scenario.getObtainedNumVM(), nServer,loadPercent, solnServer, solnMig, solnMess, 0);

				} catch ( Exception e) {
					System.out.println(e.getMessage());
					continue;
				}
				//				}
			}
		}
	}


	@SuppressWarnings("unused")
	private boolean testVmCollection(SortedMap<Integer, HostDescriptor> tempHostCollection){
		SortedMap<Integer, IVmDescriptor> currVmCollection = new TreeMap<>();
		SortedMap<Integer, IVmDescriptor> futVmCollection = new TreeMap<>();

		for (Entry<Integer, HostDescriptor> hEntry : tempHostCollection.entrySet()) {
			if (hEntry.getKey()!=hEntry.getValue().getId()) {
				System.out.println("Wrong ids (hId="+hEntry.getValue().getId()+
						" while key="+hEntry.getKey());
				return false;
			}
			//checkcurr
			for (Entry<Integer, IVmDescriptor> vmEntry : hEntry.getValue().getCurrentVmMap().entrySet()) {
				if (vmEntry.getKey()!=vmEntry.getValue().getId()) {
					System.out.println("Wrong ids (vmId="+vmEntry.getValue().getId()+
							" while key="+vmEntry.getKey()+") in the currentVmMap of host: "+hEntry.getKey());
					return false;
				}
				if (currVmCollection.get(vmEntry.getKey())==null){
					currVmCollection.put(vmEntry.getKey(),vmEntry.getValue());
				}else{
					System.out.println("Duplicated vm="+vmEntry.getValue()+" in currentVmMap");
					return false;
				}
			}

			//checkfut
			for (Entry<Integer, IVmDescriptor> vmEntry : hEntry.getValue().getFutureVmMap().entrySet()) {
				if (vmEntry.getKey()!=vmEntry.getValue().getId()) {
					System.out.println("Wrong ids (vmId="+vmEntry.getValue().getId()+
							" while key="+vmEntry.getKey()+") in the futureVmMap of host: "+hEntry.getKey());
					return false;
				}
				if (futVmCollection.get(vmEntry.getKey())==null){
					futVmCollection.put(vmEntry.getKey(), vmEntry.getValue());
				}else{
					System.out.println("Duplicated vm="+vmEntry.getValue()+" in futureVmMap");
					return false;
				}
			}
		}
		
		return true;
	}
	
	@SuppressWarnings("unused")
	private int testMigrations(SortedMap<Integer, HostDescriptor> tempHostCollection){
		int i=0;
		SortedMap<Integer, IVmDescriptor> migratedVm =new TreeMap<>();
		for (Entry<Integer, HostDescriptor> tempEntry : tempHostCollection.entrySet()) {
			for (Entry<Integer, IVmDescriptor> futVmEntry : tempEntry.getValue().getFutureVmMap().entrySet()) {
				boolean found = false;
				for (Entry<Integer, IVmDescriptor> curVmEntry : tempEntry.getValue().getCurrentVmMap().entrySet()) {
					if (curVmEntry.getValue().getId()==futVmEntry.getValue().getId()) found =true;
				}
				if (!found) {
					if (migratedVm.get(futVmEntry.getKey())==null){
						migratedVm.put(futVmEntry.getKey(), futVmEntry.getValue());
					}else{
						System.out.println("VM "+futVmEntry.getKey()+" already migrated!");
					}
					i++;
				}
			}
		}
		return i;
	}
}
