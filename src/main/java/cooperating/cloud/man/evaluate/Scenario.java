/**
 * 
 */
package cooperating.cloud.man.evaluate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.sat4j.pb.SolverFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cooperating.cloud.man.com.SlaveServer;
import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.HostStatus;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;
import cooperating.cloud.man.entities.VmDescriptor;
import cooperating.cloud.man.evaluate.Utility;
import cooperating.cloud.man.evaluate.simplex.BinPackingSolver;
import cooperating.cloud.man.evaluate.simplex.Item;

/**
 * @author Daniela Loreti
 * 
 */
public abstract class Scenario implements IScenario {
	private int numServer;
	private int numVM;
	private int numNeighbour;
	private double loadPercent;

	private double solutionValue;
	private int solutionNumServer;
	private int solutionMigration;
	private int solutionMessages;

	private double optSolutionValue;
	private boolean optSolutionIsOptimal;
	private int optSolutionNumServer;
	private int optSolutionMigration;

	private Map<Integer, IHostDescriptor> hostCollection = new HashMap<Integer, IHostDescriptor>();

	/**
	 * @param numServer
	 * @param numVM
	 * @param numNeighbour
	 * @param loadPercent
	 * @param allocator
	 * @throws IllegalArgumentException
	 */
	public Scenario(int numServer, int numVM, int numNeighbour,
			double loadPercent) throws IllegalArgumentException {
		this(numServer, numVM, numNeighbour);
		this.loadPercent = loadPercent;
		if (!checkLoad())
			throw new IllegalArgumentException(
					"The scenario is unfeasible (numServer=" + numServer
					+ " numVM=" + numVM + " loadPercent=" + loadPercent
					+ ").");
	}

	/**
	 * @param numServer
	 * @param numVM
	 * @param numNeighbour
	 */
	public Scenario(int numServer, int numVM, int numNeighbour) {
		super();
		this.numServer = numServer;
		this.numVM = numVM;
		this.numNeighbour = numNeighbour;
	}

	public void generateScenarioRandomLoad() {
		this.setHostCollection(new HashMap<Integer, IHostDescriptor>());

		for (int i = 0; i < getNumServer(); i++) {
			Map<Integer, IVmDescriptor> vmMap = new HashMap<Integer, IVmDescriptor>();
			HostDescriptor host = new HostDescriptor(i, SimParameters.HOST_TOTALCPU, HostStatus.OK, vmMap);
			getHostCollection().put(i, host);
		}

		for (int j = 0; j < numVM; j++) {
			int hostID = (int) (Math.random() * getNumServer());
			VmDescriptor v = new VmDescriptor(j, SimParameters.VM_TOTALCPU, Math.random()
					* (SimParameters.VM_TOTALCPU-SimParameters.VM_MINSIZE)+SimParameters.VM_MINSIZE, hostID);
			while (!getHostCollection().get(hostID).allocate(v)) {
				hostID = (int) (Math.random() * getNumServer());
			}
		}
	}

	public boolean checkLoad() {
		double load = (SimParameters.HOST_TOTALCPU * getNumServer()) / 100 * loadPercent;
		double mediumVmLoad = load / numVM;
		if (mediumVmLoad > SimParameters.VM_TOTALCPU) {
			System.err.println("Too few VMs");
			return false;
		}

		int loadInt = (int) ((SimParameters.HOST_TOTALCPU * getNumServer()) / 100 * loadPercent);
		if (getNumVM() > loadInt) {
			// non posso splittare il carico fino a raggiungere il numero di vm
			// richieste
			// perche ciascuna vm deve avere almeno carico 1
			System.err.println("Too many VMs");
			return false;
		} else
			return true;
	}

	public abstract void generateScenarioWithLoad();

	public void writeConfigXML() {
		String nomeFile = SimParameters.CONF_FILE_PATTERN
				.replace("nServer", numServer + "")
				.replace("nVM", numVM + "")
				.replace("Load", ((int) loadPercent) + "");

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("scenario");
			doc.appendChild(rootElement);

			for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
				hostEntry.getValue().toXml(rootElement);
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File(nomeFile));
			transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

	public void loadXML() throws IllegalArgumentException {
		ResultDocument.getInstance().resetResultDocument();
		solutionNumServer = 0;
		solutionMessages = 0;
		solutionMigration = 0;
		solutionValue = 0;

		setHostCollection(new HashMap<Integer,IHostDescriptor>());
		String fileName = SimParameters.CONF_FILE_PATTERN
				.replace("nServer", numServer + "")
				.replace("nVM", numVM + "")
				.replace("Load", ((int) loadPercent) + "");
		File fXmlFile = new File(fileName);
		int numVM = 0;
		if (fXmlFile.exists() && fileName.endsWith(".xml")) {
			try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();

				NodeList nList = doc.getElementsByTagName("host");
				for (int i = 0; i < nList.getLength(); i++) {
					Node nNode = nList.item(i);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						HostDescriptor h = new HostDescriptor(eElement);
						numVM+=h.getCurrentVmMapSize();
						getHostCollection().put(h.getId(), h);
					}
				}
			} catch (ParserConfigurationException | SAXException | IOException e1) {
				e1.printStackTrace();
				System.exit(2);
			}
		}
		setNumServer(hostCollection.size());
		setNumVM(numVM);
		System.out.println("Scenario caricato.");
	}

	public void start(List<SlaveServer> slaveServers) {
		ResultDocument.getInstance().resetResultDocument();
		solutionNumServer = 0;
		solutionMessages = 0;
		solutionMigration = 0;
		solutionValue = 0;

		List<Thread> ssThreads = new ArrayList<>();
		for (SlaveServer ss : slaveServers) {

			Thread t = new Thread(ss);
			ssThreads.add(t);
			//if (t.getId()==2 || t.getId()==3 || t.getId()==4) System.out.println( t.getId() +" is a slaveserver");
			t.start();
			
		}
		for (Thread t : ssThreads) {
			try {
				t.join(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		String fileName = SimParameters.RES_FILE_PATTERN
				.replace("nServer", numServer + "")
				.replace("nVM", numVM + "")
				.replace("nNeigh", numNeighbour + "")
				.replace("Load", ((int) loadPercent) + "");
		ResultDocument.getInstance().writeDoc(fileName);
	}

	public List<Process> startProcessList() throws IOException {
		ResultDocument.getInstance().resetResultDocument();
		List<Process> slaveServers = new ArrayList<>();
		for (Entry<Integer, IHostDescriptor> hostEntry : hostCollection.entrySet()) {
			IHostDescriptor host = hostEntry.getValue();
			Utility.logPrintln("Scenario runs the slaveserver " + host.getId()
					+ " on post " + (host.getId() + 5000));
			Process process = Runtime.getRuntime().exec(
					"java cooperating.cloud.man.communicate.SlaveServer "
							+ host.getId() + " " + (host.getId() + 5000));
			slaveServers.add(process);
		}

		for (Process process : slaveServers) {
			int exitVal = 4;
			try {
				exitVal = process.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (exitVal != 0)
				System.out.println("ATTENZIONE: Process exitValue: " + exitVal);
		}
		return slaveServers;
	}

	public void evaluateCCNsolution() {
		solutionNumServer = ResultDocument.getInstance().getWorkingHostsNum();
		solutionValue = solutionNumServer*SimParameters.P_MAX;
		solutionMigration = ResultDocument.getInstance().getMigrations();
		solutionMessages = ResultDocument.getInstance().getMessages();
	}

	/**
	 * @return the numNeighbour
	 */
	public int getNumNeighbour() {
		return numNeighbour;
	}

	/**
	 * @param numNeighbour
	 *            the numNeighbour to set
	 */
	public void setNumNeighbour(int numNeighbour) {
		this.numNeighbour = numNeighbour;
	}

	/**
	 * @return the numServer
	 */
	public int getNumServer() {
		return numServer;
	}

	/**
	 * @param numServer
	 *            the numServer to set
	 */
	public void setNumServer(int numServer) {
		this.numServer = numServer;
	}

	/**
	 * @return the numVM
	 */
	public int getNumVM() {
		return numVM;
	}

	/**
	 * @param numVM
	 *            the numVM to set
	 */
	public void setNumVM(int numVM) {
		this.numVM = numVM;
	}

	/**
	 * @return the loadPercent
	 */
	public double getLoadPercent() {
		return loadPercent;
	}

	/**
	 * @param loadPercent
	 *            the loadPercent to set
	 */
	public void setLoadPercent(double loadPercent) {
		this.loadPercent = loadPercent;
	}

	/**
	 * @return the hostCollection
	 */
	public Map<Integer, IHostDescriptor> getHostCollection() {
		return hostCollection;
	}

	/**
	 * @param hostCollection
	 *            the hostCollection to set
	 */
	protected void setHostCollection(Map<Integer, IHostDescriptor> hostCollection) {
		this.hostCollection = hostCollection;
	}

	/**
	 * @return the solutionValue
	 */
	public double getSolutionValue() {
		if (solutionValue == 0)
			evaluateCCNsolution();
		return solutionValue;
	}

	/**
	 * @return the solutionNumServer
	 */
	public int getSolutionNumServer() {
		if (solutionNumServer == 0)
			evaluateCCNsolution();
		return solutionNumServer;
	}

	/**
	 * @return the solutionMigration
	 */
	public int getSolutionMigration() {
		if (solutionMigration == 0)
			evaluateCCNsolution();
		return solutionMigration;
	}

	/**
	 * @return the solutionMessages
	 */
	public int getSolutionMessages() {
		if (solutionMessages == 0)
			evaluateCCNsolution();
		return solutionMessages;
	}

	/**
	 * @return the optSolutionValue
	 */
	public double getOptSolutionValue() {
		return optSolutionValue;
	}

	/**
	 * @return the optSolutionIsOptimal
	 */
	public boolean isOptSolutionIsOptimal() {
		return optSolutionIsOptimal;
	}

	/**
	 * @return the optSolutionNumServer
	 */
	public int getOptSolutionNumServer() {
		return optSolutionNumServer;
	}

	/**
	 * @return the optSolutionMigration
	 */
	public int getOptSolutionMigration() {
		return optSolutionMigration;
	}

	@Override
	public String toString() {
		String toRet="";
		for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
			toRet+=hostEntry.getValue()+"\n";
		}
		return toRet;
	}


	/**
	 * @return
	 */
	public Map<Integer, IHostDescriptor> getHostCollectionClone() {
		SortedMap<Integer, IHostDescriptor> hostCollectionCopy = new TreeMap<>();
		for (Entry<Integer, IHostDescriptor> hcEntry : getHostCollection().entrySet()) {
			//hostCollectionCopy.put(hcEntry.getKey(), new HostDescriptor(hcEntry.getValue()));
			hostCollectionCopy.put(hcEntry.getKey(), hcEntry.getValue().copy());
		}
		return hostCollectionCopy;
	}

	public void calculateOptimalSolution() {
		List<IVmDescriptor> vmList = new ArrayList<>();
		for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
			vmList.addAll(hostEntry.getValue().getFutureVmMap().values());
		}

		int N = numServer * numVM + numServer;
		List<Item> items = new ArrayList<>(N);

		for (int u = 0; u < N; u++) {
			Item it = null;
			if (u < numServer * numVM) {
				int j = u / numServer;
				int i = u % numServer;
				it = new Item("x" + j + "," + i, (int) Math.round(vmList.get(j).getUsedCPU()), 0, j, i);
			} else {
				int i = u - (numServer * numVM);
				it = new Item("y" + i, -(int) Math.round(hostCollection.get(i).getTotalCPU()), SimParameters.P_MAX, i);
			}
			items.add(it);
		}
//		System.out.println(items);

		BinPackingSolver bps = new BinPackingSolver(SolverFactory.newBoth(), SimParameters.SIMPLEX_TO/10);
		List<Item> sol = bps.solve(items, numServer, numVM);
		this.optSolutionIsOptimal = bps.isOptimal();
		this.optSolutionValue = bps.bestValue();
		bps.printStats();
		int n = Math.round(bps.bestValue() / SimParameters.P_MAX);
		// se cambia la f obiettivo devo sommare le yi!=0 in sol
		this.optSolutionNumServer = n;
		this.optSolutionMigration = getOptimalNumMigration(vmList, sol);
	}
	static private int getOptimalNumMigration(List<IVmDescriptor> vmList, List<Item> sol) {
		int migrations = 0;
		for (Item item : sol) {
			if (item.getName().contains("x")) {
				int j = item.getJ_index(); // indice di vm;
				int i = item.getI_index(); // indice di server;
				if (vmList.get(j).getCurrentHostID() != i) {
					// comparazione con situazione premigrazione!
					migrations++;
				}
			}
		}
		return migrations;
	}
}
