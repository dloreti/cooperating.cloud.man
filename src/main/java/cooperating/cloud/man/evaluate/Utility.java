/**
 * 
 */
package cooperating.cloud.man.evaluate;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.SortedMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;



/**
 * @author Daniela Loreti
 *
 */
public class Utility {
	public static void summaryFileAppend(String algorithm, int nServer, int nVM, int nObtainedVM, int nNeighbour, double loadPercent, 
			int sol_nServer, int sol_nMig, int sol_nMsg, float solElapsed) {
		char sep = '\t';
		try {
			File file = new File(SimParameters.SUMMARYFILE);
			boolean file_exists = file.exists();

			try (BufferedWriter resultFile = new BufferedWriter(new FileWriter(file, file_exists))) {
				if (!file_exists) {
					resultFile.write("Alg"+sep+"nHosts"+sep+"nVM"+sep+"nObtVM"+sep+"nNeighb"+sep+"load%"+sep+"solHost"+sep+"solMigs"+sep+"solMsgs"+sep+"solElap"+sep);
					resultFile.newLine();
				}
				resultFile.append(algorithm+sep+nServer+sep+nVM+sep+nObtainedVM+sep+nNeighbour+sep+(int)loadPercent+sep+sol_nServer+sep+sol_nMig+sep+sol_nMsg+sep+NumberFormat.getNumberInstance().format(solElapsed)+sep);
				resultFile.newLine();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void resetFile(String filename){
		try{
			BufferedWriter resultFile = new BufferedWriter(new FileWriter(filename, false));
			resultFile.write("");
			resultFile.close();
		}catch(IOException ioe){
			System.err.println("Problemi nella scrittura su "+ SimParameters.LOGONFILE + ": ");
			ioe.printStackTrace();
			System.exit(3);
		}
	}

	public static void logPrintln(String line){
		line= Thread.currentThread().getId()+":"+line;
		if (SimParameters.PRINTDEBUG){
			System.out.println(line);
		}
		if(SimParameters.LOGDEBUG){
			try{
				BufferedWriter resultFile = new BufferedWriter(new FileWriter(SimParameters.LOGONFILE,true));
				resultFile.write(line+"\n");
				resultFile.close();
			}catch(IOException ioe){
				System.err.println("Problemi nella scrittura su "+ SimParameters.LOGONFILE + ": ");
				ioe.printStackTrace();
				System.exit(3);
			}
		}
	}
	public static void logPrintln(String line, Throwable e){
		line= Thread.currentThread().getId()+":"+line+" Ex.message: "+e.getMessage()+"\n";
		//if (SimParameters.PRINTDEBUG){
		System.err.println(line);
		//}
		e.printStackTrace();  //printed anyway!
		if(SimParameters.LOGDEBUG){
			try{
				BufferedWriter resultFile = new BufferedWriter(new FileWriter(SimParameters.LOGONFILE,true));
				resultFile.write(line);
				for (StackTraceElement stackTraceElement : e.getStackTrace()) {
					resultFile.write(stackTraceElement.toString()+"\n");
				}
				resultFile.close();
			}catch(IOException ioe){
				System.err.println("Problemi nella scrittura su "+ SimParameters.LOGONFILE + ": ");
				ioe.printStackTrace();
				System.exit(3);
			}
		}
	}

	public static void waitForFilesModify(String dirname){
		File dir = new File(dirname);

		String directory[] =null;
		if (dir.exists() && dir.isDirectory())		{
			directory = dir.list();
		}		else{
			System.out.printf(dirname+" directory does not exist");
		}

		SortedMap<String, Long> dim = new TreeMap<>();
		SortedMap<String, Long> oldDim = new TreeMap<>();
		boolean uguali;
		int roundUguali = 0;
		System.out.print("Attendo che i file siano moficati...");
		do{
			for (String fileName : directory){
				if( fileName.endsWith(".txt")){
					File file = new File(dirname+"/"+fileName);
					dim.put(fileName, file.length());
				}
			}
			//			System.out.print("DimOLD:");
			//			for (Entry<String, Long> dimentry : oldDim.entrySet()) {
			//				System.out.print("["+dimentry.getKey().replace("result_", "").replace(".txt", "")+","+dimentry.getValue()+"]");
			//			}
			//			System.out.println();
			//			System.out.print("Dim   :");
			//			for (Entry<String, Long> dimentry : dim.entrySet()) {
			//				System.out.print("["+dimentry.getKey().replace("result_", "").replace(".txt", "")+","+dimentry.getValue()+"]");
			//			}
			//			System.out.println();
			uguali=true;

			for (Entry<String, Long> dimentry : dim.entrySet()) {
				if(oldDim.isEmpty()  || !oldDim.get(dimentry.getKey()).equals(dimentry.getValue())) {
					uguali=false;
					//					System.out.println("DimOLD:"+oldDim.get(dimentry.getKey()) +"!= Dim:"+dimentry.getValue());
					roundUguali=0;
					break;
				} 
			}
			if (uguali){
				roundUguali++;
				//				System.out.println("roundUguali="+roundUguali);
				System.out.print(".");
			}

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}//2 sec

			//System.out.println("DimOLD != Dim ");
			oldDim=new TreeMap<>();
			for (Entry<String, Long> dime : dim.entrySet()) {
				oldDim.put(dime.getKey(), dime.getValue());
			}
			
			//}while (!dim.equals(oldDim));
		}while (roundUguali<10);

	}

	public static void generateConfigXmlForOptaplanner(){
		String dirname = SimParameters.CONFIGDIR;
		File dir = new File(dirname);

		String[] directory = null;
		if (dir.exists() && dir.isDirectory()){
			directory = dir.list();
		}else{
			System.out.printf(dirname+" directory does not exist");
		}

		try {
			int idattr=1;
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("CloudBalance");
			rootElement.setAttribute("id", ""+idattr++);
			doc.appendChild(rootElement);

			Element id = doc.createElement("id");
			id.appendChild(doc.createTextNode("0"));
			rootElement.appendChild(id);

			Element computerList = doc.createElement("computerList");
			computerList.setAttribute("id",""+idattr++);
			rootElement.appendChild(computerList);

			Element processList = doc.createElement("processList");
			processList.setAttribute("id",""+idattr++);
			rootElement.appendChild(processList);

			List<String> controller = new ArrayList<>();
			int nServer=0;
			int nVM = 0;
			for (String fileName : directory)
			{
				if( fileName.endsWith(".txt")){
					@SuppressWarnings("resource")
					BufferedReader configFile = new BufferedReader(new FileReader(dirname+"/"+fileName));
					System.out.printf("Reading "+fileName+"\n");
					Element CloudComputer = doc.createElement("CloudComputer");
					CloudComputer.setAttribute("id",""+idattr++);
					computerList.appendChild(CloudComputer);

					String firstLine[] = configFile.readLine().split(" ");

					Element CloudComputerId = doc.createElement("id");
					CloudComputerId.appendChild(doc.createTextNode(firstLine[0]));
					CloudComputer.appendChild(CloudComputerId);

					Element cpuPower = doc.createElement("cpuPower");
					int cpuPowerNum = Math.round(Float.parseFloat(firstLine[1]));
					cpuPower.appendChild(doc.createTextNode(cpuPowerNum+""));
					CloudComputer.appendChild(cpuPower);

					Element memory = doc.createElement("memory");
					memory.appendChild(doc.createTextNode("0"));
					CloudComputer.appendChild(memory);
					Element networkBandwidth = doc.createElement("networkBandwidth");
					networkBandwidth.appendChild(doc.createTextNode("0"));
					CloudComputer.appendChild(networkBandwidth);
					Element cost = doc.createElement("cost");
					cost.appendChild(doc.createTextNode("250"));
					CloudComputer.appendChild(cost);


					String line = null;
					while (( line = configFile.readLine() )!=null){
						if (line.charAt(0) != '[') break;
						String lineArray[] = line.split(" ");
						String vmId = lineArray[0].replace("[", "");

						if (controller.contains(vmId)) System.out.println("Duplicated vm "+vmId);
						//else controller.add(vmId); System.out.print("  "+vmId);
						String vmCpuReq = lineArray[2].replace("]", "");

						Element CloudProcess = doc.createElement("CloudProcess");
						CloudProcess.setAttribute("id",""+idattr++);
						processList.appendChild(CloudProcess);

						Element CloudProcessId = doc.createElement("id");
						CloudProcessId.appendChild(doc.createTextNode(vmId));
						CloudProcess.appendChild(CloudProcessId);

						Element requiredCpuPower = doc.createElement("requiredCpuPower");
						int requiredCpuPowerNum = Math.round(Float.parseFloat(vmCpuReq));
						requiredCpuPower.appendChild(doc.createTextNode(requiredCpuPowerNum+""));
						CloudProcess.appendChild(requiredCpuPower);

						Element requiredMemory = doc.createElement("requiredMemory");
						requiredMemory.appendChild(doc.createTextNode("0"));
						CloudProcess.appendChild(requiredMemory);

						Element requiredNetworkBandwidth = doc.createElement("requiredNetworkBandwidth");
						requiredNetworkBandwidth.appendChild(doc.createTextNode("0"));
						CloudProcess.appendChild(requiredNetworkBandwidth);
						nVM++;
					}
					nServer++;
				}
			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("/Users/utente/Documents/workspace/optaplanner/examples/data/cloudbalancing/unsolved/config_"+nServer+"_"+nVM+".xml"));

			transformer.transform(source, result);

			System.out.println("File config_"+nServer+"_"+nVM+".xml saved!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}


}
