package cooperating.cloud.man.evaluate.simplex;

public class Item {

	private final int weight;
	private final int value;
	private final String name;
	private int j_index;
	private final int i_index;
	
//	public Item(String name, int weight, int cost) {
//		this.name = name;
//		this.weight = weight;
//		this.value = cost;
//	}	
	
	/**
	 * @param weight
	 * @param value
	 * @param name
	 * @param j_index
	 * @param i_index
	 */
	public Item(String name, int weight, int value, int j_index, int i_index) {
		super();
		this.weight = weight;
		this.value = value;
		this.name = name;
		this.j_index = j_index;
		this.i_index = i_index;
	}

	/**
	 * @param weight
	 * @param value
	 * @param name
	 * @param j_index
	 * @param i_index
	 */
	public Item(String name, int weight, int value, int i_index) {
		super();
		if (!name.contains("y"))
			throw new IllegalArgumentException("Solo un item y - bin può non avere l'indice j");
		else{
		this.weight = weight;
		this.value = value;
		this.name = name;
		this.i_index = i_index;
		}
	}

	public int getWeight() {
		return weight;
	}

	public int getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "\n"+name+"("+weight+","+value+")";
	}

	/**
	 * @return the i_index
	 */
	public int getI_index() {
		return i_index;
	}

	/**
	 * @return the j_index
	 */
	public int getJ_index() {
		return j_index;
	}
}
