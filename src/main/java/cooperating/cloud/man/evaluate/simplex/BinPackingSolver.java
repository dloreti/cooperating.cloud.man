/**
 * 
 */
package cooperating.cloud.man.evaluate.simplex;

import static org.sat4j.pb.tools.WeightedObject.newWO;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.sat4j.pb.IPBSolver;
import org.sat4j.pb.OptToPBSATAdapter;
import org.sat4j.pb.PseudoOptDecorator;
import org.sat4j.pb.tools.DependencyHelper;
import org.sat4j.pb.tools.WeightedObject;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.sat4j.specs.TimeoutException;

/**
 * @author Daniela Loreti
 *
 */
public class BinPackingSolver {
	private OptToPBSATAdapter optimizer;
	private DependencyHelper<Item, String> helper;

	public BinPackingSolver(IPBSolver solver, int timeout) {
		optimizer = new OptToPBSATAdapter(new PseudoOptDecorator(solver));
		optimizer.setTimeout(timeout);
		optimizer.setVerbose(true);
		helper = new DependencyHelper<Item, String>(optimizer, false);
	}

	public List<Item> solve(List<Item> items, int nBin, int nObj) {
		
		@SuppressWarnings("unchecked")
		WeightedObject<Item>[] objective = new WeightedObject[items.size()];
		int index = 0;
		for (Item item : items) {
			objective[index] = newWO(item, item.getValue());
			index++;
		}
		

		try{
			for(int j=0;j<nObj;j++){
				@SuppressWarnings("unchecked")
				WeightedObject<Item>[] integrity = new WeightedObject[items.size()];
				index = 0;
				for (Item item : items) {
					if(index<nBin*nObj && j == index/nBin){
						integrity[index] = newWO(item, 1);
					}else integrity[index] = newWO(item, 0);
					index++;
				}
				helper.atLeast("IntegrityLeastObj"+j, BigInteger.valueOf(1),integrity);
//				System.err.println("\n*******IntegrityLeastObj"+j);
//				for (WeightedObject<Item> weightedObject : integrity) {
//					System.out.print(weightedObject.getWeight()+" ");
//				}
				helper.atMost("IntegrityMostObj"+j, BigInteger.valueOf(1),integrity);
			}

			for(int i=0;i<nBin;i++){
				@SuppressWarnings("unchecked")
				WeightedObject<Item>[] capacity = new WeightedObject[items.size()];
				index = 0;
				for (Item item : items) {
					if(index< (nBin*nObj)){
						if	( i == (index % nBin)){
							capacity[index] = newWO(item, item.getWeight());
						}else capacity[index] = newWO(item, 0);
					}else if (index-nBin*nObj == i){
						capacity[index] = newWO(item, item.getWeight());
					}else capacity[index] = newWO(item,0);
					index++;
				}
				helper.atMost("Capacity"+i, BigInteger.valueOf(0),capacity);

//				System.err.println("\n*******Capacity"+i);
//				for (WeightedObject<Item> weightedObject : capacity) {
//					System.out.print(weightedObject.getWeight()+" ");
//				}
			}



			List<Item> result = new ArrayList<Item>();
			helper.setObjectiveFunction(objective);
			if (helper.hasASolution()) {
				IVec<Item> sol = helper.getSolution();
				for (Iterator<Item> it = sol.iterator(); it.hasNext();) {
					result.add(it.next());
				}
			}
			return result;
		} catch (ContradictionException e) {
			return Collections.emptyList();
		} catch (TimeoutException e) {
			return Collections.emptyList();
		}
	}

	public long bestValue() {
		return helper.getSolutionCost().longValue();
	}

	public boolean isOptimal() {
		return optimizer.isOptimal();
	}

	public void printStats() {
		System.out.println(optimizer.toString(""));
		optimizer.printStat(new PrintWriter(System.out, true), "");
	}
}
