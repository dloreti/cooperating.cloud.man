/**
 * This package allow to compare the cnn solution (obtained through a certain policy) with the simplex solution
 * of the corresponding bin packing problem.
 * @since 1.0
 */
package cooperating.cloud.man.evaluate.simplex;
