/**
 * 
 */
package cooperating.cloud.man.evaluate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.HostStatus;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;
import cooperating.cloud.man.entities.VmDescriptor;

/**
 * Builds a "fair" cloud scenario in which every host is loaded by a certain percentage
 * @author Daniela Loreti
 *
 */
public class FairScenario extends Scenario{



	private int underServer_num;
	private int overServer_num;
	private List<Integer> underServers;
	private List<Integer> overServers;

	private int obtainedNumVM;

	public FairScenario(int nServer, int nVM, int nNeigh, double loadPercent) throws IllegalArgumentException{
		super( nServer, nVM, nNeigh, loadPercent);
		setOverServer_num(0);
		setUnderServer_num(1);
		loadRisingServersLists();
	}
	
	public FairScenario(int nServer, int nVM, int nNeigh, double loadPercent, int underServer_num, int overServer_num) throws IllegalArgumentException{
		super( nServer, nVM, nNeigh, loadPercent);
		setOverServer_num(overServer_num);
		setUnderServer_num(underServer_num);
		loadRisingServersLists();
	}

	private void loadRisingServersLists(){
		int risingServer_num=getUnderServer_num()+getOverServer_num();
		int divValue=Math.abs(getNumServer()/risingServer_num);
		this.setUnderServers(new ArrayList<Integer>());
		this.setOverServers(new ArrayList<Integer>());

		//int overInserted=0;
		//int underInserted=0;
		for(int i=1;i<=getNumServer();i++){
			if(i%divValue==0){
				if ( getUnderServers().size()<getUnderServer_num() 
						&& ( getOverServers().size()>=getOverServer_num() || 
						 (getOverServers().size()+getUnderServers().size())%2==0) ){
					getUnderServers().add(i-1);
				}else if (  getOverServers().size()<getOverServer_num() 
						&& ( getUnderServers().size()>=getUnderServer_num() || 
						 (getOverServers().size()+getUnderServers().size())%2==1) ){
					getOverServers().add(i-1);
				}
			}
		}
		
		if (!checkRisingServerNum())
			System.err.println("load rising servers failed: underServers="+getUnderServers().size()+" wanted="+underServer_num+"\n"+
					"overServers="+getOverServers().size()+" wanted="+overServer_num);
	}

	private boolean checkRisingServerNum(){
		return (getUnderServers().size()==underServer_num && getOverServers().size()==overServer_num);
	}
	@Override
	public void generateScenarioWithLoad() {
		obtainedNumVM=0;
		this.setHostCollection(new HashMap<Integer, IHostDescriptor>());
		for (int hostID = 0; hostID < getNumServer(); hostID++) {
			Map<Integer, IVmDescriptor> vmMap = new HashMap<Integer, IVmDescriptor>();
			IHostDescriptor host = new HostDescriptor(hostID, SimParameters.HOST_TOTALCPU, HostStatus.OK, vmMap);
			getHostCollection().put(hostID, host);

			int remainingServerLoad = (int) Math.round(SimParameters.HOST_TOTALCPU/100*getLoadPercent());
			//At least one server should be under loaded
			//if(getLoadPercent() >= SimParameters.HOST_THRESH_LOW && hostID< risingServer_num){
			if (getUnderServers().contains(hostID)){
				remainingServerLoad = (int) Math.round(SimParameters.HOST_TOTALCPU/100*(SimParameters.HOST_THRESH_LOW-30));
			}
			if (getOverServers().contains(hostID)){
				remainingServerLoad = (int) Math.round(SimParameters.HOST_TOTALCPU/100*(getLoadPercent()+10));
			}

			while(remainingServerLoad>0){
				int min = Math.min((int) Math.round(SimParameters.VM_TOTALCPU), remainingServerLoad);
				int vmLoad=0;
				if (remainingServerLoad<=SimParameters.VM_MINSIZE)
					vmLoad=SimParameters.VM_MINSIZE;   //sforo il vincolo di carico
				else
					vmLoad= (int) Math.round(Math.random()*(min-SimParameters.VM_MINSIZE) + SimParameters.VM_MINSIZE);

				VmDescriptor v = new VmDescriptor(obtainedNumVM, SimParameters.VM_TOTALCPU, vmLoad, hostID);
				getHostCollection().get(hostID).allocate(v);
				obtainedNumVM++;
				remainingServerLoad-=vmLoad;
			}
		}

		Utility.logPrintln("Scenario generation: obtainedNumVM="+obtainedNumVM+" wantedNumVM="+this.getNumVM());

		while (obtainedNumVM!=this.getNumVM()){
			boolean aChange=false;
			for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
				if (obtainedNumVM>this.getNumVM()){
					boolean melted=hostEntry.getValue().tryAMelt();
					if (melted) {
						aChange=true;
						obtainedNumVM--;
						if (obtainedNumVM==this.getNumVM()) break;
					}
				}else if (obtainedNumVM<this.getNumVM()){
					boolean splitted= hostEntry.getValue().tryASplit(obtainedNumVM);
					if (splitted) {
						aChange=true;
						obtainedNumVM++;
						if (obtainedNumVM==this.getNumVM()) break;
					}
				}
			}
			if (!aChange) break;
		}

		for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
			IHostDescriptor host = hostEntry.getValue();
			host.commitChanges();			
		}

		//control the final obtainedNumVM
		int obtainedNumVM =0;
		for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
			obtainedNumVM+=hostEntry.getValue().getCurrentVmMapSize();
		}
		System.out.println("obtainedNumVM = "+ obtainedNumVM);
		this.setNumVM(obtainedNumVM);
	}



	public int getObtainedNumVM() {
		return obtainedNumVM;
	}
	public void setObtainedNumVM(int obtainedNumVM) {
		this.obtainedNumVM = obtainedNumVM;
	}
	/**
	 * @return the underServer_num
	 */
	public int getUnderServer_num() {
		return underServer_num;
	}
	/**
	 * @param underServer_num the underServer_num to set
	 */
	public void setUnderServer_num(int underServer_num) {
		this.underServer_num = underServer_num;
	}
	/**
	 * @return the overServer_num
	 */
	public int getOverServer_num() {
		return overServer_num;
	}
	/**
	 * @param overServer_num the overServer_num to set
	 */
	public void setOverServer_num(int overServer_num) {
		this.overServer_num = overServer_num;
	}
	/**
	 * @return the underServers
	 */
	public List<Integer> getUnderServers() {
		return underServers;
	}
	/**
	 * @param underServers the underServers to set
	 */
	public void setUnderServers(List<Integer> underServers) {
		this.underServers = underServers;
	}
	/**
	 * @return the overServers
	 */
	public List<Integer> getOverServers() {
		return overServers;
	}
	/**
	 * @param overServers the overServers to set
	 */
	public void setOverServers(List<Integer> overServers) {
		this.overServers = overServers;
	}


}