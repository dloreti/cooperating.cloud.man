/**
 * 
 */
package cooperating.cloud.man.evaluate;

/**
 * @author Daniela Loreti
 *
 */
public class FairDamScenario extends FairScenario{


	
	public FairDamScenario(int nServer, int nVM, int nNeigh, double loadPercent) throws IllegalArgumentException{
		super( nServer, nVM, nNeigh, loadPercent);
	}

	public FairDamScenario(int nServer, int nVM, int nNeigh, double loadPercent, int underServer_num, int overServer_num) throws IllegalArgumentException{
		super( nServer, nVM, nNeigh, loadPercent,  underServer_num,  overServer_num);
	}
	
}
