/**
 * 
 */
package cooperating.cloud.man.com;

import cooperating.cloud.man.entities.HostMonitor;

/**
 * @author Daniela Loreti
 *
 */
public abstract class MasterClient implements Runnable{


//	private int id;
//	
//	/**
//	 * @param id
//	 */
//	public MasterClient(int id) {
//		super();
//		this.setId(id);
//	}
//
//	/**
//	 * @return the id
//	 */
//	public int getId() {
//		return this.id;
//	}
//
//	private void setId(int id) {
//		this.id = id;
//	}
	
	private HostMonitor hostMonitor;

	public MasterClient(HostMonitor hostMonitor){
		this.setHostMonitor(hostMonitor);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.hostMonitor.getId();
	}
	
	/**
	 * @return the hostMonitor
	 */
	public HostMonitor getHostMonitor() {
		return hostMonitor;
	}

	/**
	 * @param hostMonitor the hostMonitor to set
	 */
	private void setHostMonitor(HostMonitor hostMonitor) {
		this.hostMonitor = hostMonitor;
	}

}
