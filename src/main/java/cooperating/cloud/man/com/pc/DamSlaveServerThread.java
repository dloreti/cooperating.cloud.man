/**
 * 
 */
package cooperating.cloud.man.com.pc;

import java.io.StreamCorruptedException;

import cooperating.cloud.man.com.SlaveServer;
import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.HostMonitor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.ResultDocument;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * Runs the DAM protocol in the role of SlaveServer by means of a producer-consumer model.
 * @author Daniela Loreti
 *
 */
public class DamSlaveServerThread extends SlaveServer{


	private Channel channel;
	private DamEndPointDescriptor descriptor;

	/**
	 * @param hostMonitor
	 */
	public DamSlaveServerThread(Channel channel, HostMonitor hostMonitor) {
		super(hostMonitor);
		this.setChannel(channel);
		this.setDescriptor(channel.getSs());
	}


	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@SuppressWarnings("unused")
	@Override
	public void run() {
		String request;
		IHostDescriptor host = null;
		try {
			while(true){
				//receiving lock
				request = (String) channel.receiveFromMc();
				String clientID = request.split(" ")[1];
				request = request.split(" ")[0];
				if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG) 
					Utility.logPrintln(descriptor+": Start round with MC"+clientID+"...");
				if (request.equals("lock"))  {
					if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG) 
						Utility.logPrintln(descriptor+": Locked by MC"+clientID+"...");
					host = getHostMonitor().lockHost();
				}
				else{
					throw new StreamCorruptedException("Wrong message received from "+channel.getMc()+": "+request);
				}

				//sending status
				IHostDescriptor cloneHost = host;//new HostDescriptor(host);
				if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG)
					Utility.logPrintln(descriptor+": Sending "+cloneHost+" to "+channel.getMc());
				channel.sendToMc(cloneHost);
				ResultDocument.getInstance().addMessage();

				//receiving status or update-real-status command
				Object element = channel.receiveFromMc();

				
				if (element.getClass()==String.class){

					//receiving update-real-status command
					String message = (String) element;
					clientID = message.split(" ")[1];
					message = message.split(" ")[0];				
					if (message.equals("update-real-status"))  {
						if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG)
							Utility.logPrintln(descriptor+": current status update requested by"+ channel.getMc());
						host.commitChanges();
						break;
					}
					else{
						throw new StreamCorruptedException("Wrong message received from "+channel.getMc()+": "+message);
					}
				}else{
					//receiving future status and updating
					host = (HostDescriptor) element;
				}


				getHostMonitor().unlockHost(host);	
				if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG)
					Utility.logPrintln(descriptor+": Unlocked by "+channel.getMc()+"...");

				//rising condition check
				if (Integer.parseInt(clientID)!=host.getId() )   //non dovrebbe esser testato qui
					getHostMonitor().checkStartMasterClient();


			}//while
			getHostMonitor().unlockHost(host);	
			if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG)
				Utility.logPrintln(descriptor+": Unlocked by "+channel.getMc()+"...END of rounds with "+channel.getMc());
		}catch(	InterruptedException e){
			Utility.logPrintln(descriptor+": waiting on receive interrupted. - END -");//, e);
		} catch (StreamCorruptedException e) {
			Utility.logPrintln(descriptor+":  Communication error. - END -" ,e);
			System.exit(1);
		}

	}


	/**
	 * @return the descriptor
	 */
	public DamEndPointDescriptor getDescriptor() {
		return descriptor;
	}
	/**
	 * @param descriptor the descriptor to set
	 */
	private void setDescriptor(DamEndPointDescriptor descriptor) {
		this.descriptor = descriptor;
	}
	/**
	 * @return the channel
	 */
	public Channel getChannel() {
		return channel;
	}
	/**
	 * @param channel the channel to set
	 */
	private void setChannel(Channel channel) {
		this.channel = channel;
	}
}
