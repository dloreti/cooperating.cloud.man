/**
 * 
 */
package cooperating.cloud.man.com.pc;

/**
 * Models an end point of the DAM communication protocol
 * @author Daniela Loreti
 *
 */
public class DamEndPointDescriptor {

	private DamEndPointType type;
	private int id;

	/**
	 * @param type the type of this comunication endpoint
	 * @param id the id of the comunication endpoint
	 */
	public DamEndPointDescriptor(DamEndPointType type, int id) {
		super();
		this.setType(type);
		this.setId(id);
	}
	

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass()==this.getClass()){
			DamEndPointDescriptor damE = (DamEndPointDescriptor)obj;
			if (damE.getId()==this.getId() && damE.getType()==this.getType())
					return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return type+""+id;
		//return "DamEndPointDescriptor [type=" + type + ", id=" + id + "]";
	}
	/**
	 * @return the type
	 */
	public DamEndPointType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	private void setType(DamEndPointType type) {
		this.type = type;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	private void setId(int id) {
		this.id = id;
	}
	
	
}
