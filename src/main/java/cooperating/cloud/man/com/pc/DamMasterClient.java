/**
 * 
 */
package cooperating.cloud.man.com.pc;

import java.util.ArrayList;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import cooperating.cloud.man.com.MasterClient;
import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;
import cooperating.cloud.man.entities.VmDescriptor;
import cooperating.cloud.man.evaluate.ResultDocument;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * Runs the DAM protocol using a producer-consumer model
 * @author Daniela Loreti
 *
 */
public class DamMasterClient extends MasterClient{

	//private ArrayList<DamEndPointDescriptor> neigh;
	private SortedMap<Integer,DamEndPointDescriptor> neigh;
	private DamEndPointDescriptor descriptor;
	private SortedMap<Integer, Channel> channels;

	/**
	 * @param id
	 * @param allocator
	 * @param hostMonitor
	 */
	public DamMasterClient(DamHostMonitor hostMonitor, ArrayList<DamEndPointDescriptor> neigh) {
		super(hostMonitor);

		this.neigh = new TreeMap<>();
		for (DamEndPointDescriptor ss : neigh) {
			this.neigh.put(ss.getId(), ss);
		}
		this.descriptor = new DamEndPointDescriptor(DamEndPointType.MC, this.getId());
		this.channels= new TreeMap<>();
	}	

	public DamEndPointDescriptor getDamEndPointDescriptor(){
		return descriptor;
	}
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		//boolean interrupted=false;
		try{		
			int roundWithUnchangedStatus= 0;
			SortedMap<Integer, IHostDescriptor> neighHost = new TreeMap<>();
			SortedMap<Integer, IHostDescriptor> neighHostPast = new TreeMap<>();
			createConnections();
			while (true){


				neighHost = new TreeMap<>();
				if (SimParameters.FULLDEBUG ) Utility.logPrintln(descriptor+": NEIGHBORHOOD:");
				for (Entry<Integer, Channel> channelEntry  : channels.entrySet()) {	
					// sending lock
					String message= new String("lock "+this.getId());
					this.sendMessage(message, channelEntry.getValue());

					//receiving status
					HostDescriptor h = (HostDescriptor) this.receiveMessage(message, channelEntry.getValue());
					if (SimParameters.FULLDEBUG) Utility.logPrintln(h.toString());
					neighHost.put(channelEntry.getValue().getSs().getId(), h);
				}

				//campare past neighborhood collection
				if (neighHost.entrySet().equals(neighHostPast.entrySet())) { 
					roundWithUnchangedStatus ++;
					Utility.logPrintln(descriptor+": *** SAME ALLOCATION! roundWithUnchangedStatus="+roundWithUnchangedStatus);
				}
				else {
					if (roundWithUnchangedStatus!=0) Utility.logPrintln(descriptor+": *** NEIGHBORHOOD CHANGED! ");
					roundWithUnchangedStatus=0;
					if (neighHostPast.size()!=0 ) compareHostCollections(neighHost,neighHostPast);
					neighHostPast = new TreeMap<>();
					for (Entry<Integer, IHostDescriptor> neighHostEntry  : neighHost.entrySet()) {
						neighHostPast.put(neighHostEntry.getKey(), neighHostEntry.getValue().copy());
					}
				}

				//maxround reached?
				if (roundWithUnchangedStatus==SimParameters.MAX_roundWithUnchangedStatus){
					for (Entry<Integer, Channel> channelEntry  : channels.entrySet()) {	
						// sending update-real-status
						String message= new String("update-real-status "+this.getId());
						this.sendMessage(message, channelEntry.getValue());
					}
					break;
				}else{

					//optimization
					Utility.logPrintln(descriptor+": RUNNING OPTIMIZATION...");
					if (this.getHostMonitor().getAllocator().getHostCollection()==null || this.getHostMonitor().getAllocator().getHostCollection().size()==0)
						this.getHostMonitor().getAllocator().setHostCollection(neighHost);
					this.getHostMonitor().getAllocator().optimize();

					for (Entry<Integer, Channel> channelEntry  : channels.entrySet()) {								
						//send updated future status - works also like an unlock
						this.sendMessage(neighHost.get(channelEntry.getValue().getSs().getId()), channelEntry.getValue());
					}
				}

			}//while
		}catch(	InterruptedException e){
			Utility.logPrintln(descriptor+": waiting on receive interrupted. - END -");//, e);
			//interrupted=true;
		}catch(	Exception e){
			Utility.logPrintln(descriptor+": Errore irreversibile, il seguente: "+e.getMessage(),e);
			Utility.logPrintln(descriptor+": Chiudo tutto!");
			System.exit(1); 
		}finally{
			getHostMonitor().setMCrunning(false);
			closeConnections();
			Utility.logPrintln(descriptor+": - END -");
		}
	}

	/**
	 * @param neighHost
	 * @param neighHostPast
	 */
	@SuppressWarnings("unused")
	private void compareHostCollections(
			SortedMap<Integer, IHostDescriptor> neighHost,
			SortedMap<Integer, IHostDescriptor> neighHostPast) {

		if ((SimParameters.FULLDEBUG||SimParameters.MIGRATIONDEBUG)){
			for (Entry<Integer, IHostDescriptor> currentEntry : neighHost.entrySet()) {
				IHostDescriptor currentHost = currentEntry.getValue();
				IHostDescriptor pastHost = neighHostPast.get(currentEntry.getKey());
				if (!pastHost.equals(currentHost)){
					for (Entry<Integer, IVmDescriptor> vmEntry : currentHost.getFutureVmMap().entrySet()) {
						VmDescriptor vm = (VmDescriptor) vmEntry.getValue();
						if (!pastHost.getFutureVmMap().containsKey(vm.getId()))
							Utility.logPrintln(descriptor+": plan to move vm"+vm.getId()+" from host "+findHostForVm(neighHostPast,vm.getId())+
									" to "+currentHost.getId());
					}
				}
			}
		}
	}

	private Integer findHostForVm(SortedMap<Integer, IHostDescriptor> neighHost, int vmid){
		for (Entry<Integer, IHostDescriptor> hostE : neighHost.entrySet()) {
			if (hostE.getValue().getFutureVmMap().containsKey(vmid)) return hostE.getKey();
		}
		return null;
	}

	/**
	 * @param channel
	 */
	private void excludeNeigh(Channel channel) {
		DamEndPointDescriptor ss = channel.getSs();
		int id = ss.getId();
		if (null==this.neigh.remove(id))
			Utility.logPrintln("neigh.remove(id="+id+") NOT found");
		ChannelManager.getInstance().close(channel);
		this.channels.remove(channel.getSs().getId());
	}

	private void excludeNeigh(DamEndPointDescriptor neighbor) {
		int id = neighbor.getId();
		if (null==this.neigh.remove(id))
			Utility.logPrintln("neigh.remove(id="+id+") NOT found");
	}

	/**
	 * @throws InterruptedException 
	 * 
	 */
	private void createConnections() throws InterruptedException {
		for (Entry<Integer, DamEndPointDescriptor> ssEntry : neigh.entrySet()) {
			if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG) Utility.logPrintln(descriptor+": connecting to "+ssEntry.getValue());
			Channel channel = ChannelManager.getInstance().connect(ssEntry.getValue(), getDamEndPointDescriptor());
			channels.put(ssEntry.getValue().getId(),channel );
		}
	}

	private void closeConnections(){
		int size=neigh.size();
		for (int i = 0; i < size; i++) {
			DamEndPointDescriptor neighbor = neigh.get(neigh.firstKey());
			Channel ch = ChannelManager.getInstance().getChannelBetween(neighbor, this.getDamEndPointDescriptor());
			if (ch!=null)
				this.excludeNeigh(ch);
			else
				this.excludeNeigh(neighbor);
		}
		if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG)
			Utility.logPrintln(descriptor+": all connection closed. neigh.size()="+neigh.size());
	}

	private void sendMessage(Object message, Channel channel){
		if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG) Utility.logPrintln(descriptor+": Sending "+message+" to "+channel.getSs());
		channel.sendToSs(message);//(message, channel.getSs());
		ResultDocument.getInstance().addMessage();
	}

	private Object receiveMessage(Object message, Channel channel) throws InterruptedException{
		if (SimParameters.FULLDEBUG) Utility.logPrintln(descriptor+": Receiving from "+channel.getSs());
		Object object = null;
		object = channel.receiveFromSs();//( channel.getSs());
		ResultDocument.getInstance().addMessage();
		return object;
	}
}

