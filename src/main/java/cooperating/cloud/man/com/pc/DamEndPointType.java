/**
 * 
 */
package cooperating.cloud.man.com.pc;

/**
 * The type of the endpoints of DAM protocol: MC for MasterClient, SS for SlaveServer
 * @author Daniela Loreti
 *
 */
public enum DamEndPointType {
	SS, MC;
}
