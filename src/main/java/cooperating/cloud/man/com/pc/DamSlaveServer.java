/**
 * 
 */
package cooperating.cloud.man.com.pc;

import java.util.ArrayList;

import cooperating.cloud.man.com.SlaveServer;
import cooperating.cloud.man.entities.HostMonitor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * Answers to MasterClients connections requests and runs a dedicated SlaveServer thread to handle the DAM protocol interacion.
 * @author Daniela Loreti
 *
 */
public class DamSlaveServer extends SlaveServer{

	private DamEndPointDescriptor descriptor;

	/**
	 * @param hostMonitor
	 */
	public DamSlaveServer(HostMonitor hostMonitor) {
		super(hostMonitor);
		this.setDescriptor(new DamEndPointDescriptor(DamEndPointType.SS, this.getId()));
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@SuppressWarnings("unused")
	@Override
	public void run() {
		if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG) 
			Utility.logPrintln(descriptor+": SlaveServer Start!");

		try {
			IHostDescriptor host = this.getHostMonitor().lockHost();
			if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG)
				Utility.logPrintln(descriptor+": SlaveServer: avviato. CPU="+host.getCurrentUsedCPUPercent()+"%. Status: "+host.getCurrentStatus()+".");
			this.getHostMonitor().unlockHost(host);  //setta a futurechanged FALSE!!!
			getHostMonitor().checkStartMasterClient();
		}
		catch (Exception e) {
			Utility.logPrintln(descriptor+": SlaveServer Unable to start the MasterClient. - END -"  ,e);
			System.exit(2);
		}


		try {
			ArrayList<Thread> slaveServerThreads = new ArrayList<>();

			while (true) {
				Channel channel = ChannelManager.getInstance().accept(descriptor);
				if (channel==null) break;
				if (SimParameters.FULLDEBUG || SimParameters.PROTOCOLDEBUG) Utility.logPrintln(descriptor+": SlaveServer Connection accepted. " + channel );
				Thread t = new Thread(new DamSlaveServerThread(channel, getHostMonitor()));
				slaveServerThreads.add(t);
				t.start();

			} // while (true)
			ChannelManager.getInstance().closeUncon(descriptor);
			Utility.logPrintln(descriptor+": SlaveServer - END -");

		}catch(	InterruptedException e){
			Utility.logPrintln(descriptor+": SlaveServer waiting on receive interrupted. - END -");//, e);
		}catch (Exception e) {
			Utility.logPrintln(descriptor+": SlaveServer Execution Error. - END -",e);
			System.exit(1);
		}
	}

	/**
	 * @return the descriptor
	 */
	public DamEndPointDescriptor getDescriptor() {
		return descriptor;
	}

	/**
	 * @param descriptor the descriptor to set
	 */
	private void setDescriptor(DamEndPointDescriptor descriptor) {
		this.descriptor = descriptor;
	}

}
