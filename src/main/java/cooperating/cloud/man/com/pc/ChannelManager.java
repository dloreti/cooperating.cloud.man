/**
 * 
 */
package cooperating.cloud.man.com.pc;

import java.util.ArrayList;

import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * Singleton object to allow comunication between all threads of the simulation.
 * The object contains all the channels currently activated.
 * @author Daniela Loreti
 *
 */
public class ChannelManager {
	private static ChannelManager instance;
	private ArrayList<Channel> channels = null;

	private ChannelManager(){
		channels = new ArrayList<>();
		//		conditions = new TreeMap<>();
		//		locks = new TreeMap<>();
	}

	public static ChannelManager getInstance(){
		if (instance== null)
			instance = new ChannelManager();
		return instance;
	}
	
	public int getChannelSize(){
		return channels.size();
	}
	
	

	@Override
	public String toString() {
		String theManager="ChannelManager channels:\n";
		for (Channel ch : channels) {
			theManager+=ch.toString()+"\n";
		}return theManager;
	}

	/**
	 * @param ss
	 * @param mc
	 * @return the channel between the specified SlaverServer and MasterClient, or null if does not exists
	 */
	public Channel getChannelBetween(DamEndPointDescriptor ss, DamEndPointDescriptor mc) {
		Channel temp = new Channel(ss, mc);
		for (int i = 0; i < channels.size(); i++) {
			
			if ( temp.equals(channels.get(i))) {
				return channels.get(i);
			}
		}
		return null;
	}


	/** Creates the channel to the specified ss and waits for connection requests
	 * @param ss
	 * @return the newly created channel
	 * @throws InterruptedException 
	 */
	public Channel accept(DamEndPointDescriptor ss) throws InterruptedException{
		Channel ch  = getUnacceptedChannel(ss);
		if (ch==null) {
			ch=addChannel(ss);
		}
		boolean signaled = ch.accept();
		if (signaled)
			return ch;
		else return null;
	}

	public Channel connect(DamEndPointDescriptor ss, DamEndPointDescriptor mc) throws InterruptedException{
		boolean set = false;
		while(!set){
			Channel ch = getUnconnectedChannel(ss);
			if (ch==null){
				ch=addChannel(ss, mc);
			}
			set=ch.connect(mc);
			return ch;
		}
		return null;
	}

	private synchronized Channel addChannel(DamEndPointDescriptor ss, DamEndPointDescriptor mc){
		Channel ch=new Channel(ss, mc);
		channels.add(ch);
		//Utility.logPrintln(mc+": Aggiunto "+ch);
		return ch;
	}
	private synchronized Channel addChannel(DamEndPointDescriptor ss){
		Channel ch=new Channel(ss); 
		channels.add(ch);
		//Utility.logPrintln(ss+": Aggiunto "+ch);
		return ch;
	}

	private synchronized Channel getUnacceptedChannel(DamEndPointDescriptor ss){
		for (Channel channel : channels) {
			if (!channel.isSsAccepted() && channel.getSs().equals(ss)){
				return channel;
			}
		}
		return null;
	}
	/** 
	 * @param ss
	 * @return a channel to the specified SlaveServer, or null if it does not exsits
	 */
	private synchronized Channel getUnconnectedChannel(DamEndPointDescriptor ss){
		for (Channel channel : channels) {
			if(channel.getSs().equals(ss) &&
					!channel.isMcConnected()){
				return channel;
			}
		}
		return null;
	}


	public synchronized boolean close(Channel channel){
		if (SimParameters.FULLDEBUG) Utility.logPrintln("Closing "+channel);
		return this.channels.remove(channel);
	}
	
	public synchronized boolean closeUncon(DamEndPointDescriptor ss){
		Channel foundCh=getUnconnectedChannel(ss);
		if (foundCh==null) return false;
		return this.channels.remove(foundCh);
	}
}
