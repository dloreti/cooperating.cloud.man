/**
 * 
 */
package cooperating.cloud.man.com.pc;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.sun.corba.se.impl.io.TypeMismatchException;

import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * Model a bidirectional channel for the comunication between a SlaveServer and a MasterClient. 
 * The channel is implemented with two FIFO queue by means of locks and conditions to allow thread synchronization.
 * @author Daniela Loreti
 *
 */
public class Channel {


	private DamEndPointDescriptor ss=null;
	private DamEndPointDescriptor mc=null;
	private LinkedList<Object> messagequeueToMc = new LinkedList<Object>();
	private LinkedList<Object> messagequeueToSs = new LinkedList<Object>();

	//	private int waitingOnReceive = 0;
	private Lock lockToSs = new ReentrantLock(); 
	private Condition condToSs = lockToSs.newCondition();
	private Lock lockToMc = new ReentrantLock(); 
	private Condition condToMc = lockToMc.newCondition();

	private Lock connectionLock = new ReentrantLock();
	private Condition mcConn = connectionLock.newCondition();
	private boolean mcConnected=false;
	private Condition ssConn = connectionLock.newCondition();
	private boolean ssAccepted=false;

	/**
	 * Called by the SlaveServer
	 * @param ss the SlaveServer
	 */
	public Channel(DamEndPointDescriptor ss) {
		super();
		try {
			this.setSs(ss);
			if (SimParameters.FULLDEBUG) Utility.logPrintln(ss+": creating the channel..."+this);
		} catch (Exception e) {
			Utility.logPrintln(ss+": Exception accurred while creating the channel. - END -",e);
			System.exit(1);
		}
	}

	/**
	 * Called by the MasterClient
	 * @param ss the SlaveServer
	 * @param mc the MasterClient
	 */
	public Channel(DamEndPointDescriptor ss, DamEndPointDescriptor mc) {
		super();
		try {
			this.setSs(ss);
			this.setMc(mc);
			if (SimParameters.FULLDEBUG) Utility.logPrintln(mc+": creating the channel..."+this);
		} catch (Exception e) {
			Utility.logPrintln(mc+": Exception accurred while creating the channel between "+ss+" and "+mc+". - END -",e);
			System.exit(1);
		}
		//this.accept();
	}

	public boolean accept() throws InterruptedException{
		connectionLock.lock();
		boolean signaled = true;
		try	{  
			ssAccepted=true;
			ssConn.signalAll();
			while ( !mcConnected && signaled){ 
				if (SimParameters.FULLDEBUG) Utility.logPrintln(ss +": SlaveServer waiting for new connections...");
				signaled = mcConn.await(SimParameters.SERVERSOCKET_TO,TimeUnit.MILLISECONDS);
			}
			if (SimParameters.FULLDEBUG) Utility.logPrintln(ss +": SlaveServer the "+this+" is now accepted and ready.");

		} finally{
			connectionLock.unlock();
		}
		return signaled;
	}

	public boolean connect(DamEndPointDescriptor mc) throws InterruptedException{
		connectionLock.lock();
		boolean set=false;
		try	{  
			set = setMc(mc);
			if (!set) 
				return set;
			mcConnected=true;
			setMc(mc);
			mcConn.signalAll();
			while ( !ssAccepted){ 
				if (SimParameters.FULLDEBUG) Utility.logPrintln(mc +": waiting for "+getSs()+" to do an accept...");
				ssConn.await();
			}
			if (SimParameters.FULLDEBUG) Utility.logPrintln(mc +": the "+this+" is now connected and ready.");
		}  finally{
			connectionLock.unlock();
		}
		return set;
	}


	public boolean isMcConnected() {
		connectionLock.lock();
		try	{  
			return mcConnected;
		} finally{
			connectionLock.unlock();
		}
	}

	public boolean isSsAccepted() {
		return ssAccepted;
	}

	public void sendToMc(Object msg){
		lockToMc.lock();
		try{  
			messagequeueToMc.push(msg);
			if (SimParameters.FULLDEBUG) Utility.logPrintln(this.getSs()+": Message["+msg+"] to "+this.getMc()+" insert into the channel");
			condToMc.signalAll();
		} finally{
			lockToMc.unlock();
		}
	}


	public void sendToSs(Object msg){
		lockToSs.lock();
		try{  
			messagequeueToSs.push(msg);
			if (SimParameters.FULLDEBUG) Utility.logPrintln(this.getMc()+": Message["+msg+"] to "+this.getSs()+" insert into the channel");
			condToSs.signalAll();
		} finally{
			lockToSs.unlock();
		}
	}

	public Object receiveFromMc() throws InterruptedException{
		Object message=null;
		lockToSs.lock();
		try	{  
			while ( messagequeueToSs.size()==0){ 
				if (SimParameters.FULLDEBUG) Utility.logPrintln(ss +": waiting on message receive.");
				condToSs.await();
			}
			message = messagequeueToSs.pollLast();
		} finally{
			lockToSs.unlock();
		}
		return message;
	}

	public Object receiveFromSs() throws InterruptedException{
		Object message=null;
		lockToMc.lock();
		try	{  
			while ( messagequeueToMc.size()==0){ 
				if (SimParameters.FULLDEBUG) Utility.logPrintln(mc +": waiting on message receive.");
				condToMc.await();
			}
			message = messagequeueToMc.pollLast();
		} finally{
			lockToMc.unlock();
		}
		return message;
	}


	@Override
	public String toString() {
		return "Channel ["+mc+", " + ss + "]";
		//, messagequeueToMc=" + messagequeueToMc + ", messagequeueToSs=" + messagequeueToSs+ "]";
	}

	public DamEndPointDescriptor getSs() {
		return ss;
	}

	public void setSs(DamEndPointDescriptor ss) throws Exception {
		if (this.getSs()==null){
			if (ss.getType()==DamEndPointType.SS)
				this.ss = ss;
			else 
				throw new TypeMismatchException(ss+" is not a "+DamEndPointType.SS);
		}else 
			throw new Exception("Cannot set ss to "+ss+" because already set to "+getSs());
	}

	public DamEndPointDescriptor getMc() {
		return mc;
	}

	/**Sets the MasterClient endpoint of this channel. If it is already set, no action is performed and the method returns null
	 * @param mc
	 * @throws TypeMismatchException if the DamEndPointDescriptor parameter is not a MasterClient
	 */
	public boolean setMc(DamEndPointDescriptor mc) throws TypeMismatchException {
		if (this.getMc()==null || this.getMc().equals(mc)){
			if (mc.getType()==DamEndPointType.MC){
				this.mc = mc;
				return true;
			}else {
				throw new TypeMismatchException(mc+" is not a "+DamEndPointType.MC);
			}
		}else
			return false;
		//throw new Exception("Cannot set mc to "+mc+" because already set to "+getMc());
	}

	/* Two Channels are equals if they are defined between the same couple of end points
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj.getClass()==this.getClass()){
			Channel ch = (Channel)obj;
			if ((ch.getMc()==null && this.getMc()!=null) || (ch.getMc()!=null && this.getMc()==null) )
				return false;
			else if ( ( (ch.getMc()==null && this.getMc()==null) || ch.getMc().equals(this.getMc())  )  
					&& ch.getSs().equals(this.getSs())  )
				return true;
		}
		return false;
	}




}
