/**
 * 
 */
package cooperating.cloud.man.com.pc;

import java.util.ArrayList;

import cooperating.cloud.man.entities.HostMonitor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.Utility;
import cooperating.cloud.man.policy.Allocator;

/**
 * Extends the HostMonitor class to add informations about the neighborhood of DamEndPointDescriptor
 * @author Daniela Loreti
 *
 */
public class DamHostMonitor extends HostMonitor {

	private ArrayList<DamEndPointDescriptor> neigh;
	/**
	 * @param host
	 * @param allocator
	 */
	public DamHostMonitor(IHostDescriptor host, Allocator allocator, ArrayList<DamEndPointDescriptor> neigh) {
		super(host, allocator);
		this.setNeigh(neigh);
	}

	/* (non-Javadoc)
	 * @see cooperating.cloud.man.entities.HostMonitor#checkStartMasterClient()
	 */
	@Override
	public void checkStartMasterClient() throws InterruptedException {	
		IHostDescriptor host = lockHost();
		if (this.getAllocator().checkRisingCondition(host)  && !isMCrunning() ){
			setMCrunning(true);
			Utility.logPrintln("SS"+host.getId()+":  STARTING THE MASTERCLIENT!!!");
			Thread masterClient = new Thread(new DamMasterClient(this,neigh));
			masterClient.start();
		}
		unlockHost(host);
	}

	/**
	 * @return the neigh
	 */
	public ArrayList<DamEndPointDescriptor> getNeigh() {
		return neigh;
	}

	/**
	 * @param neigh the neigh to set
	 */
	private void setNeigh(ArrayList<DamEndPointDescriptor> neigh) {
		this.neigh = neigh;
	}


}
