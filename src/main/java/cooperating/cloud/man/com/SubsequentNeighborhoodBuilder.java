/**
 * 
 */
package cooperating.cloud.man.com;

import java.util.ArrayList;
import java.util.SortedMap;
import java.util.TreeMap;

import cooperating.cloud.man.com.pc.DamEndPointDescriptor;
import cooperating.cloud.man.com.pc.DamEndPointType;
import cooperating.cloud.man.com.socket.Slave;
/**
 * @author Daniela Loreti
 *
 */
public class SubsequentNeighborhoodBuilder implements INeighborhoodBuilder{

	private int nNeigh;
	private int startingID;
	private int nServer;
	
	/**
	 * @param nNeigh
	 * @param startingID
	 */
	public SubsequentNeighborhoodBuilder(int startingID, int nNeigh, int nServer) {
		super();
		this.nNeigh = nNeigh;
		this.startingID = startingID;
		this.nServer=nServer;
	}


	/* (non-Javadoc)
	 * @see cooperating.cloud.man.communicate.INeighborhoodBuilder#build()
	 */
	@Override
	public SortedMap<Integer, Slave> build() {
		SortedMap<Integer, Slave> neigh = new TreeMap<>();
		for(int i = startingID;i<startingID+nNeigh;i++){
			int vic = i % nServer;
			neigh.put(vic, new Slave(vic, "localhost", vic+5000));		
		}
		return neigh;
	}

	public ArrayList<Integer> buildChannels() {
		ArrayList<Integer> neigh = new ArrayList<>();
		for(int i = startingID;i<startingID+nNeigh;i++){
			@SuppressWarnings("unused")
			int vic = i % nServer;
			//ChannelManager.getInstance().add(new Channel(vic,DamSlaveServer.getClassIdenfier()));
			//neigh.add(vic);
		}
		return neigh;
	}


	/**
	 * @return
	 */
	public ArrayList<DamEndPointDescriptor> buildEndPointDescriptors() {
		ArrayList<DamEndPointDescriptor> ssneigh = new ArrayList<>();
		for(int i = startingID;i<startingID+nNeigh;i++){
			int vic = i % nServer;
			ssneigh.add(new DamEndPointDescriptor(DamEndPointType.SS, vic));
			//ChannelManager.getInstance().add(new Channel(vic,DamSlaveServer.getClassIdenfier()));
			//neigh.add(vic);
		}
		return ssneigh;
	}
	
}
