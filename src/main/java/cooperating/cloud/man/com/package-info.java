/**
 * This is the core package for the ccn protocol implementation. 
 * The contained objects implement the roles of the protocol communication: a Slave always waiting for status
 * requests and updates, and a Master thrown by a certain condition to collect information about the state of
 * the neighborhood and eventually updates the neighborhood state according to the optimization policy.
 * @since 1.0
 */
package cooperating.cloud.man.com;
