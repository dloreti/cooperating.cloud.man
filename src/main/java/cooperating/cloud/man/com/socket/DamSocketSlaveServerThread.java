/**
 * 
 */
package cooperating.cloud.man.com.socket;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.evaluate.ResultDocument;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * @author Daniela Loreti
 *
 */
public class DamSocketSlaveServerThread  extends SocketSlaveServerThread {


	/**
	 * @param clientSocket
	 * @param hostMonitor
	 */
	public DamSocketSlaveServerThread(Socket clientSocket, SocketHostMonitor hostMonitor) {
		super(clientSocket, hostMonitor);
	}
	
	@Override
	public void run() {

		String request;
		try {
			Thread.sleep(200);
			outSock = new ObjectOutputStream(clientSocket.getOutputStream());
			inSock = new ObjectInputStream(clientSocket.getInputStream());

			//receiving lock
			request = (String) inSock.readObject();
			String clientID = request.split(" ")[1];
			request = request.split(" ")[0];
			if (SimParameters.FULLDEBUG) Utility.logPrintln("ST"+id+": Start round with MC"+clientID+"...");

			if (request.equals("lock"))  {
				if (SimParameters.FULLDEBUG) Utility.logPrintln("ST"+id+": Locked by MC"+clientID+"...");
				host = hostMonitor.lockHost();
			}
			else{
				throw new Exception("Wrong message received from MC"+clientID+": "+request);
			}

			//sending status
			Utility.logPrintln("ST"+id+": Trasmissione di stato a MC"+clientID+"...");
			outSock.writeObject(host);
			ResultDocument.getInstance().addMessage();

			//receiving status or update-real-status command
			Object element = inSock.readObject();

			if (element.getClass()==String.class){
			
				//receiving update-real-status command
				String message = (String) element;
				clientID = message.split(" ")[1];
				message = message.split(" ")[0];				
				if (message.equals("update-real-status"))  {
					Utility.logPrintln("ST"+id+": Update dello stato corrente su richiesta di MC"+clientID+"...");
					host.commitChanges();
				}
				else{
					throw new Exception("Wrong message received from MC"+clientID+": "+message);
				}
			}else{
				//receiving future status and updating
				host = (HostDescriptor) element;
			}

			
			//sending update-ack - works like an unlocked
			//outSock.writeObject(new String("update-ack"));
			//ResultDocument.getInstance().addMessage();
			
			if (SimParameters.FULLDEBUG) Utility.logPrintln("ST"+id+": Unlocked by MC"+clientID+"...");
			hostMonitor.unlockHost(host);	
			
			//rising condition check
			if (Integer.parseInt(clientID)!=host.getId()) 
				hostMonitor.checkStartMasterClient();


			// closing socket
			clientSocket.shutdownInput();
			clientSocket.shutdownOutput();
			if (SimParameters.FULLDEBUG) Utility.logPrintln("ST"+id+": Terminata connessione con " + clientSocket+ ". END of round with MC"+clientID+"...");
			clientSocket.close();				//???????
		}catch(SocketTimeoutException ste){
			Utility.logPrintln("ST"+id+": Timeout scattato: ",ste);
			Utility.logPrintln("ST"+id+":Termino.");
			System.exit(2);
		}     
		catch (Exception e) {
			Utility.logPrintln("\nST"+id+": Problemi durante la comunicazione con il MasterClient: " ,e);
			Utility.logPrintln("ST"+id+":Termino.");
			System.exit(1);
		} 
	}


}
