/**
 * 
 */
package cooperating.cloud.man.com.socket;

import java.util.SortedMap;

import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.HostMonitor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.Utility;
import cooperating.cloud.man.policy.AbstractAllocator;

/**
 * Extends the host monitor to enable socket based interaction in the neighborhood
 * @author Daniela Loreti
 *
 */
public class SocketHostMonitor extends HostMonitor{

	private SortedMap<Integer, Slave> neigh;

	public SocketHostMonitor(HostDescriptor host, AbstractAllocator allocator, SortedMap<Integer, Slave> neigh){
		super(host, allocator);
		this.neigh=neigh;
	} 

		
	
	public synchronized void checkStartMasterClient() throws InterruptedException{
		IHostDescriptor host = lockHost();
		if (this.getAllocator().checkRisingCondition(host)   && (!isMCrunning())){
			this.setMCrunning(true);
			Utility.logPrintln("ST"+host.getId()+":  STARTING THE MASTERCLIENT!!!");
			Thread masterClient = new Thread(new DamSocketMasterClient(this));
			masterClient.start();
		}
		unlockHost(host);
	}

	/**
	 * @return the neighborhood
	 */
	public SortedMap<Integer, Slave> getNeigh() {
		return neigh;
	}


}
