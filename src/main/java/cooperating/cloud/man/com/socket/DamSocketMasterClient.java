/**
 * 
 */
package cooperating.cloud.man.com.socket;

import java.net.SocketTimeoutException;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.ResultDocument;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * @author Daniela Loreti
 *
 */
public class DamSocketMasterClient extends SocketMasterClient {

	/**
	 * @param hostMonitor
	 */
	public DamSocketMasterClient(SocketHostMonitor hostMonitor) {
		super( hostMonitor);
	} 
	
	@Override
	public void run() {
		try{		
			int roundWithUnchangedStatus= 0;
			SortedMap<Integer, IHostDescriptor> neighHost = new TreeMap<>();
			SortedMap<Integer, IHostDescriptor> neighHostPast = new TreeMap<>();
			while (true){

				createAllSocketsAndStreams();

				String message= new String("lock "+this.getId());
				neighHost = new TreeMap<>();
				if (SimParameters.FULLDEBUG) Utility.logPrintln("MC"+getId()+": ALLOCAZIONE VICINATO:");
				for (Entry<Integer, Slave> neighEntry  : neigh.entrySet()) {	
					try{

						// sending lock
						Utility.logPrintln("MC"+getId()+": Invio "+message+" a SS"+neighEntry.getKey());
//						Utility.logPrintln("MC"+getId()+": Invio "+message+" a " + neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort()+"... sulla socket: key: "+
//								neighEntry.getKey()+" value: "+neighSocket.get(neighEntry.getKey()) );
						neighOutSocket.get(neighEntry.getKey()).writeObject(new String(message));
						ResultDocument.getInstance().addMessage();
						
						//receiving status
						HostDescriptor h = (HostDescriptor) neighInSocket.get(neighEntry.getKey()).readObject();
						if (SimParameters.FULLDEBUG) Utility.logPrintln(h.toString());
						neighHost.put(neighEntry.getKey(), h);

					}catch(SocketTimeoutException ste){
						Utility.logPrintln("MC"+getId()+": Timeout scattato: ",ste);
						excludeNeigh(neighEntry.getKey());
					}catch(Exception e){
						Utility.logPrintln("MC"+getId()+": Problemi nell'invio "+message+" a " + neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort()+" :",e);
						excludeNeigh(neighEntry.getKey());
					}
				}

				//campare past neighborhood collection
				//se uguale ad allocazione precedente => println msg e incrementa counter roundWithUnchangedStatus!!!
				if (neighHost.entrySet().equals(neighHostPast.entrySet())) { 
					roundWithUnchangedStatus ++;
					Utility.logPrintln("MC"+getId()+": *** ALLOCAZIONE IDENTICA ALLA PRECEDENTE! roundWithUnchangedStatus="+roundWithUnchangedStatus);
				}
				else {
					roundWithUnchangedStatus=0;
					neighHostPast = new TreeMap<>();
					for (Entry<Integer, IHostDescriptor> neighHostEntry  : neighHost.entrySet()) {
						neighHostPast.put(neighHostEntry.getKey(),neighHostEntry.getValue().copy());
					}
				}

				//maxround reached?
				if (roundWithUnchangedStatus==SimParameters.MAX_roundWithUnchangedStatus){
					// sending update-real-status
					message= new String("update-real-status "+this.getId());
					for (Entry<Integer, Slave> neighEntry  : neigh.entrySet()) {	
						try{
							Utility.logPrintln("MC"+getId()+": Invio "+message+" a SS"+neighEntry.getKey());
//							Utility.logPrintln("MC"+getId()+": Invio "+message+" a " + neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort()+"... sulla socket: key: "+
//									neighEntry.getKey()+" value: "+neighSocket.get(neighEntry.getKey()) );
							neighOutSocket.get(neighEntry.getKey()).writeObject(new String(message));
							ResultDocument.getInstance().addMessage();
						}
						catch(Exception e){
							Utility.logPrintln("MC"+getId()+": Problemi nell'invio "+message+" a " + neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort()+" :",e);
							excludeNeigh(neighEntry.getKey());
						}
					}
					break;
				}else{

					//optimization
					Utility.logPrintln("MC"+getId()+": OTTIMIZZAZIONE IN CORSO...");
					if (this.getHostMonitor().getAllocator().getHostCollection()==null || this.getHostMonitor().getAllocator().getHostCollection().size()==0)
						this.getHostMonitor().getAllocator().setHostCollection(neighHost);
					this.getHostMonitor().getAllocator().optimize();
					for (Entry<Integer, Slave> neighEntry  : neigh.entrySet()) {
						try{
							Utility.logPrintln("MC"+getId()+": Invio aggiornamento stato a SS"+neighEntry.getKey());
							//send updated future status - works also like an unlock
							neighOutSocket.get(neighEntry.getKey()).writeObject(neighHost.get(neighEntry.getKey()));
							ResultDocument.getInstance().addMessage();
							
							//receive update-ack - works also like an unlock-ack
							//esito = (String) neighInSocket.get(neighEntry.getKey()).readObject();
							//if (!esito.equals("update-ack")) throw new Exception("Wrong message received: "+esito);
						}
						catch(SocketTimeoutException ste){
							Utility.logPrintln("MC"+getId()+": Timeout scattato: "+ste.getStackTrace());
							excludeNeigh(neighEntry.getKey());
						}
						catch(Exception e){
							Utility.logPrintln("MC"+getId()+": Problemi nella ricezione dell'esito, i seguenti: ",e);
							excludeNeigh(neighEntry.getKey());
						}
					}
				}

//				if (roundWithUnchangedStatus==SimParameters.MAX_roundWithUnchangedStatus)
//					break;
			}//while
		}
		catch(Exception e){
			Utility.logPrintln("MC"+getId()+": Errore irreversibile, il seguente: "+e.getMessage(),e);
			Utility.logPrintln("MC"+getId()+": Chiudo!");
			System.exit(1); 
		}finally{
			getHostMonitor().setMCrunning(false);
		}
	}




}
