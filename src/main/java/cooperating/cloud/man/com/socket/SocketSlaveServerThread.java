/**
 * 
 */
package cooperating.cloud.man.com.socket;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.Utility;

/**
 * Implements the SlaveServer-side of the ccn protocol. 
 * @author Daniela Loreti
 */
class SocketSlaveServerThread implements Runnable{
	Socket clientSocket = null;
	ObjectInputStream inSock = null;
	ObjectOutputStream outSock = null;
	int id = -1;
	SocketHostMonitor hostMonitor = null;
	IHostDescriptor host = null;

	/**
	 * @param clientSocket
	 * @param serverSocket 
	 */
	public SocketSlaveServerThread(Socket clientSocket, SocketHostMonitor hostMonitor/*, ServerSocket serverSocket*/) {
		this.clientSocket = clientSocket;
		this.id = hostMonitor.getId();
		this.hostMonitor = hostMonitor;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		String request;
		try {
			outSock = new ObjectOutputStream(clientSocket.getOutputStream());
			inSock = new ObjectInputStream(clientSocket.getInputStream());
			/* ricezione richiesta di lock */
			request = (String) inSock.readObject();
			String clientID = request.split(" ")[1];
			request = request.split(" ")[0];
			Utility.logPrintln("ST"+id+": Start round with "+clientID+"...");

			String esito = "";
			if (request.equals("lock"))  {
				esito = "locked";
				host = hostMonitor.lockHost();
			}
			else esito = request+"???";

			Utility.logPrintln("ST"+id+": Locked by MC"+clientID+"...");

			// ritorno esito positivo del lock al client

			outSock.writeObject(new String(esito));

			request = (String) inSock.readObject();
			clientID = request.split(" ")[1];
			request = request.split(" ")[0];
			if (request.equals("getStatus"))  {
				//invio status
				//LogUtility.println("ST"+id+": Trasmissione di stato: "+ host);
				Utility.logPrintln("ST"+id+": Trasmissione di stato a "+clientID+"...");
				//ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
				outSock.writeObject(host);

				//ricezione status e aggiornamento
				//ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
				host = (HostDescriptor) inSock.readObject();
				//LogUtility.println("ST"+id+": Ricevuto aggiornamento di stato: "+ host);

			}else if (request.equals("updateCurrentStatus"))  {
				Utility.logPrintln("ST"+id+": Update dello stato corrente su richiesta di "+clientID+"...");
				host.commitChanges();
				// ritorno esito positivo del updateCurrentStatus al client
				outSock.writeObject(new String("updated"));
				Utility.logPrintln("ST"+id+": CPU="+host.getCurrentUsedCPUPercent()+"%. Status: "+host.getCurrentStatus()+".");

			}else{
				outSock.writeObject(new String(request+"???"));
			}
			//Se lo stato == OVER o UNDER lancio il MasterClient
			if (Integer.parseInt(clientID)!=host.getId()) 
				hostMonitor.checkStartMasterClient();


			/* ricezione richiesta di UNlock */
			request = (String)inSock.readObject();
			clientID = request.split(" ")[1];
			request = request.split(" ")[0];

			esito = "";
			if (request.equals("unlock"))  {
				esito = "unlocked";
				hostMonitor.unlockHost(host);
			}
			else esito = request+"???";

			Utility.logPrintln("ST"+id+": Unlocked by MC"+clientID+"...");

			// chiusura socket in downstream
			clientSocket.shutdownInput();
			// ritorno esito positivo al client
			outSock.writeObject(new String(esito));
			// chiudo la socket anche in upstream
			clientSocket.shutdownOutput();
			Utility.logPrintln("ST"+id+": Terminata connessione con " + clientSocket);
			//serverSocket.close();
			Utility.logPrintln("ST"+id+": End round with "+clientID+"...");

		}
		catch(SocketTimeoutException ste){
			Utility.logPrintln("ST"+id+": Timeout scattato: ",ste);
			Utility.logPrintln("ST"+id+":Termino.");
			System.exit(2);
		}     
		catch (Exception e) {
			Utility.logPrintln("\nST"+id+": Problemi durante la comunicazione con il MasterClient: " ,e);
			Utility.logPrintln("ST"+id+":Termino.");
			System.exit(1);
		} 

	}


}
