/**
 * 
 */
package cooperating.cloud.man.com.socket;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import cooperating.cloud.man.com.SlaveServer;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * Simulate a demon always running on a physical node. 
 * It listens on a port and create a new SlaveServerThread to handle each request.
 * @author Daniela Loreti
 */
public class DamSocketSlaveServer extends SlaveServer{
	
	/**
	 * @param host
	 * @param neigh
	 */
	public DamSocketSlaveServer(SocketHostMonitor hostMonitor) {
		super(hostMonitor);
	}



	/**
	 * @param args [serverPort]
	 */
	public void run(){
		int id = getHostMonitor().getId();
		int port= id+5000;

		/* preparazione socket e in/out stream */
		ServerSocket serverSocket = null;
		try {
			InetAddress address = InetAddress.getLoopbackAddress();
			serverSocket = new ServerSocket(port,2,address);
			serverSocket.setReuseAddress(true);
			serverSocket.setSoTimeout(SimParameters.SERVERSOCKET_TO);
			IHostDescriptor host = this.getHostMonitor().lockHost();
			Utility.logPrintln("SS"+id+": SlaveServer: avviato. CPU="+host.getCurrentUsedCPUPercent()+"%. Status: "+host.getCurrentStatus()+".");
			this.getHostMonitor().unlockHost(host);
		}
		catch (Exception e) {
			Utility.logPrintln("SS"+id+": Problemi nella creazione della server socket: Termino..." ,e);
			System.exit(2);
		}

		//SocketHostMonitor hostMonitor = new SocketHostMonitor(host, new BestFitAllocator(), neigh);
		try {
			getHostMonitor().checkStartMasterClient();
		} catch (InterruptedException e1) {
			Utility.logPrintln("SS"+id+": Termino...",e1);
			System.exit(1);
		}

		try {
			ArrayList<Thread> slaveServerThreads = new ArrayList<>();

			while (true) {
				Socket clientSocket = null;
				if (SimParameters.FULLDEBUG) Utility.logPrintln("SS"+id+": In attesa di richieste...");
				try {
					clientSocket = serverSocket.accept();
					clientSocket.setSoTimeout(240000);
					if (SimParameters.FULLDEBUG) Utility.logPrintln("SS"+id+": Connessione accettata: " + clientSocket );
					Thread t = new Thread(new DamSocketSlaveServerThread(clientSocket, (SocketHostMonitor) getHostMonitor()));
					slaveServerThreads.add(t);
					t.start();
				}
				catch (SocketTimeoutException te) {
					Utility.logPrintln("SS"+id+": Non ho ricevuto nulla dal client per "+SimParameters.SERVERSOCKET_TO/1000+" sec., interrompo "
							+ "la comunicazione e accetto nuove richieste.");
					break; 
				}
				catch (Exception e) {
					Utility.logPrintln("SS"+id+": Problemi nella accettazione della connessione: Termino..." ,e);
					serverSocket.close();
					System.exit(2);
				}
			} // while (true)
			serverSocket.close();
		}
		catch (Exception e) {
			Utility.logPrintln("SS"+id+": Errore irreversibile. Termino...",e);
			System.exit(1);
		}
		Utility.logPrintln("SS"+id+" - END -");

	} // run

}
