package cooperating.cloud.man.com.socket;

/**
 * Models a Slave interaction end-point that collects all the information to enable a socket based communication 
 * @author Daniela Loreti
 *
 */
public class Slave {

	private int id;
	private String hostname;
	private int port;
	
	public Slave(int id, String hostname, int port){
		this.setId(id);
		this.setHostname(hostname);
		this.setPort(port);
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the host name
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * @param hostname the host name to set
	 */
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
}
