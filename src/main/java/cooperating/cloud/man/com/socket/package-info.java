/**
 * This package contains all the classes to implement a socket-based comunication between DAM threads
 * @since 1.0
 */
package cooperating.cloud.man.com.socket;
