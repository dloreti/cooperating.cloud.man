/**
 * 
 */
package cooperating.cloud.man.com.socket;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.SortedMap;

import cooperating.cloud.man.com.MasterClient;
import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;


/**
 * Implements the ccn protocol of communication. Each MasterClient thread knows its neighborhood of nodes and
 * can collect informations from it. The protocol always include a lock phase, an optimization of the 
 * neiahborhood and and an unlock phase. 
 * @author Daniela Loreti 
 *
 */
public class SocketMasterClient extends MasterClient{
 

	protected SortedMap<Integer, Slave> neigh = new TreeMap<Integer, Slave>();
	protected SortedMap<Integer, Socket> neighSocket = new TreeMap<Integer, Socket>();
	protected SortedMap<Integer, ObjectInputStream> neighInSocket = new TreeMap<Integer, ObjectInputStream>();
	protected SortedMap<Integer, ObjectOutputStream> neighOutSocket = new TreeMap<Integer, ObjectOutputStream>();

	public SocketMasterClient(SocketHostMonitor hostMonitor){
		super(hostMonitor);
		this.neigh = hostMonitor.getNeigh();	
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		//hostMonitor.setMCrunning(true);

		try{
			int roundWithUnchangedStatus= 0;
			SortedMap<Integer, IHostDescriptor> neighHost = new TreeMap<>();
			while (roundWithUnchangedStatus<SimParameters.MAX_roundWithUnchangedStatus){
				createAllSocketsAndStreams();

				messageAllSendReceive("lock");

				neighHost = new TreeMap<>();
				if (SimParameters.FULLDEBUG) Utility.logPrintln("MC"+getId()+": ALLOCAZIONE VICINATO:");
				for (Entry<Integer, Slave> neighEntry  : neigh.entrySet()) {
					try{
						neighOutSocket.get(neighEntry.getKey()).writeObject(new String("getStatus "+getId()));
						if (SimParameters.FULLDEBUG) Utility.logPrintln("MC"+getId()+": Inviato getStatus a " + neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort()+"... ");

						//ObjectInputStream ois = new ObjectInputStream(neighSocket.get(neighEntry.getKey()).getInputStream());
						HostDescriptor h = (HostDescriptor) neighInSocket.get(neighEntry.getKey()).readObject();
						if (SimParameters.FULLDEBUG) Utility.logPrintln(h.toString());
						neighHost.put(neighEntry.getKey(), h);
					}
					catch(SocketTimeoutException ste){
						Utility.logPrintln("MC"+getId()+": Timeout scattato: ",ste);
						excludeNeigh(neighEntry.getKey());
					}
					catch(Exception e){
						Utility.logPrintln("MC"+getId()+": Problemi nell'invio/ricezione dello stato di "+ neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort()+
								", i seguenti: ",e);
						excludeNeigh(neighEntry.getKey());
					}
				}
				Utility.logPrintln("MC"+getId()+": OTTIMIZZAZIONE IN CORSO...");

				SortedMap<Integer, IHostDescriptor> neighHostPast = new TreeMap<>();
				for (Entry<Integer, IHostDescriptor> neighHostEntry  : neighHost.entrySet()) {
					neighHostPast.put(neighHostEntry.getKey(),neighHostEntry.getValue().copy());
				}

				if (this.getHostMonitor().getAllocator().getHostCollection()==null || this.getHostMonitor().getAllocator().getHostCollection().size()==0)
					this.getHostMonitor().getAllocator().setHostCollection(neighHost);
				this.getHostMonitor().getAllocator().optimize();

				if (SimParameters.FULLDEBUG) {
					Utility.logPrintln("MC"+getId()+": ESITO DELLA RIALLOCAZIONE:"); 
					for (Entry<Integer, IHostDescriptor> neighHostEntry  : neighHost.entrySet()) { Utility.logPrintln(neighHostEntry.getValue().toString());}
				}

				//se uguale ad allocazione precedente => println msg e incrementa counter roundWithUnchangedStatus!!!
				if (neighHost.entrySet().equals(neighHostPast.entrySet())) { 
					roundWithUnchangedStatus ++;
					Utility.logPrintln("MC"+getId()+": *** ALLOCAZIONE IDENTICA ALLA PRECEDENTE! roundWithUnchangedStatus="+roundWithUnchangedStatus);
				}
				else {
					roundWithUnchangedStatus=0;
				}


				//invio status per ogni slave
				for (Entry<Integer, Slave> neighEntry  : neigh.entrySet()) {
					try{
						//ObjectOutputStream oos = new ObjectOutputStream(neighSocket.get(neighEntry.getKey()).getOutputStream());
						neighOutSocket.get(neighEntry.getKey()).writeObject(neighHost.get(neighEntry.getKey()));
					}
					catch(SocketTimeoutException ste){
						Utility.logPrintln("MC"+getId()+": Timeout scattato: "+ste.getStackTrace());
						excludeNeigh(neighEntry.getKey());
					}
					catch(Exception e){
						Utility.logPrintln("MC"+getId()+": Problemi nella ricezione dell'esito, i seguenti: ",e);
						excludeNeigh(neighEntry.getKey());
					}
				}

				messageAllSendReceive("unlock");
				//Thread.sleep(1000);
				// tutto ok, pronto per nuova richiesta

			}// while roundWithUnchangedStatus

			//aggiorno lo stato CORRENTE di tutti i vicini
			createAllSocketsAndStreams();
			messageAllSendReceive("lock");
			messageAllSendReceive("updateCurrentStatus");
			messageAllSendReceive("unlock");

		}
		catch(Exception e){
			Utility.logPrintln("MC"+getId()+": Errore irreversibile, il seguente: "+e.getMessage(),e);
			Utility.logPrintln("MC"+getId()+": Chiudo!");
			System.exit(1); 
		}
		Utility.logPrintln("MC"+getId()+": Termino!");	
		//hostMonitor.setMCrunning(false);
	} // run

	public String messageAllSendReceive(String message) throws InterruptedException, IOException{
		// trasmissione del msg
		message=message+" "+this.getId();

		String esito ="";
		for (Entry<Integer, Slave> neighEntry  : neigh.entrySet()) {	
			try{
				Utility.logPrintln("MC"+getId()+": Invio "+message+" a " + neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort()+"... sulla socket: key: "+
						neighEntry.getKey()+" value: "+neighSocket.get(neighEntry.getKey()) );
				neighOutSocket.get(neighEntry.getKey()).writeObject(new String(message));
			}
			catch(Exception e){
				Utility.logPrintln("MC"+getId()+": Problemi nell'invio "+message+" a " + neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort()+" :",e);
				excludeNeigh(neighEntry.getKey());
			}

			// ricezione esito
			try{
				esito = (String)neighInSocket.get(neighEntry.getKey()).readObject();
				//LogUtility.println("MC"+id+": Esito trasmissione: " + esito);
			}
			catch(SocketTimeoutException ste){
				Utility.logPrintln("MC"+getId()+": Timeout scattato: "+ste.getStackTrace());
				excludeNeigh(neighEntry.getKey());
			}
			catch(Exception e){
				Utility.logPrintln("MC"+getId()+": Problemi nella ricezione dell'esito, i seguenti: ",e);
				excludeNeigh(neighEntry.getKey());
			}
		}
		return esito;
	}

	public void excludeNeigh(int id) throws IOException{
		neighSocket.get(getId()).close();
		neighInSocket.get(getId()).close();
		neighOutSocket.get(getId()).close();

		neigh.remove(getId());
		neighSocket.remove(getId());
		neighInSocket.remove(getId());
		neighOutSocket.remove(getId());
		Utility.logPrintln(	"******************************************\n" +
				"MC"+this.getId()+": Escluso il server SS"+getId()+ "\n"+
				"******************************************\n");
		System.exit(1);
	}

	public void createAllSocketsAndStreams() throws InterruptedException{
		// creazione socket
		for (Entry<Integer, Slave> neighEntry  : neigh.entrySet()) {
			boolean socketCreated =false;
			while (!socketCreated){
				try{

					Socket socket = new Socket(neighEntry.getValue().getHostname(),
							neighEntry.getValue().getPort() );
					socket.setSoTimeout(240000); // setto il timeout per non bloccare indefinitamente il client
					if (SimParameters.FULLDEBUG) Utility.logPrintln("MC"+getId()+": Creata la socket: " + socket);
					neighSocket.put(neighEntry.getKey(), socket  );
					socketCreated = true;
				}catch(ConnectException ce){
					Utility.logPrintln("MC"+getId()+": SlaveServer "+neighEntry.getValue().getHostname()+
							" non ancora attivo sulla porta "+neighEntry.getValue().getPort());
					Thread.sleep(2000);
					continue;
				}catch(Exception e){
					Utility.logPrintln("MC"+getId()+": Problemi nella creazione della socket: "+neighEntry.getValue().getHostname()+":"+
							neighEntry.getValue().getPort()+". Eliminazione dell'host dal vicinato." ,e);
					neigh.remove(neighEntry.getKey());
					break; 
				}
			}

			try{
				Thread.sleep(200);
				ObjectInputStream inSock = new ObjectInputStream(neighSocket.get(neighEntry.getKey()).getInputStream());
				neighInSocket.put(neighEntry.getKey(), inSock);	
				ObjectOutputStream outSock = new ObjectOutputStream(neighSocket.get(neighEntry.getKey()).getOutputStream());
				neighOutSocket.put(neighEntry.getKey(), outSock);							
			}
			catch(IOException e){
				Utility.logPrintln("MC"+getId()+": Problemi nella creazione degli stream  su socket: "+neighEntry.getValue().getHostname()+":"+
						neighEntry.getValue().getPort()+". Eliminazione dell'host dal vicinato." ,e);
				neigh.remove(neighEntry.getKey());
				neighSocket.remove(neighEntry.getKey());
				neighOutSocket.remove(neighEntry.getKey());
				neighInSocket.remove(neighEntry.getKey());
				break; 
			}	
			if (SimParameters.FULLDEBUG) Utility.logPrintln("MC"+getId()+": Connesso a "+neighEntry.getValue().getHostname()+":"+neighEntry.getValue().getPort());		
		}
	}

}
