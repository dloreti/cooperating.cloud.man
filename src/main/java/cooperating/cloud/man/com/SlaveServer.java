/**
 * 
 */
package cooperating.cloud.man.com;

import cooperating.cloud.man.entities.HostMonitor;

/**
 * @author Daniela Loreti
 *
 */
public abstract class SlaveServer implements Runnable{
	private HostMonitor hostMonitor;

	public SlaveServer(HostMonitor hostMonitor){
		this.setHostMonitor(hostMonitor);
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return this.hostMonitor.getId();
	}
	

	/**
	 * @return the hostMonitor
	 */
	public HostMonitor getHostMonitor() {
		return hostMonitor;
	}
	/**
	 * @param hostMonitor the hostMonitor to set
	 */
	private void setHostMonitor(HostMonitor hostMonitor) {
		this.hostMonitor = hostMonitor;
	}

}
