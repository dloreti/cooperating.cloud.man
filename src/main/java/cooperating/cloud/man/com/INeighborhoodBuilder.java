/**
 * 
 */
package cooperating.cloud.man.com;


import java.util.SortedMap;

import cooperating.cloud.man.com.socket.Slave;

/**
 * @author Daniela Loreti
 *
 */
public interface INeighborhoodBuilder {
	
	public SortedMap<Integer, Slave> build();
	
}
