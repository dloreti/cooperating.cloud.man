/**
 * 
 */
package cooperating.cloud.man.entities;

import java.util.Map;

import org.w3c.dom.Element;

/**
 * @author Daniela Loreti
 *
 */
public interface IHostDescriptor extends DoubleStateEntity{

	public boolean allocate(IVmDescriptor vm);
	
	public boolean deallocate(IVmDescriptor vm);
	
	public Map<Integer, IVmDescriptor> getFutureVmMap();
	public void resetFuture();
	public Double getFutureAvailableCPUIfAllocate(IVmDescriptor vm);
	public HostStatus getFutureStatus() ;
	public double getFutureUsedCPU();
	public int getId();
	abstract void setId(int id);

	public Map<Integer, IVmDescriptor> getCurrentVmMap() ;
	/**
	 * @param rootElement
	 */
	public void toXml(Element rootElement);

	/**
	 * @return
	 */
	public boolean tryAMelt();

	/**
	 * @param obtainedNumVM
	 * @return
	 */
	public boolean tryASplit(int obtainedNumVM);

	/**
	 * @return
	 */
	public int getCurrentVmMapSize();

	/**
	 * @return
	 */
	public double getCurrentUsedCPUPercent();

	/**
	 * @return
	 */
	public HostStatus getCurrentStatus();

	/**
	 * @param eElement
	 */
	void loadFromXmlElement(Element eElement);

	/**
	 * @return
	 */
	public IVmDescriptor getBiggestVmInFutureVmMap();

	/**
	 * @param parseDouble
	 */
	public void setTotalCPU(double parseDouble);

	/**
	 * @param valueOf
	 */
	public void setCurrentStatus(HostStatus valueOf);

	/**
	 * @param hashMap
	 */
	public void setCurrentVmMap(
			Map<Integer, IVmDescriptor> hashMap);
		
	/**
	 * @param hashMap
	 */
	public void setFutureVmMap(
			Map<Integer, IVmDescriptor> hashMap);
		
	public IHostDescriptor copy();

	public double getTotalCPU();
}
