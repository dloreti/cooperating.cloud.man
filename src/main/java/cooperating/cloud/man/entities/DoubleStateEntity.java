/**
 * 
 */
package cooperating.cloud.man.entities;

/**
 * @author Daniela Loreti
 *
 */
public interface DoubleStateEntity {
	
	public void commitChanges();

}
