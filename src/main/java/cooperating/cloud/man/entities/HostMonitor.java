/**
 * 
 */
package cooperating.cloud.man.entities;

import java.util.concurrent.locks.*;

import cooperating.cloud.man.policy.Allocator;

/**
 * Allows thread coordination in the host state access 
 * @author Daniela Loreti
 *
 */
public abstract class HostMonitor {


	private IHostDescriptor host = null;
	private int freeHost=1;

	private boolean MCrunning = false;
	//private boolean futureChanged = true; //force MCs to execute for the first time
	
	private Lock lock = new ReentrantLock(); 
	private Condition C = lock.newCondition();

	private Allocator allocator;

	public HostMonitor(IHostDescriptor host, Allocator allocator){
		this.setHost(host);
		this.setAllocator(allocator);
	}


	/**
	 * @throws InterruptedException 
	 * 
	 */
	public IHostDescriptor lockHost() throws InterruptedException {
		lock.lock();
		try
		{  
			while (freeHost<=0)
				C.await();
			freeHost--;
		} finally{
			lock.unlock();
		}
		return getHost();
	}
	/**
	 * @param host2 
	 * 
	 */
	public void unlockHost(IHostDescriptor host2) {
		lock.lock();
		try
		{  
			//setFutureChanged(!value.equals(getHost()));
			setHost(host2);
			freeHost++;
			C.signalAll();
		} finally{
			lock.unlock();
		}
	}

	public abstract void checkStartMasterClient() throws InterruptedException;

	public synchronized boolean isMCrunning() {
		return MCrunning;
	}

	public synchronized void setMCrunning(boolean mCrunning) {
		MCrunning = mCrunning;
	}

	public int getId(){
		return getHost().getId();
	}

	/**
	 * @return the host
	 */
	private IHostDescriptor getHost() {
		return host;
	}


	/**
	 * @param host2 the host to set
	 */
	private void setHost(IHostDescriptor host2) {
		this.host = host2;
	}


	/**
	 * @return the allocator
	 */
	public Allocator getAllocator() {
		return allocator;
	}


	/**
	 * @param allocator the allocator to set
	 */
	private void setAllocator(Allocator allocator) {
		this.allocator = allocator;
	}


}
