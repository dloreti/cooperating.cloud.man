/**
 * 
 */
package cooperating.cloud.man.entities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Daniela Loreti
 *
 */
public interface IVmDescriptor extends DoubleStateEntity, Comparable<IVmDescriptor>{
	
	public 	void setInFutureNotAllocated();

	public int getId();

	public double getUsedCPU();

	/**
	 * @return
	 */
	public int getFutureHostID();

	/**
	 * @param id
	 */
	public void setFutureHostID(int id);

	/**
	 * @param doc
	 * @return
	 */
	public Element toXml(Document doc);

	/**
	 * @param spittedLoad
	 */
	public void setUsedCPU(double spittedLoad);

	/**
	 * @return
	 */
	public IVmDescriptor copy();

	/**
	 * @param id
	 * @return
	 */
	public boolean futureHistoryContains(int id);

	public int getCurrentHostID();

}
