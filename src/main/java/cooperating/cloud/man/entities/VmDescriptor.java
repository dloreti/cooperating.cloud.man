/**
 * 
 */
package cooperating.cloud.man.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * The descriptor of the virtual machine
 * @author Daniela Loreti
 *
 */
@SuppressWarnings("serial")
public class VmDescriptor implements Serializable, IVmDescriptor{
	

	private int id;
	private double totalCPU;
	private double usedCPU;
	private int currentHostID = -1; //not currently allocated
	private int futureHostID = -1;  //not allocated in the future
	List<Integer> futureHistory = new ArrayList<Integer>();

	public VmDescriptor(int id, double totalCPU, double usedCPU, int currentHostID){
		this.setId(id);
		this.setTotalCPU(totalCPU);
		this.setUsedCPU(usedCPU);
		this.setCurrentHostID(currentHostID);
		this.setFutureHostID(currentHostID);
	}

	private VmDescriptor(IVmDescriptor vm){
		if (vm.getClass()==VmDescriptor.class){
			VmDescriptor v1 = (VmDescriptor) vm;
			this.setId(v1.id);
			this.setTotalCPU(v1.totalCPU);
			this.setUsedCPU(v1.usedCPU);
			this.setCurrentHostID(v1.currentHostID);
			this.setFutureHostID(v1.futureHostID);
		}else {
			System.err.println("VirtualMachineDescriptor clone Not inizialized");
		}
		
		
		//this(iVirtualMachineDescriptor.id, iVirtualMachineDescriptor.totalCPU,iVirtualMachineDescriptor.usedCPU, iVirtualMachineDescriptor.getCurrentHostID(), iVirtualMachineDescriptor.getFutureHostID());
	}
	
	
	/**
	 * @param item
	 */
	public VmDescriptor(Element item, int currentHostID) {
		this(Integer.parseInt(item.getAttribute("id")),
				Double.parseDouble(item.getAttribute("totalCPU")),
				Double.parseDouble(item.getAttribute("usedCPU")),
				currentHostID);
	}

	public String toString(){
		String story="";
		for (Integer hostid : this.getFutureHistory()) {
			story+=hostid+",";
		}
		return "VirtualMachine[id="+id+",totalCPU="+totalCPU+",usedCPU="+getUsedCPU()+",futureHostID="+futureHostID+",currentHostID="+currentHostID+
				",story{"+
				story+"}]";		
	}

	public Element toXml(Document doc){
		Element vm = doc.createElement("vm");
		vm.setAttribute("id", this.getId()+"");
		vm.setAttribute("totalCPU", this.getTotalCPU()+"");
		vm.setAttribute("usedCPU", this.getUsedCPU()+"");
		return vm;
	}
	/**
	 * @return the VIRTUALLY available CPU 
	 */
	public double getAvailableCPU() {
		return totalCPU - getUsedCPU();
	}

	/**
	 * @return the id of the VM
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id of the VM to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the totalCPU allocated to the VM
	 */
	public double getTotalCPU() {
		return totalCPU;
	}
	/**
	 * @param totalCPU the totalCPU to set
	 */
	public void setTotalCPU(double totalCPU) {
		this.totalCPU = totalCPU;
	}
	/**
	 * @return the usedCPU
	 */
	public double getUsedCPU() {
		return usedCPU;
	}
	/**
	 * @param usedCPU the usedCPU to set
	 */
	public void setUsedCPU(double usedCPU) {
		if (usedCPU>totalCPU)
			try {
				throw new Exception ("vmid="+this.getId()+" usedCPU="+usedCPU+">totalCPU="+this.getTotalCPU());
			} catch (Exception e) {
				e.printStackTrace();
			}
		this.usedCPU = usedCPU;
	}
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(IVmDescriptor o) {
		if (this.getUsedCPU() > o.getUsedCPU())
			return -1;
		else if (this.getUsedCPU() == o.getUsedCPU()) {
			//confronto degli ID!
			if (this.getId() > o.getId())
				return -1;
			else if (this.getId() == o.getId())  //consistent with equals method!
				return 0;
			else
				return 1;
		}else 
			return 1;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		VmDescriptor vm = (VmDescriptor)obj;
		return this.getId()==vm.getId();
	}
	/**
	 * @return the currentHostID or -1 if not currently allocated
	 */
	public int getCurrentHostID() {
		return currentHostID;
	}

	/**
	 * @param currentHostID the currentHostID to set
	 */
	private void setCurrentHostID(int currentHostID) {
		this.currentHostID=currentHostID;
	}

	/**
	 * @return the futureHostID or -1 if not allocated in the future
	 */
	public int getFutureHostID() {
		return futureHostID;
	}

	/**
	 * @param futureHostID the futureHostID to set
	 */
	public void setFutureHostID(int futureHostID) {
		if (futureHostID!=-1) getFutureHistory().add(futureHostID);
		this.futureHostID=futureHostID;
	}

	//	public VirtualMachineDescriptor clone(){
	//		//if (this.currentHostID==null) System.err.println("VM"+id+" has null currHostId");
	//		return new VirtualMachineDescriptor(this.id, totalCPU, getUsedCPU(), currentHostID);
	//	}


	/**
	 * 
	 */
	public void commitChanges() {
		setCurrentHostID(this.getFutureHostID());
	}

	public boolean isCurrentlyAllocated(){
		return (currentHostID!=-1);
	}
	public boolean isInFutureAllocated(){
		return (futureHostID!=-1);
	}

	@SuppressWarnings("unused")
	private void setCurrentlyNotAllocated(){
		this.setCurrentHostID(-1);
	}
	
	public void setInFutureNotAllocated(){
		this.setFutureHostID(-1);
	}

	/* (non-Javadoc)
	 * @see cooperating.cloud.man.entities.IVirtualMachineDescriptor#copy()
	 */
	@Override
	public IVmDescriptor copy() {
		return new VmDescriptor(this);
	}
	

	public boolean futureHistoryContains(int hostID){
		return getFutureHistory().contains(hostID);
	}



	private List<Integer> getFutureHistory() {
		return futureHistory;
	}

	
	@SuppressWarnings("unused")
	private void setFutureHistory(List<Integer> futureHistory) {
		this.futureHistory = futureHistory;
	}
}
