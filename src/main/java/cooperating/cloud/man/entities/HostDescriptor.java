package cooperating.cloud.man.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import cooperating.cloud.man.evaluate.ResultDocument;
import cooperating.cloud.man.evaluate.SimParameters;
import cooperating.cloud.man.evaluate.Utility;

/**
 * This class represents the physical host state.
 * @author Daniela Loreti
 *
 */
@SuppressWarnings("serial")
public class HostDescriptor implements Serializable, IHostDescriptor{

	private int id;
	private double totalCPU;

	private HostStatus currentStatus;
	private HostStatus futureStatus;
	private Map<Integer, IVmDescriptor> currentVmMap = new HashMap<Integer, IVmDescriptor>();
	private Map<Integer, IVmDescriptor> futureVmMap = new HashMap<Integer, IVmDescriptor>();

	/**
	 * Instantiate a Host
	 * @param id a numeric global identifier f the host. During a simulation different host with the same id
	 * are not allowed
	 * @param totalCPU is the total amount of CPU in MIPS
	 * @param currentStatus The current HostStatus 
	 * @param currentVmMap array of the currently allocated virtual machines.
	 */
	public HostDescriptor(int id,
			double totalCPU,HostStatus currentStatus, Map<Integer,IVmDescriptor> currentVmMap) {
		setId(id);
		setTotalCPU(totalCPU);
		setCurrentStatus(currentStatus);
		setCurrentVmMap(currentVmMap);
		setFutureStatus(currentStatus);
		setFutureVmMap(this.getCurrentVmMapClone());
	}

	private HostDescriptor(IHostDescriptor h){
		if (h.getClass()==HostDescriptor.class){
			HostDescriptor h1 = (HostDescriptor) h;
			setId(h1.getId());
			setTotalCPU(h1.getTotalCPU());
			setCurrentStatus(h1.getCurrentStatus());
			setCurrentVmMap(h1.getCurrentVmMapClone());
			setFutureStatus(h1.getFutureStatus());
			setFutureVmMap(h1.getFutureVmMapClone());
		}else {
			System.err.println("HostDescriptor clone Not inizialized");
		}
	}
	
	

	public void loadFromXmlElement(Element eElement){
		this.id = Integer.parseInt(eElement.getAttribute("id"));
		this.totalCPU=Double.parseDouble(eElement.getAttribute("totalCPU"));
		this.currentStatus= HostStatus.valueOf(eElement.getAttribute("status"));
		this.setCurrentVmMap(new HashMap<Integer, IVmDescriptor>());
		NodeList vmList = eElement.getElementsByTagName("vm");
		for (int i = 0; i < vmList.getLength(); i++) {
			IVmDescriptor v=new VmDescriptor((Element) vmList.item(i), getId());
			this.allocate(v);
		}
		commitChanges();
	}


	public HostDescriptor(Element eElement){
		loadFromXmlElement(eElement);
	}

	public String toString(){		
		String toret= "Host[id="+id+",tot="+totalCPU+",currUsed="+getCurrentUsedCPU()+",futureUsed="+getFutureUsedCPU()+",futureStatus="+futureStatus+",currentStatus="+currentStatus+",\n\t" +
				"futVmMap[";
		for ( Entry<Integer, IVmDescriptor> futureVmMapEntry  : getFutureVmMap().entrySet()) {
			toret+=futureVmMapEntry.getValue().getId()+/*"-"+futureVmMapEntry.getValue().getUsedCPU()+*/", ";
		}
		toret+="] future-used="+getFutureUsedCPU()+"\n\tcurVmMap[";
		for ( Entry<Integer, IVmDescriptor> currentVmMapEntry  : getCurrentVmMap().entrySet()) {
			toret+=currentVmMapEntry.getValue().getId()+/*"-"+currentVmMapEntry.getValue().getUsedCPU()+*/", ";
		}
		toret+="]] current-used="+getCurrentUsedCPU();
		return toret;
	}

	/**
	 * Generate an xml element containing all the important data of this host and adds it to the rootElement paramenter.
	 * If a tag for the same host is already in the document, the tag is overwritten.
	 * @param rootElement the Document rootElement in which the hostElement will be appended
	 * @return the xml document Element of this host
	 */
	public void  toXml(Element rootElement){
		Document doc = rootElement.getOwnerDocument();
		NodeList nList = doc.getElementsByTagName("host");
		Element hostEl = null;
		for(int i=0;i<nList.getLength();i++){
			if (nList.item(i).getAttributes().getNamedItem("id").getTextContent().equals(this.getId()+"")){
				hostEl=(Element) nList.item(i);
				hostEl.getParentNode().removeChild(hostEl);
				break;
			}
		}
		hostEl = doc.createElement("host");
		hostEl.setAttribute("id", this.getId()+"");
		hostEl.setAttribute("totalCPU", this.getTotalCPU()+"");
		hostEl.setAttribute("usedCPU", this.getCurrentUsedCPU()+"");
		hostEl.setAttribute("status", this.getCurrentStatus()+"");
		for (Entry<Integer, IVmDescriptor> vmMapEntry : this.getCurrentVmMap().entrySet()) {
			Element vm = vmMapEntry.getValue().toXml(doc);
			hostEl.appendChild(vm);
		}
		rootElement.appendChild(hostEl);
	}

	public void resetFuture(){
		for (Entry<Integer, IVmDescriptor> vmEntry : getFutureVmMap().entrySet()) {
			vmEntry.getValue().setInFutureNotAllocated();
		}
		this.getFutureVmMap().clear();
		updateFutureStatus();
	}

	public Double getFutureAvailableCPUIfAllocate(IVmDescriptor vm){
		return getFutureAvailableCPU()-vm.getUsedCPU();
	}

	public double getFutureAvailableCPU(){
		return totalCPU - this.getFutureUsedCPU();
	}

	public synchronized double getFutureUsedCPU(){
		int usedCPU=0;
		for (Entry<Integer, IVmDescriptor> vmEntry : getFutureVmMap().entrySet()){
			usedCPU += vmEntry.getValue().getUsedCPU();
		}return usedCPU;
	}

	public double getFutureUsedCPUPercent(){
		return getFutureUsedCPU()*100/totalCPU;
	}

	public Set<Integer> getFutureVmMapKeySet(){
		return getFutureVmMap().keySet();
	}

	public synchronized double getCurrentUsedCPU(){
		int usedCPU=0;
		for (Entry<Integer, IVmDescriptor> vmEntry : getCurrentVmMap().entrySet()){
			usedCPU += vmEntry.getValue().getUsedCPU();

		}return usedCPU;
	}

	public double getCurrentUsedCPUPercent(){
		return getCurrentUsedCPU()*100/totalCPU;
	}

	public boolean allocate(IVmDescriptor vm){
		if (vm.getUsedCPU()<=getFutureAvailableCPU()){
			vm.setFutureHostID(this.getId());
			getFutureVmMap().put(vm.getId(), vm);
			updateFutureStatus();
			return true;
		}else{
			return false;
		}
	}

	/**Deallocate the vm passed as parameter from the Future VmMap, if present.
	 * @param vm
	 * @return true if the vm was in the Future VmMap, false otherwise
	 */
	public synchronized boolean deallocate(IVmDescriptor vm){	
		if(vm.getFutureHostID()==getId() && getFutureVmMap().remove(vm.getId()) != null){
			vm.setInFutureNotAllocated();
			updateFutureStatus();
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Re-calculate the Future Status of the host based on the Future VmMap to allocate
	 */
	private void updateFutureStatus() {
		HostStatus.updateHostStatus(this);
		//this.getFutureStatus().updateHostStatus(this);
	}

	/**
	 * Prints on xml result file the number of vm in futureVmMap that are not present in currentVmMap.
	 * That is, prints the number of vm to migrate in.
	 */
	public void printMigrationInNumber(){
		//migration-IN count
		@SuppressWarnings("unused")
		String toPrint = "SS"+id+": "+this.toString()+"\n MIG:[";
		String detail = "";
		int i =0; 
		for (Entry<Integer, IVmDescriptor> futEntry : getFutureVmMap().entrySet()) {
			boolean found = false;
			for (Entry<Integer, IVmDescriptor> curEntry : getCurrentVmMap().entrySet()) {
				if (curEntry.getKey().equals(futEntry.getKey())) {
					found=true;
					break;
				}
			}
			if (!found){
				toPrint+=futEntry.getKey()+" ";
				detail+=futEntry.getKey()+", ";
				i++;
			}
			//			if (!currentVmMap.keySet().contains(futEntry.getKey())){
			//				toPrint+=futEntry.getKey()+" ";
			//				detail+=futEntry.getKey()+", ";
			//				i++;
			//			}
		}
		toPrint+="] totMigration="+i;
		if (SimParameters.FULLDEBUG) 
			Utility.logPrintln(toPrint);

		//updating the migration-IN value for the xml result file
		if(i>0)
			ResultDocument.getInstance().addMigrations(i,id,detail);
	}

	/**
	 * Simulate a set of migrations really performed. In practice only COMMITS the Future Status and the Future VmMap into 
	 * the current configuration and updates the migration counter.
	 */
	public void commitChanges(){
		printMigrationInNumber();
		this.currentStatus = this.futureStatus;
		this.setCurrentVmMap(this.getFutureVmMapClone());
		ResultDocument.getInstance().addHostNode(this);
	}


	/** Two hostDescriptors are equals if their future Status and VmMap are equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!obj.getClass().equals(this.getClass())) return false;
		HostDescriptor h = (HostDescriptor)obj;
		if ( this.futureStatus!=h.futureStatus  ||  getFutureVmMapSize()!=h.getFutureVmMapSize() ) return false;
		for (Entry<Integer, IVmDescriptor> fvm : getFutureVmMap().entrySet()) {
			if (!h.getFutureVmMap().containsKey(fvm.getKey()) || !h.getFutureVmMap().get(fvm.getKey()).equals(fvm.getValue())) return false;
		}
		return true;
		//return (getFutureVmMap().equals(h.getFutureVmMap()) && this.futureStatus == h.futureStatus);
	}

	/*public boolean equals(Host h){
		return (futureVmMap.equals(h.getFutureVmMap()) && this.futureStatus == h.futureStatus);
	}*/

	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	public double getTotalCPU() {
		return totalCPU;
	}

	public void setTotalCPU(double totalCPU) {
		this.totalCPU = totalCPU;
	}

	public HostStatus getCurrentStatus() {
		return currentStatus;
	}

	/**Simply sets the current status. No changes effects the future.
	 * @param curVmMap
	 */
	public void setCurrentStatus(HostStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public HostStatus getFutureStatus() {
		return futureStatus;
	}

	public void setFutureStatus(HostStatus futureStatus) {
		this.futureStatus = futureStatus;
	}

	public Map<Integer, IVmDescriptor> getCurrentVmMap() {
		return currentVmMap;
	}

	private  Map<Integer, IVmDescriptor> getCurrentVmMapClone() {
		Map<Integer, IVmDescriptor> curVmMapClone = new HashMap<>();
		for (Entry<Integer, IVmDescriptor> VmEntry : getCurrentVmMap().entrySet()) {
			IVmDescriptor vm = VmEntry.getValue().copy();
			curVmMapClone.put(VmEntry.getKey(), vm);
		}
		return curVmMapClone;
	}

	/**Simply sets the current vmMap. No changes effects the future, no clone is performed.
	 * @param curVmMap
	 */
	public void setCurrentVmMap(Map<Integer, IVmDescriptor> curVmMap) {
		for (Entry<Integer, IVmDescriptor> curEntry : curVmMap.entrySet()) {
			VmDescriptor vm=(VmDescriptor) curEntry.getValue();
			if ( vm.isInFutureAllocated() && vm.getFutureHostID()!=this.getId())
				try {
					throw new Exception("The current vmMap has conflicting "
							+ "FutureHostID values for vm"+vm.getId()+"(objID="+System.identityHashCode(vm)+
							"): vm.FutureHostID="+vm.getFutureHostID()+" destinationHost="+getId());
				} catch (Exception e) {
					Utility.logPrintln("Exception in vm migration process", e);
				}
			vm.commitChanges();
		}
		this.currentVmMap = curVmMap;
	}

	public synchronized Map<Integer, IVmDescriptor> getFutureVmMap() {
		return futureVmMap;
	}

	private synchronized Map<Integer, IVmDescriptor> getFutureVmMapClone() {
		Map<Integer, IVmDescriptor> futVmMapClone = new HashMap<Integer, IVmDescriptor>();
		for (Entry<Integer, IVmDescriptor> VmEntry : getFutureVmMap().entrySet()) {
			//VirtualMachineDescriptor vm =  new VirtualMachineDescriptor(VmEntry.getValue());
			IVmDescriptor vm = VmEntry.getValue().copy();
			futVmMapClone.put(VmEntry.getKey(),vm);
		}
		return futVmMapClone;
		//return this.getFutureVmMap();
	}

	/**Simply sets the future vmMap. No changes effects the current, no clone is performed.
	 * @param futVmMap
	 */
	public synchronized void setFutureVmMap(Map<Integer, IVmDescriptor> futVmMap) {
		this.resetFuture();
		this.futureVmMap=futVmMap;
		for (Entry<Integer, IVmDescriptor> vmEntry : futVmMap.entrySet()) {
			vmEntry.getValue().setFutureHostID(this.getId());
		}
		updateFutureStatus();
	}

	public int getFutureVmMapSize(){
		return getFutureVmMap().size();
	}

	public int getCurrentVmMapSize(){
		return getCurrentVmMap().size();
	}

	/**
	 * Tries to melt two vm of the Future VmMap. No changes on the Current one.
	 * @return true if one melt is performed, false if it is not possible to melt more.
	 */
	public boolean tryAMelt(){
		for (Entry<Integer, IVmDescriptor> vmEntry1 : getFutureVmMap().entrySet()) {
			VmDescriptor vm1 = (VmDescriptor) vmEntry1.getValue();
			for (Entry<Integer, IVmDescriptor> vmEntry2 : getFutureVmMap().entrySet()) {
				VmDescriptor vm2 = (VmDescriptor) vmEntry2.getValue();
				if (!vm2.equals(vm1) && (vm1.getUsedCPU()+vm2.getUsedCPU())<=SimParameters.VM_TOTALCPU ){
					vm1.setUsedCPU(vm1.getUsedCPU()+vm2.getUsedCPU());
					//getFutureVmMap().remove(vm2.getId());
					deallocate(vm2);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Tries to split a vm of the Future VmMap. No changes on the Current one. The biggest vm is preferred. 
	 * The two generated vm have a usedCPU value equals to a half of the CPU used by the chosen vm.
	 * @return true if one split is performed, false if is not possible to spit more.
	 */
	public boolean tryASplit(int newvm_id){
		IVmDescriptor maxVM = getBiggestVmInFutureVmMap();
		if(maxVM.getUsedCPU()<SimParameters.VM_MINSIZE*2)
			return false;
		double spittedLoad = maxVM.getUsedCPU()/2;
		maxVM.setUsedCPU(spittedLoad);
		IVmDescriptor vm2 = new VmDescriptor(newvm_id, SimParameters.VM_TOTALCPU, spittedLoad, getId())	;
		allocate(vm2);
		return true;
	}

	/**
	 * @return the biggest vm in Future VmMap, that is the VM that uses the maximum amount of CPU.
	 */
	public IVmDescriptor getBiggestVmInFutureVmMap(){
		double max=0;
		IVmDescriptor toReturn = null;
		for (Entry<Integer, IVmDescriptor> vmEntry : getFutureVmMap().entrySet()) {
			double usedCPU=vmEntry.getValue().getUsedCPU();
			if (usedCPU>max){
				max=usedCPU;
				toReturn= vmEntry.getValue();
			}
		}
		return toReturn;

	}
	/*private  boolean melt(int vm1id, int vm2id){
		if (!getFutureVmMap().containsKey(vm1id) || !getFutureVmMap().containsKey(vm2id)) return false;
		VirtualMachineDescriptor vm1 = getFutureVmMap().get(vm1id);
		VirtualMachineDescriptor vm2 = getFutureVmMap().get(vm2id);
		vm1.setUsedCPU(vm1.getUsedCPU()+vm2.getUsedCPU());
		vm2=getFutureVmMap().remove(vm2id);
		return (vm2!=null);
	}

	public  IVirtualMachineDescriptor split(int vmid, int vm1_newload, int newvm_id){
		if (!getFutureVmMap().containsKey(vmid)) return null;
		VirtualMachineDescriptor vm1 = (VirtualMachineDescriptor) getFutureVmMap().get(vmid);

		int  oldload_vm1 = (int) vm1.getUsedCPU();
		vm1.setUsedCPU(vm1_newload);
		VirtualMachineDescriptor vm2 = new VirtualMachineDescriptor(newvm_id, SimParameters.VM_TOTALCPU, oldload_vm1-vm1_newload, getId())	;
		allocate(vm2);
		return new VirtualMachineDescriptor(vm2);
	}*/

	/* (non-Javadoc)
	 * @see cooperating.cloud.man.entities.IHostDescriptor#copy()
	 */
	@Override
	public IHostDescriptor copy() {
		return new HostDescriptor(this);
	}

}
