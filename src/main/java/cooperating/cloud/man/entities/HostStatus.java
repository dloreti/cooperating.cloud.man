/**
 * 
 */
package cooperating.cloud.man.entities;

import cooperating.cloud.man.evaluate.SimParameters;

/**
 * Possible states of the host
 * @author Daniela Loreti
 *
 */
public enum HostStatus {
	OVER, UNDER, OK, OFF;

	public static void updateHostStatus(HostDescriptor h){
		//FUTURE STATUS UPDATE
		if(h.getFutureVmMapSize()==0){
			h.setFutureStatus(HostStatus.OFF);
		}else
			if (h.getFutureUsedCPUPercent()<=SimParameters.HOST_THRESH_LOW) 
				h.setFutureStatus(HostStatus.UNDER);
			else if (h.getFutureUsedCPUPercent()>=SimParameters.HOST_THRESH_UP) 
				h.setFutureStatus(HostStatus.OVER);
			else 
				h.setFutureStatus(HostStatus.OK);
		
		//CURRENT STATUS UPDATE
		if(h.getCurrentVmMapSize()==0)
			h.setCurrentStatus(HostStatus.OFF);
		else
			if (h.getCurrentUsedCPUPercent()<=SimParameters.HOST_THRESH_LOW) 
				h.setCurrentStatus(HostStatus.UNDER);
			else if (h.getCurrentUsedCPUPercent()>=SimParameters.HOST_THRESH_UP) 
				h.setCurrentStatus(HostStatus.OVER);
			else 
				h.setCurrentStatus(HostStatus.OK);
	}
}
