/**
 * This package contains the entities of the project.
 * @since 1.0
 */
package cooperating.cloud.man.entities;
