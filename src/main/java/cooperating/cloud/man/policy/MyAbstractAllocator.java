package cooperating.cloud.man.policy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cooperating.cloud.man.entities.HostStatus;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;
import cooperating.cloud.man.evaluate.SimParameters;

/**
 * @author Roberto Foschini
 */
public class MyAbstractAllocator extends AbstractAllocator {
	private Map<Integer, Map<Integer, IVmDescriptor>> futureVmMap = new HashMap<>();
	protected void saveFutureVmMap() {
		synchronized (futureVmMap) {
			if (!futureVmMap.isEmpty())
				throw new UnsupportedOperationException();

			for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
				Map<Integer, IVmDescriptor> hostFutureVmMap = new HashMap<>(hostEntry.getValue().getFutureVmMap());
				futureVmMap.put(hostEntry.getKey(), hostFutureVmMap);
			}
		}
	}
	protected void restoreFutureVmMap() {
		synchronized (futureVmMap) {
			if (futureVmMap.isEmpty())
				throw new UnsupportedOperationException();

			for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
				Map<Integer, IVmDescriptor> hostFutureVmMap = futureVmMap.get(hostEntry.getKey());
				hostEntry.getValue().setFutureVmMap(hostFutureVmMap);
			}
		}
	}
	protected void freeFutureVmMap() {
		synchronized (futureVmMap) {
			if (futureVmMap.isEmpty())
				throw new UnsupportedOperationException();
			else
				futureVmMap.clear();
		}
	}

	protected int getWorkingHostsNumber() {
		int nHosts;
		switch (SimParameters.WORKING_HOST_MODE) {
		case STATIC:
			nHosts = getHostCollection().size();
			nHosts *= 1/2f;
			break;
		case ACTIVE:
			nHosts = getActiveHostCount();
			nHosts *= 2/3f;
			break;
		default:
			nHosts = 0;
			break;
		}
		return nHosts;
	}
	protected int getActiveHostCount() {
		int nHosts = 0;
		for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet())
			if (hostEntry.getValue().getFutureStatus()!=HostStatus.OFF)
				++nHosts;
		return nHosts;
	}
	protected int getVmCount() {
		int size = 0;
		for (IHostDescriptor hostEntry : getHostCollection().values())
			size += hostEntry.getFutureVmMap().size();	
		return size;
	}

	protected boolean checkOrdered(List<IVmDescriptor> vmList) {
		// controllo che gli host siano ordinati
		Integer oldHost = null;
		for (int host : getHostCollection().keySet()) {
			if (!(oldHost==null || oldHost<host))
				return false;
			oldHost = host;
		}
		
		// controllo che le vm siano ordinate
		IVmDescriptor oldVm = null;
		for (IVmDescriptor vm : vmList) {
			if (!(oldVm==null || oldVm.getUsedCPU()>vm.getUsedCPU() || oldVm.getUsedCPU()==vm.getUsedCPU() && oldVm.getId()>vm.getId()))
				return false;
			oldVm = vm;
		}

		return true;
	}
}
