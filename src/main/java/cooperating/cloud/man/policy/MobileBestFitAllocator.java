/**
 * 
 */
package cooperating.cloud.man.policy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import cooperating.cloud.man.entities.HostDescriptor;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;
import cooperating.cloud.man.entities.VmDescriptor;
import cooperating.cloud.man.evaluate.SimParameters;

/**
 * @author Daniela Loreti
 *
 */
public class MobileBestFitAllocator extends AbstractAllocator {

	private double mobile_tresh_up;
	private double mobile_tresh_down;
	private IHostDescriptor mc_old;
	private IHostDescriptor mc;

	public MobileBestFitAllocator(SortedMap<Integer, IHostDescriptor> neighHost, IHostDescriptor mc){
		super(neighHost);
		//check HostDescriptor: his vm should be VirtualMachineDescriptor
		this.setMc(mc);
		this.setMc_old(null);
		this.calculateNeighAverage();
	}

	public MobileBestFitAllocator(IHostDescriptor mc) {
		this(new TreeMap<Integer, IHostDescriptor>() , mc);
	}

//	private void initHostHistoricalCollection(){
//			for (Entry<Integer, IHostDescriptor> entry : this.getHostCollection().entrySet()) {
//				entry.setValue(new HostHistoricalDescriptorDecorator(entry.getValue()));
//			}
//	}
	
	/** If mc is under the mobile_tresh_down, send all the vms on mc to neighbors if possible and switch it off. if not possible, nothing is done.
	 * If mc is over the mobile_tresh_up selects the best vms to migrate and perform a best fit allocation on neighbor if possible.
	 * 
	 * @see cooperating.cloud.man.policy.AbstractAllocator#optimize()
	 */
	@Override
	public void optimize() {
		List<IVmDescriptor> vmList  = new ArrayList<>();
		if (getMc().getFutureUsedCPU() < getMobile_tresh_down()){
			for (Entry<Integer, IVmDescriptor> vm : getMc().getFutureVmMap().entrySet()) {
				IVmDescriptor vmh =  vm.getValue();
				vmList.add(vmh);
			}
		}else if(getMc().getFutureUsedCPU() > getMobile_tresh_up()){
			vmList = selectVirtualMachinesOfMc();
		}
		Map<Integer, Integer> migrationMap = bestFitMigrationMap(vmList);
		if (migrationMap!=null && migrationMap.size()!=0  ){
			bestFitAllocate(migrationMap);
			calculateNeighAverage();
		}
	}


	/**According to a bestFit policy tries to allocate all the vm in vmList. If it is possible, returns a map<vmid,destinationHostid> (without performing the migration)
	 * Otherwise returns null. A vm can be sent to a neighbor only if this does not cause the neighbor to go over the fixed SimParameters.HOST_THRESH_UP
	 * @param vmList
	 * @return
	 */
	private Map<Integer, Integer>  bestFitMigrationMap(List<IVmDescriptor> vmList ){
		Map<Integer, Integer> migrationMap = new HashMap<Integer, Integer>();
		Collections.sort(vmList);
		//reimplementare il sort! se due virtuali sono = devo lasciare nell'ordine in cui sono!!!
		for (IVmDescriptor virtualMachine : vmList) {
			Double min=Double.MAX_VALUE;
			int bestHostId = -1;
			for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
				HostDescriptor h= (HostDescriptor) hostEntry.getValue();
				if (virtualMachine.futureHistoryContains(h.getId())) {
					//System.out.println("Impossible to migrate "+virtualMachine+" to H"+h.getId());
					continue;
				}
				double fixed_threshup= h.getTotalCPU()/100*SimParameters.HOST_THRESH_UP;
				Double availableCPUIfAllocate = fixed_threshup-h.getFutureUsedCPU()-virtualMachine.getUsedCPU();
				if (availableCPUIfAllocate>0 && availableCPUIfAllocate<min){
					min = availableCPUIfAllocate;
					bestHostId=hostEntry.getKey();
				}
			}
			if (bestHostId==-1){ 
				return null;
			}else	{
				migrationMap.put(virtualMachine.getId(), bestHostId); 
			}
		}
		if (migrationMap.size()==0) return null;
		else return migrationMap;
	}

	/** Effectively migrate only if possible to do so for each vm currently allocated on mc
	 * @param migrationMap
	 */
	private void bestFitAllocate(Map<Integer, Integer> migrationMap){
		setMc_old( getMc().copy());
		for (Entry<Integer, Integer> migEntry : migrationMap.entrySet()) {
			int vmid = migEntry.getKey();
			VmDescriptor vm = (VmDescriptor)getMc().getFutureVmMap().get(vmid);
			if (vm.isInFutureAllocated()){
				int originHostid = vm.getFutureHostID();
				if (originHostid==getMc().getId())
					getMc().deallocate(vm);
				else
					getHostCollection().get(originHostid).deallocate(vm);
			}
			int destinationHostid = migEntry.getValue();
			getHostCollection().get(destinationHostid).allocate(vm);	
		}
	}

	/**Implements the Minimization of Migrations (MM) by Anton Beloglazov on the single mc host.
	 * @return the littlest vm to migrate out of mc. Is the vm that brigs mc back under the mobile_tresh_up. 
	 * Returns null if no vm can satisfy this constraint.
	 */
	public List<IVmDescriptor> selectVirtualMachinesOfMc(){
		List<IVmDescriptor> vmList = new ArrayList<>();
		for (Entry<Integer, IVmDescriptor> vm : getMc().getFutureVmMap().entrySet()) {
			VmDescriptor vmh = (VmDescriptor) vm.getValue();
			vmList.add(vmh);
		}
		Collections.sort(vmList);

		double mcUtil = getMc().getFutureUsedCPU();
		double bestFitUtil = Double.MAX_VALUE;
		IVmDescriptor bestFitVm = null;

		List<IVmDescriptor> migrationList = new ArrayList<IVmDescriptor>();
		while (mcUtil>mobile_tresh_up){
			for (IVmDescriptor vm : vmList) {
				if (vm.getUsedCPU()>(mcUtil-mobile_tresh_up)){
					double t = vm.getUsedCPU()-(mcUtil-mobile_tresh_up);
					if (t<bestFitUtil){
						bestFitUtil=t;
						bestFitVm=vm;
					}
				}else{
					if (bestFitUtil==Double.MAX_VALUE) 
						bestFitVm= vm;
					break;
				}
			}
			mcUtil=mcUtil-bestFitVm.getUsedCPU();
			migrationList.add(bestFitVm);
			vmList.remove(bestFitVm);
		}
		return migrationList;
	}

	/* (non-Javadoc)
	 * @see cooperating.cloud.man.policy.Allocator#checkRisingCondition(cooperating.cloud.man.entities.HostDescriptor)
	 */
	@Override
	public boolean checkRisingCondition(IHostDescriptor host) {
		if (getMc_old()!=null && getMc_old().equals(host)) return false;		
		//the mc should start every time something changes in the status, because the mobile thresholds must be recollected!
		setMc(host);
		return true;
	}


	private double calculateNeighAverage(){
		double neighTotalUsed=0;
		for (Entry<Integer, IHostDescriptor> hEntry : this.getHostCollection().entrySet()) {
			neighTotalUsed+=hEntry.getValue().getFutureUsedCPU();
		}
		double average=neighTotalUsed/(getHostCollection().size()+1);

		setMobile_tresh_up(average+(average/100*SimParameters.MOBILE_THRESH));
		setMobile_tresh_down(average-(average/100*SimParameters.MOBILE_THRESH));
		return average;
	}


	/**
	 * @return the mobile_tresh_up
	 */
	private double getMobile_tresh_up() {
		if (mobile_tresh_up==0) 
			calculateNeighAverage();
		return mobile_tresh_up; 
	}

	/**
	 * @param mobile_tresh_up the mobile_tresh_up to set
	 */
	private void setMobile_tresh_up(double mobile_tresh_up) {
		this.mobile_tresh_up = mobile_tresh_up;
	}

	/**
	 * @return the mobile_tresh_down
	 */
	private double getMobile_tresh_down() {
		if (mobile_tresh_down==0)
			calculateNeighAverage();
		return mobile_tresh_down;
	}

	/**
	 * @param mobile_tresh_down the mobile_tresh_down to set
	 */
	private void setMobile_tresh_down(double mobile_tresh_down) {
		this.mobile_tresh_down = mobile_tresh_down;
	}

	public IHostDescriptor getMc() {
		return mc;
	}

	private void setMc(IHostDescriptor mc) {
		if (getHostCollection()!=null) getHostCollection().remove(mc.getId());
		setMc_old(mc.copy());
		this.mc = mc;
	}

	/**
	 * @return the mc_old
	 */
	private IHostDescriptor getMc_old() {
		return mc_old;
	}

	/**
	 * @param mc_old the mc_old to set
	 */
	private void setMc_old(IHostDescriptor mc_old) {
		this.mc_old = mc_old;
	}

}
