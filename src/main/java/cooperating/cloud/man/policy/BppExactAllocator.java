package cooperating.cloud.man.policy;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Roberto Foschini
 */
public class BppExactAllocator extends BppTemplateAllocator {
	private static Logger logger = Logger.getAnonymousLogger();

	private static native int[] bpp(long[] w, long c);
	static {
		logger.info("java.library.path="+System.getProperty("java.library.path"));
		try {
			System.loadLibrary("Opt");
		} catch (UnsatisfiedLinkError ex) {
			logger.log(Level.SEVERE, "Error loading native library", ex);
			System.exit(0);
		}
	}


	@Override
	protected int[] optimizeBpp(long[] w, long c) {
		// algorithm directly returns number of bin, otherwise #bin=max(x[i])
//		final int[] xo = null; // new int[w.length];
		try {
//			System.out.println("w.length=" + w.length + ", c=" + c + ", xo.length=" + xo.length);
			final int[] xs = bpp(w, c);
			return xs;
		} catch (UnsatisfiedLinkError ex) {
			logger.log(Level.SEVERE, "Error calling native library", ex);
			System.exit(0);
			return null;
		}
	}
}
