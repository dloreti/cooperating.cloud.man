/**
 * 
 */
package cooperating.cloud.man.policy;

import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.SortedMap;

import cooperating.cloud.man.entities.HostStatus;
import cooperating.cloud.man.entities.IHostDescriptor;

/**
 * @author Daniela Loreti
 */
public abstract class AbstractAllocator implements Allocator {
	protected static final Logger logger = Logger.getAnonymousLogger();
	private SortedMap<Integer, IHostDescriptor> hostCollection;

	public AbstractAllocator() {
		super();
	}

	public AbstractAllocator(SortedMap<Integer, IHostDescriptor> neighHost) {
		super();
		this.setHostCollection(neighHost);
	}

	/**
	 * @return the hostCollection
	 */
	@Override
	public SortedMap<Integer, IHostDescriptor> getHostCollection() {
		return hostCollection;
	}

	/**
	 * @param hostCollection the hostCollection to set
	 */
	@Override
	public void setHostCollection(SortedMap<Integer, IHostDescriptor> hostCollection) {
		if (getHostCollection()!=null && getHostCollection().size()!=0) {
			String toPrint="";
			for (Entry<Integer, IHostDescriptor> hentry : getHostCollection().entrySet()) {
				toPrint+="\n"+hentry.getValue();
			}
			throw new IllegalAccessError("HostCollection already set: "+toPrint);
		}		
		this.hostCollection = hostCollection;
	}
	
	@Override
	public void optimize() {
		if (hostCollection==null) 
			throw new RuntimeException("HostCollection not initialized.");
		else
			throw new RuntimeException("Call to AbstractAllocator.optimize()"); 
	}
	
	@Override
	public boolean checkRisingCondition(IHostDescriptor host) {
		return host.getFutureStatus()==HostStatus.OVER || host.getFutureStatus()==HostStatus.UNDER;
//		double used = host.getFutureUsedCPU()/host.getTotalCPU();
//		return used<=0.75;
	}
}
