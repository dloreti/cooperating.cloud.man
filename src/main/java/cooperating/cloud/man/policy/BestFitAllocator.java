/**
 * 
 */
package cooperating.cloud.man.policy;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map.Entry;

import cooperating.cloud.man.entities.HostStatus;
import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;

/**
 * @author Daniela Loreti
 * @author Roberto Foschini
 *
 */
public class BestFitAllocator extends MyAbstractAllocator {
	/** 
	 * @see cooperating.cloud.man.policy.AbstractAllocator#optimize()
	 */
	@Override
	public void optimize() {
		saveFutureVmMap();

		int i = 0, nHosts = getWorkingHostsNumber(), vmCountBefore = getVmCount();
		List<IVmDescriptor> vmList = new ArrayList<IVmDescriptor>();
		for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
			if (i++ < nHosts)
				continue;
			vmList.addAll(hostEntry.getValue().getFutureVmMap().values());
			hostEntry.getValue().resetFuture();
		}
		Collections.sort(vmList);
		assert checkOrdered(vmList);

		try {
			for (IVmDescriptor vmDesc : vmList) {
				double min = Double.MAX_VALUE;
				IHostDescriptor hostDesc = null;
				for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
					double availableCPU = hostEntry.getValue().getFutureAvailableCPUIfAllocate(vmDesc);
					if (availableCPU>=0 && availableCPU<min) {
						if (hostEntry.getValue().getFutureStatus()!=HostStatus.OFF) {
							// Host is ON
							hostDesc = hostEntry.getValue();
							min = availableCPU;
						} else {
							// Host is OFF
							hostDesc = hostEntry.getValue();
							break;
						}
					}
				}
				if (hostDesc!=null)
					hostDesc.allocate(vmDesc); 
				else
					throw new RuntimeException();
			}
		} catch (RuntimeException ex) {
			restoreFutureVmMap();
		} finally {
			freeFutureVmMap();
			assert getVmCount() == vmCountBefore;
		}
	}
}
