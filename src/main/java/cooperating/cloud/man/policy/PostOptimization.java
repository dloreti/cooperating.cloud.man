package cooperating.cloud.man.policy;

import java.util.logging.Logger;

/**
 * @author Roberto Foschini
 */
class PostOptimization {
	private Logger logger = Logger.getAnonymousLogger();

	private static final int msTimeoutPerm = 30000;
	private static final int msTimeoutTail = 30000;
	private static class PermInt {
		private final int[] values;
		public PermInt(int N) {
			values = new int[N];
			for (int i = 0; i < N; ++i)
				values[i] = i;
		}
		public PermInt(int[] perm) {
			values = perm.clone();
		}
		void swap(int x, int y) {
			int tmp = values[x];
			values[x] = values[y];
			values[y] = tmp;
		} 
		public int[] get() {
			return values;
		}
		public boolean getNext() {
			int N = values.length;

			int i = N - 1;
			while (i>0 && values[i-1] >= values[i])
				i--;

			if (i==0)
				return false;

			int j = N;
			while (values[j-1] <= values[i-1])
				j--;

			swap(i-1, j-1);    // swap values at positions (i-1) and (j-1)

			i++; j = N;
			while (i < j) {
				swap(i-1, j-1);
				i++;
				j--;
			}

			return true;
		}
	}
	/**
	 * 
	 * @param x0
	 * @param xs
	 * @return
	 */
	int distance(int[] x0, int[] xs) {
		int ret = 0;
		for (int j = 1; j < x0.length && j < xs.length; ++j)
			if (x0[j] != xs[j])
				ret++;
		return ret;
	}
	/**
	 * 
	 * @param s
	 * @param v
	 */
	void printIntArray(String s, int[] v) {
		System.out.print(s);
		for (int x : v)
			System.out.printf(" %2d", x);
		System.out.println();
	}
	int[] init(int[] xsNew) {
		// calcolo il valore della soluzione
		int zs = 0;
		for (int i = 0; i < xsNew.length; ++i)
			if (zs < xsNew[i]+1)
				zs = xsNew[i]+1;

		// inizializzo vettore permutazioni
		int[] permBest = new int[zs];
		for (int i = 0; i < zs; ++i)
			permBest[i] = i;
		return permBest;
	}
	/**
	 * 
	 * @param xsOld
	 * @param xsNew
	 * @param perm
	 * @return
	 */
	int[] minMigrationsPerm(int[] xsOld, int[] xsNew, int[] perm) {
		final int N = xsOld.length;

//		printIntArray("\nxsOld=", xsOld);
//		printIntArray("xsNew=", xsNew);

		// soluzione migliore
		int[] permBest = null;
		int[] xsBest = null;

		// soluzione corrente
		int[] xsCurr = new int[N];

		long from = System.currentTimeMillis();
		PermInt permInt = new PermInt(perm);
		do {
			int[] permCurr = permInt.get();

			for (int j = 0; j < N; ++j)
				xsCurr[j] = permCurr[xsNew[j]];

			int distBest = (xsBest==null) ? Integer.MAX_VALUE : distance(xsOld, xsBest);
			int distCurr = distance(xsOld, xsCurr);
			if (distBest > distCurr) {
				permBest = permCurr.clone();
				xsBest = xsCurr.clone();

				logger.fine("\nmigliore:" + distBest + " > " + distCurr);
//				printIntArray("pCurr=", permCurr);
//				printIntArray("xsOld=", xsOld);
//				printIntArray("xCurr=", xsCurr);

				if (distCurr==0)
					break;
			}

			if (System.currentTimeMillis() - from > msTimeoutPerm)
				break;
		} while (permInt.getNext());

//		printIntArray("\npermBest=", permBest);
//		printIntArray("xsBest=", xsBest);

		return permBest;
	}
	/**
	 * 
	 * @param xsOld
	 * @param xsNew
	 * @return
	 */
	int[] minMigrationsTail(int[] xsOld, int[] xsNew, int[] perm) {
		final int N = xsOld.length;

		// soluzione corrente
		int[] xsCurr = new int[N];

		// soluzione migliore
		int[] permBest = null;
		int[] xsBest = null;
		
		long from = System.currentTimeMillis();
		PermInt permInt = new PermInt(perm);
		int nPerm = 0;
		do {
			// genera nuova soluzione xsCurr
			int[] permCurr = permInt.get();
			for (int j = 0; j < permCurr.length; j++)
				xsCurr[permCurr[j]] = xsNew[j];
			nPerm++;

			int distBest = (xsBest==null) ? Integer.MAX_VALUE : distance(xsOld, xsBest);
			int distCurr = distance(xsOld, xsCurr);
			if (distBest > distCurr) {
				permBest = permCurr.clone();
				xsBest = xsCurr.clone();

				System.out.println("\nmigliore:" + distBest + " > " + distCurr);
//				printIntArray("pCurr=", permCurr);
//				printIntArray("xsOld=", xsOld);
//				printIntArray("xCurr=", xsCurr);

				if (distCurr==0)
					break;
			}

			if (System.currentTimeMillis() - from > msTimeoutTail)
				break;
		} while (permInt.getNext());

		System.out.println("\n#permutazioni generate: " + nPerm);
//		printIntArray("pBest=", permBest);
//		printIntArray("xBest=", xsBest);

		return permBest;
	}
}
