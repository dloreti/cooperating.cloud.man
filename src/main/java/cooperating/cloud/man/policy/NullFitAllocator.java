package cooperating.cloud.man.policy;

/**
 * @author Roberto Foschini
 */
public class NullFitAllocator extends AbstractAllocator {
	/** 
	 * @see cooperating.cloud.man.policy.AbstractAllocator#optimize()
	 */
	@Override
	public void optimize() {
	}
}
