package cooperating.cloud.man.policy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;

/**
 * @author Roberto Foschini
 */
public abstract class BppTemplateAllocator extends MyAbstractAllocator {
	protected abstract int[] optimizeBpp(long[] w, long c);

	protected int[] postOpt(int[] xsOld, int[] xsNew) {
		final int N = xsOld.length;
		int zs = 0;
		for (int i = 0; i < N; ++i)
			if (zs < xsNew[i])
				zs = xsNew[i];

		PostOptimization po = new PostOptimization();

		// stampa soluzioni bpp
//		po.printIntArray("\nxsOld=", xsOld);
//		po.printIntArray("xCurr=", xsNew);
//		po.printIntArray("pCurr=", perm);
/*
 * 
 */
		int[] permBest = po.init(xsNew);

		// aggiusta testa
		permBest = po.minMigrationsPerm(xsOld, xsNew, permBest);
//		po.printIntArray("pBest=", permBest);
		// aggiusta coda
//		permBest = po.minMigrationsTail(xsOld, xsNew, permBest);
//		po.printIntArray("pBest=", permBest);
/*
 * 
 */
		// permuta il vettore xsNew
		int[] xsBest = new int[N];
		for (int j = 0; j < N; ++j)
			xsBest[j] = permBest[xsNew[j]];

		// stampa soluzione permutata
//		po.printIntArray("\nxsOld=", xsOld);
//		po.printIntArray("xsNew=", xsNew);
//		po.printIntArray("xBest=", xsBest);
//		po.printIntArray("pBest=", permBest);

		return xsBest; 
	}

	private int getNumHosts(int[] xsNew) {
		int numHosts = 0;
		for (int i = 0; i < xsNew.length; ++i)
			numHosts = Math.max(numHosts, xsNew[i]);
		assert (0 < numHosts) && (numHosts <= xsNew.length);
		return numHosts;
	}
	
	@Override
	public void optimize() {
		saveFutureVmMap();

		List<IVmDescriptor> vmList = new ArrayList<>();
		for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
			vmList.addAll(hostEntry.getValue().getFutureVmMap().values());
			hostEntry.getValue().resetFuture();
		}

		// genero xsOld (devo partire dalle vm senza il sort)
//		int[] xsOld = new int[vmList.size()];
//		for (int i = 0; i < vmList.size(); ++i)
//			xsOld[i] = vmList.get(i).getCurrentHostID();

		// ordino i dati per poter eseguire il bpp
		Collections.sort(vmList);
		assert checkOrdered(vmList);

		try {
			// dopo il sort, creo istanza bpp (base 0)
			final int n = vmList.size();
			final long[] w = new long[n];
			for (int i = 0; i < n; ++i)
				w[i] = (long)vmList.get(i).getUsedCPU();
			long c = 0;
			for (IHostDescriptor hostEntry : getHostCollection().values()) {
				c = (long)hostEntry.getTotalCPU();
				break;
			}

			// risolvo l'istanza bpp
			int[] xsNew = optimizeBpp(w, c);
			if (xsNew!=null) {
//				int numHosts = getNumHosts(xsNew);
//				logger.fine(""+numHosts);
	
				// porto il vettore soluzione da base 1 a base 0
				for (int i = 0; i < xsNew.length; ++i)
					--xsNew[i];
	
				// vedo se riesco a diminuire il numero di migrazioni
//				if (getHostCollection().size()<=10)
//					xsNew = postOpt(xsOld, xsNew);
	
				// esegue allocazione vm agli host
				for (int j = 0; j < vmList.size(); ++j) {
					Entry<Integer, IHostDescriptor> host = null;
					for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
						if (--xsNew[j]<0) {
							host = hostEntry;
							break;
						}
					}
					if (host!=null) {
						logger.fine("position " + j + " assigning vm " + vmList.get(j).getId() + " to host " + host.getValue().getId());
						host.getValue().allocate(vmList.get(j));
					} else
						// in questo caso la soluzione trovata dall'algoritmo necessita di più hosts rispetto a quella originale
//						System.out.println("cosa è successo?? " + j + ", " + xsNew[j] + ", " + numHosts);
						throw new RuntimeException();
				}
			}
			else
				throw new UnsupportedOperationException();
		} catch (RuntimeException ex) {
			restoreFutureVmMap();
		} finally {
			freeFutureVmMap();
			assert getVmCount() == vmList.size();
		}
	}
}
