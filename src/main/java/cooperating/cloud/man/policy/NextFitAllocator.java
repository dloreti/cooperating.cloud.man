package cooperating.cloud.man.policy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import cooperating.cloud.man.entities.IHostDescriptor;
import cooperating.cloud.man.entities.IVmDescriptor;

/**
 * @author Roberto Foschini
 */
public class NextFitAllocator extends MyAbstractAllocator {
	/** 
	 * @see cooperating.cloud.man.policy.AbstractAllocator#optimize()
	 */
	@Override
	public void optimize() {
		saveFutureVmMap();

		int i = 0, nHosts = getWorkingHostsNumber(), vmCountBefore = getVmCount();
		List<IVmDescriptor> vmList = new ArrayList<>();
		for (Entry<Integer, IHostDescriptor> hostEntry : getHostCollection().entrySet()) {
			if (i++ < nHosts)
				continue;
			vmList.addAll(hostEntry.getValue().getFutureVmMap().values());
			hostEntry.getValue().resetFuture();
		}
		Collections.sort(vmList);
		assert checkOrdered(vmList);

		try {
			Iterator<Entry<Integer, IHostDescriptor>> iter = getHostCollection().entrySet().iterator();
			Entry<Integer, IHostDescriptor> hostEntry = iter.next();
			for (IVmDescriptor vmDesc : vmList) {
				IHostDescriptor hostDesc = null;
				double availableCPU = hostEntry.getValue().getFutureAvailableCPUIfAllocate(vmDesc);
				if (availableCPU>=0)
					hostDesc = hostEntry.getValue();
				else {
					if (iter.hasNext()) {
						hostEntry = iter.next();
						availableCPU = hostEntry.getValue().getFutureAvailableCPUIfAllocate(vmDesc);
						if (availableCPU>=0)
							hostDesc = hostEntry.getValue();
						else
							// la vm non ci sta
							;
					} else {
						// non ci sono altri host ==> l'algoritmo NextFit terminerebbe
						// provo a ricominciare da capo
						hostEntry = getHostCollection().entrySet().iterator().next();
						availableCPU = hostEntry.getValue().getFutureAvailableCPUIfAllocate(vmDesc);
						if (availableCPU>=0)
							hostDesc = hostEntry.getValue();
						else
							; // la vm non ci sta
					}
				}
				if (hostDesc!=null)
					hostDesc.allocate(vmDesc);
				else
					// impossibile allocare tutte le vm
					throw new RuntimeException();
			}
		} catch (RuntimeException ex) {
			restoreFutureVmMap();
		} finally {
			freeFutureVmMap();
			assert getVmCount() == vmCountBefore;
		}
	}
}
