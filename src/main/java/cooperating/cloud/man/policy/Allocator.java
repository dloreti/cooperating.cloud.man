/**
 * 
 */
package cooperating.cloud.man.policy;

import java.util.SortedMap;

import cooperating.cloud.man.entities.IHostDescriptor;

/**
 * @author Daniela Loreti
 *
 */
public interface Allocator {
	
	public void optimize();

	public boolean checkRisingCondition(IHostDescriptor host);

	/**
	 * @return
	 */
	public SortedMap<Integer, IHostDescriptor> getHostCollection();

	/**
	 * @param neighHost
	 */
	public void setHostCollection(SortedMap<Integer, IHostDescriptor> hostCollection) ;
}
