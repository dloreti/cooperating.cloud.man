package cooperating.cloud.man.policy;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Roberto Foschini
 */
public class BppHeurAllocator extends BppTemplateAllocator {
	private static final boolean DEBUG = false; 
	private static Logger logger = Logger.getAnonymousLogger(); 
	private static int lb1eqlb2 = 0, lb1gtlb2 = 0, lb1ltlb2 = 0;

	private int m_N;
	private long[] m_W;
	private long m_C;

	private int search(long[] w, long r, int jp, int n)
	{
		int	n1, n2, nl, i;

		n1 = jp;
		n2 = n;
		while (n2-n1 > 2) {
			nl = (n1+n2)/2;
			if (w[nl] > r) n1 = ++nl;
			else n2 = --nl;
		}

		for (i = n1; i <= n2; ++i)
			if (w[i] <= r)
				return --i;
		return n2;
	}

	private final int LB0(int n, long[] w, long c) {
		long sumw = 0;
		for (int j = 1; j <= n; ++j)
			sumw += w[j];
		return (int)Math.ceil(sumw/(double)c);
	}

	private final int LB1(int n, long[] w, long c) {
		long sumw = 0, sumwb = 0;
		int jb = 0;
		for (int j = 1; j <= n; ++j) {
			sumw += w[j];
			// cerco il primo indice per cui non posso mettere due oggetti assieme 
			if (w[j] + w[n] > c) {
				jb = j;
				sumwb = sumw;
			}
		}
		//	calcolo capacita` inutilizzabile
		return jb + (int)Math.ceil((sumw - sumwb)/(double)c);
	}

	private final int LB2(int N, long[] W, long c) {
		int lb;
		int	LBcur, LBmax;
		
		boolean b1;
		boolean b2;

		int	lb1; // ROBY
		int	i4;
		long sw2, sw3, sw4;

		int	lb2; // TOTH
		int	xx, nn, cj2, cj3;
		int	cj12, cj12p1, cj4;
		long cbw;

		if ( W[1] <= c/2 ) {
//	CASE 1: ALL ITEMS ARE <= C/2
			lb = LB0(N, W, c);

			int	ir, irat;
			irat = (int)(c/W[1]);
			for (int i = 2; i <= N; ++i) {
				ir = (int)(c/W[i]);
				if (ir > irat) {
					LBcur = (i - 2)/irat + 1;
					if (lb < LBcur) lb = LBcur;
					LBmax = (N - 1)/ir + 1;
					if (lb >= LBmax) return lb;
					irat = ir;
				}
			}
			LBcur = (N - 1)/irat + 1;
			if (lb < LBcur) lb = LBcur;
		}
		else {
//	CASE 2: THERE EXIST ITEMS  > C/2
			lb = search(W, c/2, 1, N);
			if (lb == N)
				return lb;

			int	i2, i3;
			long	alfa, cmalfa;
			i2 = lb;
			i3 = lb + 1;

			b1 = false;
			b2 = false;

			lb1 = lb;

			sw2 = 0;
			sw3 = 0;
			for (sw4 = 0, i4 = i3; i4 <= N; ++i4)
				sw4 += W[i4];

			lb2 = lb;

			cj12 = i2;
			cj12p1 = i3;
			cj4 = N - cj12;
			cbw = c - W[cj12];

//	i2 = NEXT ITEM TO BE CONSIDERED FOR POSSIBLE INCLUSION IN  n2
//	i3 = NEXT ITEM TO BE CONSIDERED FOR POSSIBLE INCLUSION IN  n3
			while (i3 <= N) {
/*************************************************************************/
				alfa = W[i3];
				while ((i3 <= N) && (W[i3] >= alfa))
					sw3 += W[i3++];
				cmalfa = c - alfa;
				while ((i2 >= 1) && (W[i2] <= cmalfa))
					sw2 += W[i2--];
/*************************************************************************/
				if (!b1)
				{
					LBcur = i2 + (int)((sw3 + sw2 - 1)/c + 1);
					if (lb1 < LBcur) lb1 = LBcur;
					LBmax = i2 + (int)((sw4 + sw2 - 1)/c + 1);

					b1 = (lb1 >= LBmax);
				}
/*************************************************************************/
				if (!b2)
				{
					cj3 = i3 - cj12p1;
					cj2 = cj12 - i2;
					// Valuto la capacit� residua in due modi:
                    // Ogni oggetto in J2 occupa un bin differente, ma ha spazio libero per altri oggetti
					nn = cj2*(int)(cbw/alfa);		// quanti oggetti alfa stanno nel bin con minore capacit�
//#if 0
//					// Calcolo esatto, in O(n)
//					// Non produce miglioramenti apprezzabili sul bound
//					for (nn = 0, xx = i2+1; xx <= cj12; ++xx)
//						nn += (C-W[xx])/alfa;		// quanti oggetti alfa stanno nel bin
//#endif

// Nel 2.64% dei casi (nn < xx), ma il bound migliora nello 0.77% dei casi
					// Vantaggi: pochi calcoli aggiuntivi
					// Svantaggi: devo avere sw2
					xx = (int)((cj2*c - sw2)/alfa);	// quanti oggetti alfa stanno in tutti i bin
					nn = Math.min(nn, xx);

					xx = (int)(c/alfa); // quanti oggetti alfa stanno in un bin nuovo

					// Calcolo quanti extra-bin mi servono
					if (cj3 > nn) {
						LBcur = cj12 + (cj3 - nn - 1)/xx + 1;
						if (lb2 < LBcur) lb2 = LBcur;
					}
					LBmax = cj12 + ((cj4 > nn) ? (cj4 - nn - 1)/xx + 1 : 0);

					b2 = (lb2 >= LBmax);
				}
/*************************************************************************/
				if (b1 && b2)
					break;
			}

			// Statistiche
			if (lb1 == lb2)
				++lb1eqlb2;
			else
				if (lb1 > lb2)
					++lb1gtlb2;
                else
					++lb1ltlb2;
			logger.fine(">=<" + ", =" + lb1eqlb2 + ", >" + lb1gtlb2 + ", <" + lb1ltlb2);

//			lb = lb1;
			lb = Math.max(lb1, lb2);
		}
		return lb;
	}

	private final int LB3(int n, long[] w, long c, boolean myopt, int zsub, int[] xsub) {
		long[] crub = new long[n+1];
		Arrays.fill(crub, c);

		int	ns;
		int	k;
		int L3, L2;
		int	z, zr;
	    int[] bs = new int[n+1];
		Arrays.fill(bs, 0);
	    long[] ws = w;

//	#ifdef UpperBound3
		int[] SetN = new int[n+1];

		for (int j = 1; j <= n; ++j)
			SetN[j] = j;

    	Arrays.fill(xsub, 0);
//	#endif
		L3 = 0;
		z = 0;
		ns = n;

//		printW(0, w);
//		if (w[1]>c/10)

		while (ns >= 1) {
			if (ns%1000==0)
				System.out.println("ns="+ns);
			zr = mtrp(ns, ws, c, bs);

			// istruzione (1) spostata sotto
//			z += zr;
			k = 0;
			for (int j = 1; j <= ns; ++j) {
				if (bs[j] == 0) {
					++k;
					ws[k] = ws[j];
//	#ifdef UpperBound3
					SetN[k] = SetN[j];
				}
				else {
					// bs è in base 1; se voglio ottenere xs devo sommare il valore corrente di z
					xsub[SetN[j]] = z+bs[j];
					// se mi occorre anche la capacità residua
					crub[xsub[SetN[j]]] -= ws[j];
//	#endif
				}
			}

			ns = k;
			if (ns == 0)
				L2 = 0;
			else {
				L2 = LB2(ns, ws, c);
				// istruzione (2) spostata: se ns è unsigned, va messo qui!!!
				--ns;	//	COMMENT: REMOVAL OF SMALLEST ITEM
			}

			// stampa
			if (DEBUG)
				System.out.println();
			logger.info("L2=" + L2);
			printSet("LB-SetN", ns, SetN);

			// istruzione (1) spostata qui per consentire a xsub di essere impostato correttamente
			z += zr;

			if (L3 < z+L2)
				L3 = z+L2;
			// istruzione (2) spostata sopra
//			--ns;	//	COMMENT: REMOVAL OF SMALLEST ITEM

//			printWZX(0, w, z, xsub);
		}
//		else
//			L3 = LB2(n, w, c);

		// A questo ho una soluzione (z), ma potrebbe essere parziale.
		// Tento di completare la soluzione senza aggiungere nuovi bin.
		// Se ci riesco, la soluzione è ottima; se invece non ci riesco, non posso trarre conclusioni
//		if (logLevel >= LOG_DEBUG)
//			printWZX(0, w, z, xsub);
		xsub = COMPLETESOLUTION(n, w, z, xsub, crub);
		for (int j = 1; j <= n; ++j)
			if (zsub < xsub[j])
				zsub = xsub[j];
//		if (logLevel >= LOG_DEBUG)
//			printWZX(0, w, zsub, xsub);

//		std::clog << "z=" << z << ", zsub=" << zsub << std::endl;
		myopt = (z == zsub);

		assert(0 < zsub && zsub <= n);

		return L3;
	}

	private int mtrp(int n, long[] w, long c, int[] b) {
		int CSetNmNs, CSetNp;
		long	E, JSUM, JMAX;
		int	i, j, k, l;
		int	j1, j2, j3, j4, i3 = 0, i4 = 0;
		int[] SetNp = new int[n+1];

		int	Kpre, j2pre, j3pre;
		int[] SetNmNs = new int[n+1];

		int zr = 0;
		for (j = 1; j <= n; ++j) {
			SetNmNs[j] = j;
			SetNp[j] = j;
			b[j] = 0;
		}
		l = 1;

		CSetNmNs = n;
		CSetNp = n;
		l = 1;

		logger.info("MTRP");
//		printSet("NmNs", CSetNmNs, SetNmNs);

//		Loop principale
		while (CSetNmNs > 0) {
			j1 = SetNp[1];
			j2 = 0;
			j3 = 0;
			j4 = 0;

			j2pre = j2;
			j3pre = j3;

//		Cerca in N' il massimo (K) [cardinalit�]
			k = 1;
			JSUM = w[j1];
			for (j = CSetNp; j >= 2; --j) {
				E = w[SetNp[j]];
				JSUM += E;
				if (JSUM > c) {
					if (w[j1]+E > c) break;
				}
				else {
					++k;
				}
			}

			Kpre = k;

			if (k == 1) {
				b[j1] = ++zr;
				logger.info("k=" + k + ", j=" + j1);
			} else {
//		Find J2 in N' s.t. W(J2) = max{W(JA) : JA in N', W(J1)+W(JA)<=C}
				j2 = SetNp[j+1];

				j2pre = j2;

				if ((k == 2) || (w[j1]+w[j2] == c)) {
					b[j1] = b[j2] = ++zr;
					k = 2;	//	Nel caso non lo sia
					logger.info("k=" + k + ", j1=" + j1 + ", j2=" + j2);
				} else {
					if (k == 3) {
//		Find J3,J4 in N' s.t. W(J3)+W(J4) = max{W(JA)+W(JB) : JA,JB in N', W(J1)+W(JA)+W(JB)<=C}
						j3 = j+1;
						j4 = CSetNp;
						JMAX = 0;
						while (j3 < j4) {
							i = SetNp[j3];
							j = SetNp[j4];
							JSUM = w[i]+w[j];
							if (w[j1]+JSUM <= c) {
								if (JMAX < JSUM) {
									JMAX = JSUM;
									i3 = j3;
									i4 = j4;
								}
								--j4;
							} else {
								++j3;
							}
						}
						j3 = SetNp[i3];

						j3pre = j3;

						j4 = SetNp[i4];

						if (w[j2] >= JMAX) {
							b[j1] = b[j2] = ++zr;
							j3 = 0;
							k = 2;
						} else {
							JSUM = w[SetNp[i4-1]] + w[SetNp[i4-2]];
							if ((w[j2] == w[j3]) && ((i4-i3 <= 2) || (w[j1]+JSUM > c))) {

								if (! (w[j1]+JSUM > c))
									logger.info("MTRP: @" + (w[j1] + w[j3] + w[j4]));

								b[j1] = b[j3] = b[j4] = ++zr;
								j2 = j3;
								j3 = j4;
							} else {
								j2 = 0;
								j3 = 0;
								k = 0;
							}
						}
					} else {
						if (k == 4) {
							// Cerco una triade di elementi di somma superiore a W[J2]
//							logger.info("k==4");
/*
							C INSERT ITEMS  I ,  TP ,  ISP ,  IFP .
							  250   IF ( TP .GT. ZZ ) GO TO 260
									IF ( KREL .LT. KINF ) KINF = KREL
							  260   MM = I
									IF ( I .LE. ZZ ) GO TO 270
									M = M + 1
									MM = M
							  270   CALL INSERT(I,MM,FS,X,IFP,K,JDIM)
									CALL INSERT(TP,MM,FS,X,IFP,K,JDIM)
									CALL INSERT(ISP,MM,FS,X,IFP,K,JDIM)
									LIFP = IFP
									CALL INSERT(LIFP,MM,FS,X,IFP,K,JDIM)
									XHEU(I) = KREL
									XHEU(TP) = KREL
									XHEU(ISP) = KREL
									XHEU(LIFP) = KREL
									MP = MP + 1
									IW = W(I) + W(TP) + W(ISP) + W(LIFP)
									RES(MM) = C - IW
									ISUMR = ISUMR - IW
									IF ( JJ .EQ. II) II = IFP
									JJ = IFP
									NFREE = NFREE - 4
									GO TO 450
 */
						} else {
//							logger.info("MTRP: k>4");
						}

						j2 = 0;
						k = 0;
					}
				}
			}

			//	Stampa di debug
			if (DEBUG)
				System.out.println();
			switch (k) {
			case 0:
				logger.info("MTRP: K=" + Kpre + "->" + k + ", L=" + l);
				break;
			case 1:
				logger.info("MTRP: J1=" + j1 + ", K=" + Kpre + "->" + k + ", L=" + l);
				break;
			case 2:
				logger.info("MTRP: J1=" + j1 + ", J2=" + j2pre + ", K=" + Kpre + "->" + k + ", L=" + l);
				break;
			case 3:
				logger.info("MTRP: J1=" + j1 + ", J2=" + j2pre + ", J3=" + j3pre + ", K=" + Kpre + "->" + k + ", L=" + l);
				break;
			default:
				logger.info("MTRP: J1=" + j1 + ", J2=" + j2pre + ", J3=" + j3pre + ", J4=" + j4 + ", K=" + Kpre + "->" + k + ", L=" + l);
				break;
			}
			printSet("NmNs", CSetNmNs, SetNmNs);
			printSet("Np  ", CSetNp, SetNp);
			printX(b); // n

			assert(SetNmNs[1] == SetNp[1]);

//		Compattazione insieme SetNmNs()
			i3 = 0;
			for (j = 2; j <= CSetNmNs; ++j) {
				i = SetNmNs[j];
				if ((i != j2) && (i != j3))
					SetNmNs[++i3] = i;
			}

			assert(k >= 0 && k <= 3);
			CSetNmNs -= Math.max(1, k);
			// Se CSetNmNs è unsigned, non funziona...
//			assert(CSetNmNs >= 0);
			if ((j1 > j2) && (j2 > 0))
				++CSetNmNs;

//		Compattazione insieme SetNp()
			while ((++l <= n) && (b[SetNp[l]] > 0)) ;
			if (l > n)
				break;
			i = SetNp[l];
			SetNp[l] = SetNp[1];
			SetNp[1] = i;
			i4 = l;
	//
			if (k > 0) {
				i3 = 0;
				for (j = 1; j <= CSetNp; ++j) {
					i = SetNp[j];
					if ((i != j1) && (i != j2) && (i != j3)) {
						SetNp[++i3] = i;
					}
					else {
						if (j <= i4) --l;
					}
				}
				CSetNp -= k;
			}
		}

		return zr;
	}
	private enum UB { NF, FF, BF, WF, };
	private int UB(UB type, int j, int n, int zs, int[] xs, long[] cr) {
		long[] w = m_W;
		assert(zs <= n+1);

		//	Inizio dal contenitore pieno
		for (cr[0] = 0; j <= n; ++j) { // j è già al valore corretto
			if (xs[j] != 0) continue;
			int	ival = 0;
			long cval;

			switch (type)
			{
			case NF:	//	Next Fit (decreasing)
				//	Complessita`: O(N)
				if (w[j] <= cr[zs])
					ival = zs;
				break;

			case FF:	//	First Fit (decreasing)
				//	Complessita`: O(N*m) --> O(N*log(N))
				for (int i = 1; i <= zs; ++i)
					if (w[j] <= cr[i]) {
						// determina la prima capacita` residua
						ival = i;
						break;
					}
				break;

			case BF:	//	Best Fit (decreasing)
				//	Complessita`: O(N*m) --> O(N*log(N))
				cval = Long.MAX_VALUE;
				for (int i = 1; i <= zs; ++i)
					if (w[j] <= cr[i])
						// determina la minima capacita` residua
						if (cval > cr[i]) {
							cval = cr[i];
							ival = i;
						}
				break;

			case WF:	//	Worst Fit (decreasing)
				//	Complessita`: O(N*m) --> O(N*log(N))
				cval = 0;
				for (int i = 1; i <= zs; ++i)
					if (w[j] <= cr[i])
						// determina la massima capacita` residua
						if (cval < cr[i]) {
							cval = cr[i];
							ival = i;
						}
				break;

			default:
				break;
			}

			//	E` necessario un nuovo bin?
			if (ival == 0)
				ival = ++zs;
			assert(cr[ival] >= w[j]);
			cr[ival] -= w[j];
			xs[j] = ival;
		}

		return zs;
	}
	private int[] COMPLETESOLUTION(int N, long[] w, int zs, int[] xs, long[] cr) {
		int zsBest;
		int[] xsBest = null;
		long[] crBest;

		zsBest = N+1;
//		Applica diversi algoritmi agli oggetti non ancora assegnati
		for (int k = 1; k <= 3; ++k)
		{
			int ubTemp = zs;
			int[] xsTemp = xs.clone();
			long[] crTemp = cr.clone();

			switch (k) {
			case 1:
				ubTemp = UB(UB.FF, 1, N, ubTemp, xsTemp, crTemp);
				break;
			case 2:
				ubTemp = UB(UB.BF, 1, N, ubTemp, xsTemp, crTemp);
				break;
			case 3:
				ubTemp = UB(UB.WF, 1, N, ubTemp, xsTemp, crTemp);
				break;
			default:
				throw new RuntimeException();
			}

			if (zsBest > ubTemp) {
				zsBest = ubTemp;
				xsBest = xsTemp;
				crBest = crTemp;
			}
		}
		return xsBest; 
	}

	private void printW(long[] w, long c) {
		if (DEBUG) {
			System.out.print("w=");
			for (int i = 1; i <= m_N; ++i)
				System.out.print(" " + w[i]);
			System.out.println(" <= " + c);
		}
	}
	private void printX(int[] x) {
		if (DEBUG) {
			System.out.print("x["+(x.length-1)+"]=");
			for (int i = 1; i <= m_N; ++i)
				System.out.print(" " + x[i]);
			System.out.println();
		}
	}
	private void printSet(String set, int card, int[] value) {
		if (DEBUG) {
			System.out.printf("Set%s[%4d]=", set, card);
			for (int i = 1; i <= card; ++i) // i < value.length
				System.out.printf(" %4d", value[i]);
			System.out.println();
		}
	}

	@Override
	protected int[] optimizeBpp(long[] w, long c) {
		logger.setLevel(Level.WARNING);

		// trasla l'array
		m_N = w.length;
		m_W = new long[m_N+1];
		for (int i = 0; i < m_N; ++i)
			m_W[i+1] = w[i];
		m_C = c;

		printW(m_W, m_C);
//		System.out.println("lb0=" + LB0(m_N, m_W, m_C));
//		System.out.println("lb1=" + LB1(m_N, m_W, m_C));
//		System.out.println("lb2=" + LB2(m_N, m_W, m_C));
		int lb = LB2(m_N, m_W, m_C);
		int[] xs = new int[m_N+1];
		long[] cr = new long[m_N+1];
		Arrays.fill(cr, c);
		int[] xsub = COMPLETESOLUTION(m_N, m_W, 0, xs, cr);

//		int lb3 = LB3(m_N, m_W, m_C, true, 0, xsub);
//		System.out.println("lb=" + lb3);
		int zsub = 0;
		for (int i = 0; i <= m_N; ++i)
			if (zsub < xsub[i])
				zsub = xsub[i];

		logger.fine("lb=" + lb + ", zs=" + zsub + ", " + (lb >= zsub ? "optimal" : "----------"));
		printX(xsub);
//		xsub = PostOptimization.postOptimization(null, xsub);
		printX(xsub);

		// trasla l'array
		int [] x = Arrays.copyOfRange(xsub, 1, m_N+1);
		return x;
	}
}
