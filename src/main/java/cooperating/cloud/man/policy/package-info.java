/**
 * This package collects all the optimization policies implemented on a single ccn neighborhood.
 * @since 1.0
 */
package cooperating.cloud.man.policy;
